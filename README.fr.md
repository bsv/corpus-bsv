[documentation en anglais](README.md)

# Corpus des Bulletins de Santé du Végétal

Ce dépôt contient les ressources et scripts utilisé pour la collecte, transformation et annotation des Bulletins de Santé du Végétal (BSV).
Le contenu est organisé ainsi:

* corpus_test/ : contient des métadonnées sur les différents corpus disponibles.
* src/ : contient le code utilisé pour la collecte, transformation et annotation automatique des bulletins. Le code est organisé ie 4 ssous-dossiers:
    * src/collecte/ : le worflow de collecte des bulletins qui crawl les sites des Direction Régionales de l'Agriculture et de la Forêt (DRAAF) pour récupérer les bulletins.
    * src/alvisNLP/ : contient les plans utilisés par [alvisnlp](https://github.com/Bibliome/alvisnlp) pour produire des annotations automatiques des mentions spécifiques au domaines (usages des cultures, variétés, stades de développement, organismes nuisibles, maladies et leurs vecteurs), ainsi que des entités du domaine général (dates et lieux).
    * src/xR2RML/ :  le template et la configuration à utiliser avec [xR2RML](https://github.com/frmichel/morph-xr2rml) pour transformer les annotations dans le format RDF approprié.
    * src/workflow/ : le worflow utilisé pour mettre à jour le graphe de connaissances en y ajoutant les annotations et données de provenance.
* sample/ : contient des examples de requêtes à exécuter pour récupérer les informations en lien avec les BSV contenues dans le graphe de connaissances.

La transformation des bulletins du format PDF au format HTML est effectuée à l'aide de [pdf2blocs](https://gitlab.irstea.fr/copain/pdf2blocs).

## A Unified Approach to Publish Semantic Annotations of Agricultural Documents as Knowledge Graphs

L'état du dépôt git correspondant à ce qui est décrit dans cette publication se trouve dans la branche SAAD (Semantic Annotations of Agricultural Documents) [ici](https://forgemia.inra.fr/bsv/corpus-bsv/-/tree/SAAD).
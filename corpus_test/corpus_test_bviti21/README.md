# Bilans viticulture 2021

Corpus de 18 bulletins de santé du végétal qui font le bilan
de l'année 2021 pour la viticulture.

## Auvergne-Rhône-Alpes

- *01/10/2021* : 20211027_BSV_Bilan_sanitaire_Vigne_RA_cle0de71c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2021/20211027_BSV_Bilan_sanitaire_Vigne_RA_cle0de71c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2021/20211027_BSV_Bilan_sanitaire_Vigne_RA_cle0de71c.html)]
- *22/02/2022* : BSV_bilan_2021_vigne_Auvergne_cle8a4a15 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2022/BSV_bilan_2021_vigne_Auvergne_cle8a4a15.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2022/BSV_bilan_2021_vigne_Auvergne_cle8a4a15.html)]


## Centre-Val de Loire

- *08/10/2021* : BSV_viti_bilan_2021_cle8cbb4e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2021/BSV_viti_bilan_2021_cle8cbb4e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q13947/2021/BSV_viti_bilan_2021_cle8cbb4e.html)]


## Grand-Est

- *15/12/2021* : BSVBilan_Vigne_ALS_2021_cle8bbbec [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSVBilan_Vigne_ALS_2021_cle8bbbec.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSVBilan_Vigne_ALS_2021_cle8bbbec.html)]
- *10/11/2021* : BSVBilan_Vigne_CHA_2021_cle83ec56 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSVBilan_Vigne_CHA_2021_cle83ec56.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSVBilan_Vigne_CHA_2021_cle83ec56.html)]
- *13/10/2021* : BSVBilan_Vigne_LOR_2021_cle89a116 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSVBilan_Vigne_LOR_2021_cle89a116.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSVBilan_Vigne_LOR_2021_cle89a116.html)]


## Nouvelle-Aquitaine

- *07/12/2021* : BSV_NA_VIGNE_CHARENTES_23_20211207_Bilan_cle0d7d67 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2021/BSV_NA_VIGNE_CHARENTES_23_20211207_Bilan_cle0d7d67.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2021/BSV_NA_VIGNE_CHARENTES_23_20211207_Bilan_cle0d7d67.html)]
- *05/01/2021* : BSV_NA_VIGNE_CHARENTES_24_Bilan_20210105_cle061d59 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2021/BSV_NA_VIGNE_CHARENTES_24_Bilan_20210105_cle061d59.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2021/BSV_NA_VIGNE_CHARENTES_24_Bilan_20210105_cle061d59.html)]
- *21/12/2021* : BSV_NA_VIGNE_Haut-Poitou-N__18-20211221-Bilan-1_cle03312b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2021/BSV_NA_VIGNE_Haut-Poitou-N__18-20211221-Bilan-1_cle03312b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2021/BSV_NA_VIGNE_Haut-Poitou-N__18-20211221-Bilan-1_cle03312b.html)]
- *21/12/2021* : BSV_NA_VIGNE_Nord_Aquitaine_22_20211221_Bilan-1_cle0e38d2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2021/BSV_NA_VIGNE_Nord_Aquitaine_22_20211221_Bilan-1_cle0e38d2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2021/BSV_NA_VIGNE_Nord_Aquitaine_22_20211221_Bilan-1_cle0e38d2.html)]
- *21/12/2021* : BSV_NA_VIGNE_Sud_Aquitaine_21_20211221_Bilan-1_cle4383b1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2021/BSV_NA_VIGNE_Sud_Aquitaine_21_20211221_Bilan-1_cle4383b1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2021/BSV_NA_VIGNE_Sud_Aquitaine_21_20211221_Bilan-1_cle4383b1.html)]


## Occitanie

- *28/12/2021* : bsv_bilan_viti_lr_2021_cle04d1f3 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2021/bsv_bilan_viti_lr_2021_cle04d1f3.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2021/bsv_bilan_viti_lr_2021_cle04d1f3.html)]
- *15/11/2021* : bsv_bilan_viti_mp_aveyron_2021_cle0f1d3e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2021/bsv_bilan_viti_mp_aveyron_2021_cle0f1d3e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2021/bsv_bilan_viti_mp_aveyron_2021_cle0f1d3e.html)]
- *18/11/2021* : bsv_bilan_viti_mp_cahors_2021_cle828384 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2021/bsv_bilan_viti_mp_cahors_2021_cle828384.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2021/bsv_bilan_viti_mp_cahors_2021_cle828384.html)]
- *21/10/2021* : bsv_bilan_viti_mp_fronton_tarnetgaronne_2021_cle8c68e4 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2021/bsv_bilan_viti_mp_fronton_tarnetgaronne_2021_cle8c68e4.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2021/bsv_bilan_viti_mp_fronton_tarnetgaronne_2021_cle8c68e4.html)]
- *21/10/2021* : bsv_bilan_viti_mp_gaillac_2021_cle01454b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2021/bsv_bilan_viti_mp_gaillac_2021_cle01454b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2021/bsv_bilan_viti_mp_gaillac_2021_cle01454b.html)]
- *21/10/2021* : bsv_bilan_viti_mp_gascogne_2021_cle439499 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2021/bsv_bilan_viti_mp_gascogne_2021_cle439499.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2021/bsv_bilan_viti_mp_gascogne_2021_cle439499.html)]


## Provence-Alpes-Côte d'Azur
- *18/11/2021* : BSVViti_20211118_BILAN_2021_cle062e84 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2021/BSVViti_20211118_BILAN_2021_cle062e84.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q15104/2021/BSVViti_20211118_BILAN_2021_cle062e84.html)]










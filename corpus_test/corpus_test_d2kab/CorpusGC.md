# Corpus de test D2KAB

Ce corpus de bulletins de santé du végétal est constitué
d'une sélection de bulletins "Grandes cultures". Ces bulletins ont été
choisis sur 2019 pour couvrir le plus largement possible les différentes
éditions "Grandes cultures" disponibles.

Il contient 94 bulletins de santé du végétal.

## GrandesCultures
### Auvergne - Rhône-Alpes
- 20190227_BSV_grandes_cultures_Auvergne_N02_cle0189db [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/20190227_BSV_grandes_cultures_Auvergne_N02_cle0189db.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/20190227_BSV_grandes_cultures_Auvergne_N02_cle0189db.html)
culture Colza  
stade  C2, BBCH31, C1, BBCH30, D1
Ravageurs: Charançon de la tige du colza, Charançon de la tige du choux

- 20190522_BSV_grandes_cultures_Auvergne_N14_cle02c8cf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/20190522_BSV_grandes_cultures_Auvergne_N14_cle02c8cf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/20190522_BSV_grandes_cultures_Auvergne_N14_cle02c8cf.html)
- 20190710_BSV_grandes_cultures_Auvergne_N21_cle0abf82 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/20190710_BSV_grandes_cultures_Auvergne_N21_cle0abf82.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/20190710_BSV_grandes_cultures_Auvergne_N21_cle0abf82.html)
- 20191002_BSV_grandes_cultures_Auvergne_N27_cle0fb714 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/20191002_BSV_grandes_cultures_Auvergne_N27_cle0fb714.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/20191002_BSV_grandes_cultures_Auvergne_N27_cle0fb714.html)
- 20190228_BSV_grandes_cultures_Rhone-Alpes_N_02_cle46d5a8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/20190228_BSV_grandes_cultures_Rhone-Alpes_N_02_cle46d5a8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/20190228_BSV_grandes_cultures_Rhone-Alpes_N_02_cle46d5a8.html)
- 20190516_BSV_grandes_cultures_Rhone-Alpes_N_13_cle4cc4d2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/20190516_BSV_grandes_cultures_Rhone-Alpes_N_13_cle4cc4d2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/20190516_BSV_grandes_cultures_Rhone-Alpes_N_13_cle4cc4d2.html)
- 20190919_BSV_grandes_cultures_Rhone-Alpes_N24_cle03fa8f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/20190919_BSV_grandes_cultures_Rhone-Alpes_N24_cle03fa8f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/20190919_BSV_grandes_cultures_Rhone-Alpes_N24_cle03fa8f.html)
- 20191114_BSV_grandes_cultures_Rhone-Alpes_N32_cle05361b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/20191114_BSV_grandes_cultures_Rhone-Alpes_N32_cle05361b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/20191114_BSV_grandes_cultures_Rhone-Alpes_N32_cle05361b.html)

###  Bourgogne Franche-Comté
- BSV_GC_n_01_du_28_aout_2018_cle098cfb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2018/BSV_GC_n_01_du_28_aout_2018_cle098cfb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2018/BSV_GC_n_01_du_28_aout_2018_cle098cfb.html)
 Culture: Colza, Tournesol;
 stade: 2 à 4 feuilles, levée, M1.1 (Capitule vert-jaune et graine à 50% d’humidité) et M4 (Maturité: graine à 10% ou moins d’humidité, capitule brun / noir);
 bioagresseurs: limace, Altises des crucifères ou petites altises, Adventices
- BSV_GC_n_1_du_27_08_19_cle0c116a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_GC_n_1_du_27_08_19_cle0c116a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_GC_n_1_du_27_08_19_cle0c116a.html)
- BSV_GC_n_19_du_26_03_19_cle8bda1e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_GC_n_19_du_26_03_19_cle8bda1e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_GC_n_19_du_26_03_19_cle8bda1e.html)
- BSV_GC_n_27_du_21_05_19_cle8d148f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_GC_n_27_du_21_05_19_cle8d148f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_GC_n_27_du_21_05_19_cle8d148f.html)
- BSV_GC_n_33_du_2_07_19_cle0b813e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_GC_n_33_du_2_07_19_cle0b813e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_GC_n_33_du_2_07_19_cle0b813e.html)


### Bretagne
- BSV_no6_du_26-03-19_cle8da969 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_no6_du_26-03-19_cle8da969.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_no6_du_26-03-19_cle8da969.html)
culture: Colza, Lin hiver, Lin oléagineux, Lin de printemps, blé tendre d'hiver, orge d'hiver
stade:  F1 (Premières fleurs ouvertes). E (Boutons séparés). G1 (Chute des premiers pétales) , D1, D2,  Epi 1 cm , Mi-tallage, Fin tallage, 2 nœuds
bioagresseur: Méligèthe crucifères, Charançon des siliques, Piétin-verse, Rouille jaune, Septoriose, Altises, Sclérotinia , Cylindrosporiose, Pseudocercosporella, rhizoctone, Oïdium, Taupins, Rhynchosporiose, Helminthosporiose, Rouille naine, Piétin-échaudage
- BSV_no11_du_30-04-19_cle0bc1be [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_no11_du_30-04-19_cle0bc1be.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_no11_du_30-04-19_cle0bc1be.html)
- BSV_no23_du_23-07-19_cle0f6782 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_no23_du_23-07-19_cle0f6782.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_no23_du_23-07-19_cle0f6782.html)
- BSV_no30_du_22-10-19_cle085f67 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_no30_du_22-10-19_cle085f67.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_no30_du_22-10-19_cle085f67.html)


### Centre - Val de Loire
##### Céréales à paille
- BSV_cereales_paille_02_du_29-10-19_cle8cbcac [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_cereales_paille_02_du_29-10-19_cle8cbcac.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_cereales_paille_02_du_29-10-19_cle8cbcac.html)
culture: blé dur, blé tendre, orge d'hiver
stade: levée, 1 ou 2 feuilles, germination, pré-semis
bioagresseur: cicadelle, puceron, limace, mulot, campagnol
- BSV_cereales_paille_06_du_26-11-19_cle81855b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_cereales_paille_06_du_26-11-19_cle81855b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_cereales_paille_06_du_26-11-19_cle81855b.html)
- BSV_cereales_paille_15_du_16-04-19_cle81e145 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_cereales_paille_15_du_16-04-19_cle81e145.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_cereales_paille_15_du_16-04-19_cle81e145.html)

##### Maïs
- BSV_mais_05_du_12-06-19_cle8158c5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_mais_05_du_12-06-19_cle8158c5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_mais_05_du_12-06-19_cle8158c5.html)
- BSV_mais_11_du_23-07-19_cle86f6f1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_mais_11_du_23-07-19_cle86f6f1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_mais_11_du_23-07-19_cle86f6f1.html)
- BSV_mais_15_du_03-09-19_cle85c39e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_mais_15_du_03-09-19_cle85c39e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_mais_15_du_03-09-19_cle85c39e.html)

##### Oléagineux
- BSV_oleagineux_06_du_08-10-19_cle8a1442 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_oleagineux_06_du_08-10-19_cle8a1442.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_oleagineux_06_du_08-10-19_cle8a1442.html)
- BSV_oleagineux_24_du_24-04-19_cle86ac9f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_oleagineux_24_du_24-04-19_cle86ac9f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_oleagineux_24_du_24-04-19_cle86ac9f.html)
- BSV_oleagineux_27_du_14-05-19_cle81b81c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_oleagineux_27_du_14-05-19_cle81b81c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_oleagineux_27_du_14-05-19_cle81b81c.html)


### Hauts de France
- bsv_02_gc_26_fev_2019_cle49dcfd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/bsv_02_gc_26_fev_2019_cle49dcfd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/bsv_02_gc_26_fev_2019_cle49dcfd.html)
culture: blé d'hiver, orge d'hiver, colza
stade: mi-tallage, fin tallage, B10, C1, C2, D1
bioagresseur: puceron, septoriose, oidium, mouche, charonçon de la tige, charançcon du bourgeon terminal, altise, meligethe

- bsv-gc-n__14-21-mai-2019_cle85ad4f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/bsv-gc-n__14-21-mai-2019_cle85ad4f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/bsv-gc-n__14-21-mai-2019_cle85ad4f.html)
- BSV-GC-n__20-02072019_cle4693f1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV-GC-n__20-02072019_cle4693f1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV-GC-n__20-02072019_cle4693f1.html)
- BSV-GC-n__33-01102019_cle41f418 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV-GC-n__33-01102019_cle41f418.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV-GC-n__33-01102019_cle41f418.html)
- BSV-GC-n__36-22102019_cle43a7f1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV-GC-n__36-22102019_cle43a7f1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV-GC-n__36-22102019_cle43a7f1.html)


### Grand Est
- 20190417_ALS_BSV_Grandes_Cultures_cle8c93cf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190417_ALS_BSV_Grandes_Cultures_cle8c93cf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190417_ALS_BSV_Grandes_Cultures_cle8c93cf.html)
culture: colza, orge, blé
stade: D1, E, F1, F3, F4, G1, BBCH 50, BBCH 57,  début floraison, Epi 1 cm, 1 noeud, 2 noeuds, 3 noeuds visibles, début montaison.
ravageur: meligethe, Helminthosporiose, Rhynchosporiose, Oïdium
- 20190417_ALS_BSV_Grandes_Cultures_cle8c93cf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190417_ALS_BSV_Grandes_Cultures_cle8c93cf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190417_ALS_BSV_Grandes_Cultures_cle8c93cf.html)
- alsace_gdes_cultures_no10_du_30-04-19_cle015281 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_gdes_cultures_no10_du_30-04-19_cle015281.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_gdes_cultures_no10_du_30-04-19_cle015281.html)
- alsace_gdes_cultures_no17_du_19-06-19_cle026e84 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_gdes_cultures_no17_du_19-06-19_cle026e84.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_gdes_cultures_no17_du_19-06-19_cle026e84.html)
- alsace_gdes_cultures_no3_du_13-03-19_cle4272b9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_gdes_cultures_no3_du_13-03-19_cle4272b9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_gdes_cultures_no3_du_13-03-19_cle4272b9.html)
- 20190417_CHA_BSV_Grandes_Cultures_cle8333d7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190417_CHA_BSV_Grandes_Cultures_cle8333d7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190417_CHA_BSV_Grandes_Cultures_cle8333d7.html)
- 20190320_LOR_BSV_Grandes_Cultures_cle83816d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190320_LOR_BSV_Grandes_Cultures_cle83816d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190320_LOR_BSV_Grandes_Cultures_cle83816d.html)
- 20190320_LOR_BSV_Grandes_Cultures_cle83816d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190320_LOR_BSV_Grandes_Cultures_cle83816d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190320_LOR_BSV_Grandes_Cultures_cle83816d.html)
- 20190507_LOR_BSV_Grandes_Cultures_cle893371 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190507_LOR_BSV_Grandes_Cultures_cle893371.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190507_LOR_BSV_Grandes_Cultures_cle893371.html)
- 20190522_LOR_BSV_Grandes_Cultures_cle8b3b61 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190522_LOR_BSV_Grandes_Cultures_cle8b3b61.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190522_LOR_BSV_Grandes_Cultures_cle8b3b61.html)
- 20190710_LOR_BSV_Grandes_Cultures_cle8d764a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190710_LOR_BSV_Grandes_Cultures_cle8d764a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190710_LOR_BSV_Grandes_Cultures_cle8d764a.html)
- char_gdes_Cultures_no11_du_30-04-19_cle096f9c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/char_gdes_Cultures_no11_du_30-04-19_cle096f9c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/char_gdes_Cultures_no11_du_30-04-19_cle096f9c.html)
- char_gdes_Cultures_no14_du_22-05-19_cle0b1586 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/char_gdes_Cultures_no14_du_22-05-19_cle0b1586.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/char_gdes_Cultures_no14_du_22-05-19_cle0b1586.html)
- char_gdes_Cultures_no26_du_14-08-19_cle0fb929 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/char_gdes_Cultures_no26_du_14-08-19_cle0fb929.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/char_gdes_Cultures_no26_du_14-08-19_cle0fb929.html)
- ge_houblon_no6_du_19-06-19_cle41558d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/ge_houblon_no6_du_19-06-19_cle41558d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/ge_houblon_no6_du_19-06-19_cle41558d.html)

### Île-de-France
- BSV_GRANDES_CULTURES_2019_No7_cle8446d7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_GRANDES_CULTURES_2019_No7_cle8446d7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_GRANDES_CULTURES_2019_No7_cle8446d7.html)
- BSV_GRANDES_CULTURES_2019_No21_cle091311 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_GRANDES_CULTURES_2019_No21_cle091311.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_GRANDES_CULTURES_2019_No21_cle091311.html)
- BSV_GRANDES_CULTURES_2019_No24_cle0c9cf1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_GRANDES_CULTURES_2019_No24_cle0c9cf1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_GRANDES_CULTURES_2019_No24_cle0c9cf1.html)
- BSV_GRANDES_CULTURES_2019_No36_cle02e122 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_GRANDES_CULTURES_2019_No36_cle02e122.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_GRANDES_CULTURES_2019_No36_cle02e122.html)


### Normandie
##### Betteraves
- BSV_Betterave_no2-1_cle861f11 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_Betterave_no2-1_cle861f11.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_Betterave_no2-1_cle861f11.html)
- BSV_Betterave_n19_cle0c8c47 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_Betterave_n19_cle0c8c47.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_Betterave_n19_cle0c8c47.html)

##### Céréales
- BSV_07-48_cereales_Normandie_2020_cle8d6d3f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_07-48_cereales_Normandie_2020_cle8d6d3f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_07-48_cereales_Normandie_2020_cle8d6d3f.html)
- BSV_19-19_cereales_Normandie_2019_cle8dba8a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_19-19_cereales_Normandie_2019_cle8dba8a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_19-19_cereales_Normandie_2019_cle8dba8a.html)
- BSV_27-29_cereales_Normandie_2019_cle8e5177 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_27-29_cereales_Normandie_2019_cle8e5177.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_27-29_cereales_Normandie_2019_cle8e5177.html)
- BSV_28-19_cereales_Normandie_R2015_cle8e8112 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2015/BSV_28-19_cereales_Normandie_R2015_cle8e8112.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2015/BSV_28-19_cereales_Normandie_R2015_cle8e8112.html)
- BSV_4-27_cereales_Normandie_R2016_cle84c13e-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2015/BSV_4-27_cereales_Normandie_R2016_cle84c13e-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2015/BSV_4-27_cereales_Normandie_R2016_cle84c13e-2.html)

##### Colza
- BSV_Colza_no01-20fev_cle091791 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_Colza_no01-20fev_cle091791.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_Colza_no01-20fev_cle091791.html)
- BSV_Colza_no17-02oct2019_cle8cb181 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_Colza_no17-02oct2019_cle8cb181.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_Colza_no17-02oct2019_cle8cb181.html)
- BSV_Colza_no20-23oct2019_cle8f1a1f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_Colza_no20-23oct2019_cle8f1a1f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_Colza_no20-23oct2019_cle8f1a1f.html)

##### Pomme de terre
- BSV_PDT_03_du_19_05_15_cle091415 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_PDT_03_du_19_05_15_cle091415.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_PDT_03_du_19_05_15_cle091415.html)
- BSV_PDT_15_du_19_07_31_cle01a8b1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_PDT_15_du_19_07_31_cle01a8b1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_PDT_15_du_19_07_31_cle01a8b1.html)

### Nouvelle Aquitaine
- BSV_GC_NA_Limousin_02_20190305_cle077475 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_GC_NA_Limousin_02_20190305_cle077475.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_GC_NA_Limousin_02_20190305_cle077475.html)
- BSV_GC_NA_Limousin_13_20190521_cle0cb17e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_GC_NA_Limousin_13_20190521_cle0cb17e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_GC_NA_Limousin_13_20190521_cle0cb17e.html)
- BSV_GC_NA_Limousin_17_20190730_cle0fa494 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_GC_NA_Limousin_17_20190730_cle0fa494.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_GC_NA_Limousin_17_20190730_cle0fa494.html)
- BSV_GC_NA_Limousin_31_20191126_cle074695 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_GC_NA_Limousin_31_20191126_cle074695.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_GC_NA_Limousin_31_20191126_cle074695.html)
- BSV_NA_GC_AQUITAINE_N01_20190207_cle01cb7e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_GC_AQUITAINE_N01_20190207_cle01cb7e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_GC_AQUITAINE_N01_20190207_cle01cb7e.html)
- BSV_NA_GC_AQUITAINE_N19_20190613_cle0c6817 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_GC_AQUITAINE_N19_20190613_cle0c6817.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_GC_AQUITAINE_N19_20190613_cle0c6817.html)
- BSV_NA_GC_AQUITAINE_N25_20190801_cle0eefb1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_GC_AQUITAINE_N25_20190801_cle0eefb1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_GC_AQUITAINE_N25_20190801_cle0eefb1.html)
- BSV_NA_GC_AQUITAINE_N35_20191024_cle0eeb7e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_GC_AQUITAINE_N35_20191024_cle0eeb7e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_GC_AQUITAINE_N35_20191024_cle0eeb7e.html)
- BSV_NA_GC_Poitou_Charentes_04_20190306_cle854b15 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_GC_Poitou_Charentes_04_20190306_cle854b15.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_GC_Poitou_Charentes_04_20190306_cle854b15.html)
- BSV_NA_GC_Poitou-Charentes_10_20190416_cle8376ad [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_GC_Poitou-Charentes_10_20190416_cle8376ad.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_GC_Poitou-Charentes_10_20190416_cle8376ad.html)
- BSV_NA_GC_Poitou-Charentes_19_20190618_cle869d2b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_GC_Poitou-Charentes_19_20190618_cle869d2b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_GC_Poitou-Charentes_19_20190618_cle869d2b.html)


### Occitanie
- bsv_grandes_cultures_n02_27092018_cle87a58c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_grandes_cultures_n02_27092018_cle87a58c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_grandes_cultures_n02_27092018_cle87a58c.html)
- bsv_grandes_cultures_n05_18102018_cle81e819-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_grandes_cultures_n05_18102018_cle81e819-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_grandes_cultures_n05_18102018_cle81e819-1.html)
- bsv_grandes_cultures_n10_22112018_cle815c16 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_grandes_cultures_n10_22112018_cle815c16.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_grandes_cultures_n10_22112018_cle815c16.html)
- bsv_grandes_cultures_n35_09082018-2_cle06f22b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_grandes_cultures_n35_09082018-2_cle06f22b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_grandes_cultures_n35_09082018-2_cle06f22b.html)
- bsv_gc_mp_n01_25092019_cle039d59 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_mp_n01_25092019_cle039d59.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_mp_n01_25092019_cle039d59.html)
- bsv_gc_mp_n02_03102019_cle0d83ff [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_mp_n02_03102019_cle0d83ff.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_mp_n02_03102019_cle0d83ff.html)
- bsv_gc_mp_n11_12122019_cle0bba99 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_mp_n11_12122019_cle0bba99.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_mp_n11_12122019_cle0bba99.html)
- bsv_gc_mp_n19_21032019_cle0c7f7d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_mp_n19_21032019_cle0c7f7d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_mp_n19_21032019_cle0c7f7d.html)
- bsv_gc_mp_n27_16052019_cle046a11 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_mp_n27_16052019_cle046a11.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_mp_n27_16052019_cle046a11.html)
- bsv_gc_mp_n33_27062019_cle051c4f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_mp_n33_27062019_cle051c4f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_mp_n33_27062019_cle051c4f.html)


### Pays de la Loire
- 20180911_bsv_grandes_cultures-26_cle0f15a8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2018/20180911_bsv_grandes_cultures-26_cle0f15a8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2018/20180911_bsv_grandes_cultures-26_cle0f15a8.html)
- 20181002_bsv_grandes_cultures_29_cle0423a6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2018/20181002_bsv_grandes_cultures_29_cle0423a6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2018/20181002_bsv_grandes_cultures_29_cle0423a6.html)
- 20181106_bsv_grandes_cultures_34_cle08159b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2018/20181106_bsv_grandes_cultures_34_cle08159b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2018/20181106_bsv_grandes_cultures_34_cle08159b.html)
- 20190730_bsv_grandes_cultures_cle879378 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2019/20190730_bsv_grandes_cultures_cle879378.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2019/20190730_bsv_grandes_cultures_cle879378.html)
- 20191210_bsv_grandes_cultures_cle8fd173 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2019/20191210_bsv_grandes_cultures_cle8fd173.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2019/20191210_bsv_grandes_cultures_cle8fd173.html)


### Provence - Alpes - Côte d'Azur
- BSV_GC_ARCMED_N03_18042019_cle479bcc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/BSV_GC_ARCMED_N03_18042019_cle479bcc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/BSV_GC_ARCMED_N03_18042019_cle479bcc.html)
- bsv_gc_arcmed_n03_18042019_cle485f1e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_arcmed_n03_18042019_cle485f1e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_arcmed_n03_18042019_cle485f1e.html)
- BSV_GC_ARCMED_N04_30042019_cle47541c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/BSV_GC_ARCMED_N04_30042019_cle47541c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/BSV_GC_ARCMED_N04_30042019_cle47541c.html)
- bsv_gc_arcmed_n09_13062019_cle4a8a8c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_arcmed_n09_13062019_cle4a8a8c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_arcmed_n09_13062019_cle4a8a8c.html)
- bsv_gc_arcmed_n09_3262159476651410763_cle013bb3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/bsv_gc_arcmed_n09_3262159476651410763_cle013bb3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/bsv_gc_arcmed_n09_3262159476651410763_cle013bb3.html)
- bsv_gc_arcmed_n12_04072019_cle43eb7b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_gc_arcmed_n12_04072019_cle43eb7b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_gc_arcmed_n12_04072019_cle43eb7b.html)


### Autres régions
#### Centre Ouest
- BSV_LIN_Olea_n12_NR_25032019_cle86164b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2018/BSV_LIN_Olea_n12_NR_25032019_cle86164b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2018/BSV_LIN_Olea_n12_NR_25032019_cle86164b.html)
- BSV_LIN_Olea_n17_NR_30042019_cle8fe95e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_LIN_Olea_n17_NR_30042019_cle8fe95e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_LIN_Olea_n17_NR_30042019_cle8fe95e.html)

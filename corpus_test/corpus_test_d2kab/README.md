# Corpus de test D2KAB

Ce corpus se découpe en trois sous-corpus ; un par cas d'usage:
- [Viticulture](https://gitlab.irstea.fr/copain/d2kab/-/blob/master/corpus_test/corpus_test_d2kab/CorpusViticulture.md),
- [Grande Culture](https://gitlab.irstea.fr/copain/d2kab/-/blob/master/corpus_test/corpus_test_d2kab/CorpusGC.md),
- [Maraichage](https://gitlab.irstea.fr/copain/d2kab/-/blob/master/corpus_test/corpus_test_d2kab/CorpusMaraichage.md).

Chaque sous-corpus est organisé par région administrative (nouvelle région).

**Répartition :**

Année | Nombre de BSV
--- | ---:
2015 | 2
2018 | 17
2019 | 211

La sélection manuelle a porté sur la couverture de l'ensemble du territoire français.
Toutes les regions de france métropolitaine sont couvertes.

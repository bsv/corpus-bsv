# Corpus de test D2KAB

Ce corpus de bulletins de santé du végétal est constitué
d'une sélection de bulletins sur la viticulture. Ces bulletins ont été
choisis sur 2019 pour couvrir le plus largement possible les différentes
éditions "Viticulture" disponibles.

Il contient 77 bulletins de santé du végétal.

## Viticulture
### Auvergne - Rhône-Alpes
#### Auvergne
- BSV_19-04-16_2_cle84b397 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_19-04-16_2_cle84b397.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_19-04-16_2_cle84b397.html)
stade debourrement, pousse verte
- BSV_19-06-25_12_cle0b3618 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_19-06-25_12_cle0b3618.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_19-06-25_12_cle0b3618.html)
- BSV_19-07-30_17_cle08d978 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_19-07-30_17_cle08d978.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_19-07-30_17_cle08d978.html)

#### Rhône-Alpes
- BSV-viti-RA-03_16-04-2019_cle0bbc57 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV-viti-RA-03_16-04-2019_cle0bbc57.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV-viti-RA-03_16-04-2019_cle0bbc57.html)
stade 02-03 Début gonflement du bourgeon, 05 Pointe verte, 07 1èrefeuille étalée, 06 Éclatement du bourgeon, 07-09 1-2 feuilles étalées, 09 2-3 feuilles étalées
- BSV-viti-RA-15_09-07-2019_cle01ba24 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV-viti-RA-15_09-07-2019_cle01ba24.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV-viti-RA-15_09-07-2019_cle01ba24.html)
- BSV-viti-RA-16_16-07-2019_cle0bfb92 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV-viti-RA-16_16-07-2019_cle0bfb92.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV-viti-RA-16_16-07-2019_cle0bfb92.html)

### Bourgogne-Franche-Comté
- BSV_1_-_16_avril_2019_cle43d31d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_1_-_16_avril_2019_cle43d31d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_1_-_16_avril_2019_cle43d31d.html)
stade: bourgeon dans le coton,  2,3  feuilles  étalées,  pointe  verte,  éclatement  du bourgeon
- BSV_12_-_2_juillet_2019_cle8252f7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_12_-_2_juillet_2019_cle8252f7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_12_-_2_juillet_2019_cle8252f7.html)
- BSV_16_-_30_juillet_2019_cle8caa6e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_16_-_30_juillet_2019_cle8caa6e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_16_-_30_juillet_2019_cle8caa6e.html)
- BSV_4_-_7_mai_2019_cle8db5ca [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_4_-_7_mai_2019_cle8db5ca.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_4_-_7_mai_2019_cle8db5ca.html)


### Centre - Val de Loire
- BSV_viti_01_du_16-04-19_cle878624 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_viti_01_du_16-04-19_cle878624.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_viti_01_du_16-04-19_cle878624.html)
- BSV_viti_12_du_02-07-19-1_cle05965f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_viti_12_du_02-07-19-1_cle05965f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_viti_12_du_02-07-19-1_cle05965f.html)
- BSV_viti_13_du_09-07-19_cle86811a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_viti_13_du_09-07-19_cle86811a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_viti_13_du_09-07-19_cle86811a.html)
- BSV_viti_16_du_16-09-19_cle87b836 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_viti_16_du_16-09-19_cle87b836.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_viti_16_du_16-09-19_cle87b836.html)

### Corse
- BSV_Viticulture_2_17_mai_2019_cle8f88af [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2019/BSV_Viticulture_2_17_mai_2019_cle8f88af.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2019/BSV_Viticulture_2_17_mai_2019_cle8f88af.html)
- BSV_Viticulture_5_18_juillet_2019_cle8b576d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2019/BSV_Viticulture_5_18_juillet_2019_cle8b576d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2019/BSV_Viticulture_5_18_juillet_2019_cle8b576d.html)

### Grand Est
#### Alsace
- alsace_vigne_no8_du_25-06-19_cle818164 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no8_du_25-06-19_cle818164.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no8_du_25-06-19_cle818164.html)
stade nouaison, petits pois
- alsace_vigne_no13_du_29-07-19_cle853dc6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no13_du_29-07-19_cle853dc6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no13_du_29-07-19_cle853dc6.html)
- alsace_vigne_no14_du_20-08-19_cle8ee257 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no14_du_20-08-19_cle8ee257.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no14_du_20-08-19_cle8ee257.html)


#### Lorraine
- 20190619_LOR_BSV_Viticulture_cle8c2fdf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190619_LOR_BSV_Viticulture_cle8c2fdf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190619_LOR_BSV_Viticulture_cle8c2fdf.html)
- 20190710_LOR_BSV_Viticulture_cle8bd128 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190710_LOR_BSV_Viticulture_cle8bd128.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190710_LOR_BSV_Viticulture_cle8bd128.html)
- 20190731_LOR_Viticulture-1_cle45ef1d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190731_LOR_Viticulture-1_cle45ef1d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190731_LOR_Viticulture-1_cle45ef1d.html)

#### Champagne
- BSV_2019_n13_cle0f596d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/BSV_2019_n13_cle0f596d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/BSV_2019_n13_cle0f596d.html)
- BSV_2019_n16_cle04a239 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/BSV_2019_n16_cle04a239.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/BSV_2019_n16_cle04a239.html)
- BSV_2019_n18_cle02fbf1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/BSV_2019_n18_cle02fbf1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/BSV_2019_n18_cle02fbf1.html)


### Nouvelle-Aquitaine
#### Charentes
- BSV_NA_VIGNE_CHARENTES_02_20190409_cle8ac81e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_02_20190409_cle8ac81e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_02_20190409_cle8ac81e.html)
stade 03 (bourgeon  dans  le  coton),  07 (première feuille étalée)
- BSV_NA_VIGNE_CHARENTES_07_20190514_cle841735 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_07_20190514_cle841735.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_07_20190514_cle841735.html)
- BSV_NA_VIGNE_CHARENTES_18_20190730_cle8d5827 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_18_20190730_cle8d5827.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_18_20190730_cle8d5827.html)
- BSV_NA_VIGNE_CHARENTES_19_20190806_cle88b155 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_19_20190806_cle88b155.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_19_20190806_cle88b155.html)
- BSV_NA_VIGNE_CHARENTES_22_20190903_cle8f1178 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_22_20190903_cle8f1178.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_CHARENTES_22_20190903_cle8f1178.html)

#### Haut-Poitou
- BSV_NA_VIGNE_Haut-Poitou_03_20190430_cle4165b7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Haut-Poitou_03_20190430_cle4165b7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Haut-Poitou_03_20190430_cle4165b7.html)
- BSV_NA_VIGNE_Haut-Poitou_13_20190709_cle44ed93 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Haut-Poitou_13_20190709_cle44ed93.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Haut-Poitou_13_20190709_cle44ed93.html)
- BSV_NA_VIGNE_Haut-Poitou_15_20190806_cle4c661d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Haut-Poitou_15_20190806_cle4c661d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Haut-Poitou_15_20190806_cle4c661d.html)

#### Limousin
- BSV_NA_VIGNE_Limousin_02_20190409_cle81e534 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Limousin_02_20190409_cle81e534.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Limousin_02_20190409_cle81e534.html)
- BSV_NA_VIGNE_Limousin_07_20190522_cle81f925 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Limousin_07_20190522_cle81f925.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Limousin_07_20190522_cle81f925.html)
- BSV_NA_VIGNE_Limousin_14_20190709_cle8d4f42 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Limousin_14_20190709_cle8d4f42.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Limousin_14_20190709_cle8d4f42.html)
- BSV_NA_VIGNE_Limousin_17_20190730_cle835848 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Limousin_17_20190730_cle835848.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Limousin_17_20190730_cle835848.html)

#### Nord-Aquitaine
- BSV_NA_VIGNE_Nord_Aquitaine_04_20190416_cle876cf5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_04_20190416_cle876cf5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_04_20190416_cle876cf5.html)
- BSV_NA_VIGNE_Nord_Aquitaine_07_20190507_cle8b2385 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_07_20190507_cle8b2385.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_07_20190507_cle8b2385.html)
- BSV_NA_VIGNE_Nord_Aquitaine_17_20190716_cle8d5da5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_17_20190716_cle8d5da5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_17_20190716_cle8d5da5.html)
- BSV_NA_VIGNE_Nord_Aquitaine_19_20190730_cle819684 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_19_20190730_cle819684.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_19_20190730_cle819684.html)
- BSV_NA_VIGNE_Nord_Aquitaine_20_20190806_cle891ea2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_20_20190806_cle891ea2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Nord_Aquitaine_20_20190806_cle891ea2.html)

#### Sud-Aquitaine
- BSV_NA_VIGNE_Sud_Aquitaine_02_20190401_01_cle4bdf4a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_02_20190401_01_cle4bdf4a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_02_20190401_01_cle4bdf4a.html)
- BSV_NA_VIGNE_Sud_Aquitaine_08_20190514_cle8e63bb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_08_20190514_cle8e63bb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_08_20190514_cle8e63bb.html)
- BSV_NA_VIGNE_Sud_Aquitaine_17_20190716_cle841a78 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_17_20190716_cle841a78.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_17_20190716_cle841a78.html)
- BSV_NA_VIGNE_Sud_Aquitaine_18_20190723_cle84e39a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_18_20190723_cle84e39a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_18_20190723_cle84e39a.html)
- BSV_NA_VIGNE_Sud_Aquitaine_20_20190806_cle831e2b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_20_20190806_cle831e2b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_20_20190806_cle831e2b.html)

### Occitanie
#### Raisin de table
- bsv_raisin_n07_14052019_cle85bd4c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_raisin_n07_14052019_cle85bd4c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_raisin_n07_14052019_cle85bd4c.html)
- bsv_raisin_n12_18062019_cle8578dc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_raisin_n12_18062019_cle8578dc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_raisin_n12_18062019_cle8578dc.html)
- bsv_raisin_n15_16072019_cle8c8617 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_raisin_n15_16072019_cle8c8617.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_raisin_n15_16072019_cle8c8617.html)

#### Languedoc-Roussillon
- bsv_viti_lr_n03_16042019_cle82d51a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_lr_n03_16042019_cle82d51a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_lr_n03_16042019_cle82d51a.html)
et « 2 ou 3 feuilles étalées » (stade 09 ou E ou BBCH 12-13).  « boutons floraux séparés » (stade 17 ou H ou BBCH 57).
- bsv_viti_lr_n06_06052019-1_cle4a8957-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_lr_n06_06052019-1_cle4a8957-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_lr_n06_06052019-1_cle4a8957-1.html)
- bsv_viti_lr_n17_30072019_cle8cc4c8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_lr_n17_30072019_cle8cc4c8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_lr_n17_30072019_cle8cc4c8.html)

#### Gascogne-St Mont-Madiran
- bsv_vigne_n17_gascogne_24072018_cle4238ae [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_vigne_n17_gascogne_24072018_cle4238ae.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_vigne_n17_gascogne_24072018_cle4238ae.html)
- bsv_vigne_n21_gascogne_11092018_cle4c45ec [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_vigne_n21_gascogne_11092018_cle4c45ec.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_vigne_n21_gascogne_11092018_cle4c45ec.html)

#### Aveyron
- bsv_viti_mp_aveyron_n02_19042019-2_cle8676eb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_aveyron_n02_19042019-2_cle8676eb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_aveyron_n02_19042019-2_cle8676eb.html)
- bsv_viti_mp_aveyron_n11_25062019_cle084a76 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_aveyron_n11_25062019_cle084a76.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_aveyron_n11_25062019_cle084a76.html)
- bsv_viti_mp_aveyron_n14_17072019_cle05cdbe [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_aveyron_n14_17072019_cle05cdbe.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_aveyron_n14_17072019_cle05cdbe.html)
- bsv_viti_mp_aveyron_n16_30072019_cle0dd53b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_aveyron_n16_30072019_cle0dd53b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_aveyron_n16_30072019_cle0dd53b.html)

#### Cahors - Lot
- bsv_viti_mp_cahors_n05_07052019-1_cle882e62 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_cahors_n05_07052019-1_cle882e62.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_cahors_n05_07052019-1_cle882e62.html)
- bsv_viti_mp_cahors_n12_25062019_cle4cf277 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_cahors_n12_25062019_cle4cf277.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_cahors_n12_25062019_cle4cf277.html)
- bsv_viti_mp_cahors_n17_30072019_cle434e53 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_cahors_n17_30072019_cle434e53.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_cahors_n17_30072019_cle434e53.html)
- bsv_viti_mp_cahors_n19_20082019_cle4e16b4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_cahors_n19_20082019_cle4e16b4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_cahors_n19_20082019_cle4e16b4.html)

#### Fronton - Tarn & Garonne
- bsv_viti_mp_fronton_tarnetgaronne_n04_24042019_cle43debd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_fronton_tarnetgaronne_n04_24042019_cle43debd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_fronton_tarnetgaronne_n04_24042019_cle43debd.html)
- bsv_viti_mp_fronton_tarnetgaronne_n13_25062019_cle48ca82 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_fronton_tarnetgaronne_n13_25062019_cle48ca82.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_fronton_tarnetgaronne_n13_25062019_cle48ca82.html)
- bsv_viti_mp_fronton_tarnetgaronne_n18_30072019_cle4d7faa [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_fronton_tarnetgaronne_n18_30072019_cle4d7faa.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_fronton_tarnetgaronne_n18_30072019_cle4d7faa.html)

#### Gaillac
- bsv_viti_mp_gaillac_n02_09042019_cle0bbe75 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_gaillac_n02_09042019_cle0bbe75.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_gaillac_n02_09042019_cle0bbe75.html)
- bsv_viti_mp_gaillac_n13_25062019_cle0e5f7f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_gaillac_n13_25062019_cle0e5f7f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_gaillac_n13_25062019_cle0e5f7f.html)
- bsv_viti_mp_gaillac_n18_30072019_cle0fe8aa [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_viti_mp_gaillac_n18_30072019_cle0fe8aa.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_viti_mp_gaillac_n18_30072019_cle0fe8aa.html)


### Pays de la Loire
- bsv_viti_3_18_04_2019_cle43e1c9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2019/bsv_viti_3_18_04_2019_cle43e1c9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2019/bsv_viti_3_18_04_2019_cle43e1c9.html)
- bsv_viti_13_27_06_2019_cle07f426 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2019/bsv_viti_13_27_06_2019_cle07f426.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2019/bsv_viti_13_27_06_2019_cle07f426.html)
- bsv_viti_14_04_07_2019-1_cle888dd3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2019/bsv_viti_14_04_07_2019-1_cle888dd3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2019/bsv_viti_14_04_07_2019-1_cle888dd3.html)
- bsv_viti_17_25_07_2019_cle0a86e1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2019/bsv_viti_17_25_07_2019_cle0a86e1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2019/bsv_viti_17_25_07_2019_cle0a86e1.html)

### Provence Alpes-Côte d’Azur
- BSVViti_20190409_N3_cle88a431 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/BSVViti_20190409_N3_cle88a431.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/BSVViti_20190409_N3_cle88a431.html)
- BSVViti_20190618_N16_cle091811 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/BSVViti_20190618_N16_cle091811.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/BSVViti_20190618_N16_cle091811.html)
- BSVViti_20190730_N22_cle019b19 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/BSVViti_20190730_N22_cle019b19.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/BSVViti_20190730_N22_cle019b19.html)
- BSVViti_20190820_N23_cle0c5111 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/BSVViti_20190820_N23_cle0c5111.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/BSVViti_20190820_N23_cle0c5111.html)

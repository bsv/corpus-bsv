# Corpus de test *Viticulture Alsace*

Ce corpus est composé des bulletins de santé du végétal publiés
entre 2011 et 2022 et qui concernent la viticulture en Alsace
(départements du Haut-Rhin et du Bas-Rhin).

Certains de ces bulletins ont été collectés de façon manuelle
durant le projet Vespa, les autres ont été collectés de façon
automatisée.

Année | Nombre de BSV
--- | ---:
2011 | 17
2012 | 13
2013 | 16
2014 | 17
2015 | 17
2016 | 17
2017 | 15
2018 | 15
2019 | 14
2020 | 16
2021 | 16
2022 | 14
*Total* | **187**

Certains de ces bulletins appartiennent aussi à d'autres corpus de test.
Dans ce cas, l'autre corpus de test est indiqué entre parenthèses.

## Les BSV
### 2011

- *18/04/2011* : 01_BSV_VIGNE_18042011_cle491792 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/01_BSV_VIGNE_18042011_cle491792.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/01_BSV_VIGNE_18042011_cle491792.html)]
- *28/04/2011* : 02_BSV_VIGNE_280411_cle8cf82b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/02_BSV_VIGNE_280411_cle8cf82b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/02_BSV_VIGNE_280411_cle8cf82b.html)]
- *03/05/2011* : 03_BSV_VIGNE_03052011_cle4b4de2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/03_BSV_VIGNE_03052011_cle4b4de2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/03_BSV_VIGNE_03052011_cle4b4de2.html)]
- *10/05/2011* : 04_BSV_VIGNE_10052011_cle439ac4 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/04_BSV_VIGNE_10052011_cle439ac4.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/04_BSV_VIGNE_10052011_cle439ac4.html)]
- *17/05/2011* : 05_BSV_VIGNE_17052011_cle4178c5 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/05_BSV_VIGNE_17052011_cle4178c5.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/05_BSV_VIGNE_17052011_cle4178c5.html)]
- *24/05/2011* : 06_BSV_VIGNE_24052011_cle4df684 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/06_BSV_VIGNE_24052011_cle4df684.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/06_BSV_VIGNE_24052011_cle4df684.html)]
- *31/05/2011* : 07_BSV_VIGNE_31052011_cle4a37fc [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/07_BSV_VIGNE_31052011_cle4a37fc.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/07_BSV_VIGNE_31052011_cle4a37fc.html)]
- *07/06/2011* : 08_BSV_VIGNE_07062011_cle4ffe7b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/08_BSV_VIGNE_07062011_cle4ffe7b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/08_BSV_VIGNE_07062011_cle4ffe7b.html)]
- *14/06/2011* : 09_BSV_VIGNE_14062011_cle44b7c3 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/09_BSV_VIGNE_14062011_cle44b7c3.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/09_BSV_VIGNE_14062011_cle44b7c3.html)]
- *21/06/2011* : 10_BSV_VIGNE_21062011_cle4ff9bf [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/10_BSV_VIGNE_21062011_cle4ff9bf.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/10_BSV_VIGNE_21062011_cle4ff9bf.html)]
- *28/06/2011* : 11_BSV_VIGNE_28062011_cle47e6ed [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/11_BSV_VIGNE_28062011_cle47e6ed.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/11_BSV_VIGNE_28062011_cle47e6ed.html)]
- *05/07/2011* : 12_BSV_VIGNE_05072011_cle4a112d [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/12_BSV_VIGNE_05072011_cle4a112d.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/12_BSV_VIGNE_05072011_cle4a112d.html)]
- *12/07/2011* : 13_BSV_VIGNE_12072011_cle4c11d5 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/13_BSV_VIGNE_12072011_cle4c11d5.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/13_BSV_VIGNE_12072011_cle4c11d5.html)]
- *18/07/2011* : 14_BSV_VIGNE_18072011_cle48b659 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/14_BSV_VIGNE_18072011_cle48b659.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/14_BSV_VIGNE_18072011_cle48b659.html)]
- *26/07/2011* : 15_BSV_VIGNE_26072011_cle45466d [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/15_BSV_VIGNE_26072011_cle45466d.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/15_BSV_VIGNE_26072011_cle45466d.html)]
- *26/08/2011* : 16_BSV_VIGNE_26082011_cle416bdf [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/16_BSV_VIGNE_26082011_cle416bdf.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/16_BSV_VIGNE_26082011_cle416bdf.html)]
- *03/11/2011* : 17_BSV_VIGNE_03112011_cle4c64d5 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/17_BSV_VIGNE_03112011_cle4c64d5.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/17_BSV_VIGNE_03112011_cle4c64d5.html)]

### 2012

- *07/05/2012* : 01_BSV_VIGNE_07052012_cle456ecf [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/01_BSV_VIGNE_07052012_cle456ecf.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/01_BSV_VIGNE_07052012_cle456ecf.html)]
- *15/05/2012* : 02_BSV_VIGNE_15052012_cle4b5d6a [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/02_BSV_VIGNE_15052012_cle4b5d6a.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/02_BSV_VIGNE_15052012_cle4b5d6a.html)]
- *22/05/2012* : 03_BSV_VIGNE_22052012_cle481f26 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/03_BSV_VIGNE_22052012_cle481f26.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/03_BSV_VIGNE_22052012_cle481f26.html)]
- *29/05/2012* : 04_BSV_VIGNE_29052012_cle4b5a67 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/04_BSV_VIGNE_29052012_cle4b5a67.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/04_BSV_VIGNE_29052012_cle4b5a67.html)]
- *05/06/2012* : 05_BSV_VIGNE_05062012_cle4f87e7 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/05_BSV_VIGNE_05062012_cle4f87e7.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/05_BSV_VIGNE_05062012_cle4f87e7.html)]
- *12/06/2012* : 06_BSV_VIGNE_12062012_cle4f1d2a [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/06_BSV_VIGNE_12062012_cle4f1d2a.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/06_BSV_VIGNE_12062012_cle4f1d2a.html)]
- *19/06/2012* : 07_BSV_VIGNE_19062012_cle4eeb69 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/07_BSV_VIGNE_19062012_cle4eeb69.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/07_BSV_VIGNE_19062012_cle4eeb69.html)]
- *26/06/2012* : 08_BSV_VIGNE_26062012_cle45b44f [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/08_BSV_VIGNE_26062012_cle45b44f.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/08_BSV_VIGNE_26062012_cle45b44f.html)]
- *03/07/2012* : 09_BSV_VIGNE_03072012_cle4f1a1b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/09_BSV_VIGNE_03072012_cle4f1a1b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/09_BSV_VIGNE_03072012_cle4f1a1b.html)]
- *09/07/2012* : 10_BSV_VIGNE_09072012_cle4eaea1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/10_BSV_VIGNE_09072012_cle4eaea1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/10_BSV_VIGNE_09072012_cle4eaea1.html)]
- *17/07/2012* : 11_BSV_VIGNE_17072012_cle4d2944 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/11_BSV_VIGNE_17072012_cle4d2944.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/11_BSV_VIGNE_17072012_cle4d2944.html)]
- *24/07/2012* : 12_BSV_VIGNE_24072012_cle4bbd41 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/12_BSV_VIGNE_24072012_cle4bbd41.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/12_BSV_VIGNE_24072012_cle4bbd41.html)]
- *06/09/2012* : 13_BSV_VIGNE_06092012_cle4192df [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/13_BSV_VIGNE_06092012_cle4192df.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/13_BSV_VIGNE_06092012_cle4192df.html)]

### 2013

- *30/04/2013* : 01_BSV_VIGNE_30042013_cle4ad591 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/01_BSV_VIGNE_30042013_cle4ad591.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/01_BSV_VIGNE_30042013_cle4ad591.html)]
- *07/05/2013* : 02_BSV_VIGNE_07052013_cle42d13e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/02_BSV_VIGNE_07052013_cle42d13e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/02_BSV_VIGNE_07052013_cle42d13e.html)]
- *14/05/2013* : 03_BSV_VIGNE_14052013_cle451d47 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/03_BSV_VIGNE_14052013_cle451d47.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/03_BSV_VIGNE_14052013_cle451d47.html)]
- *21/05/2013* : 04_BSV_VIGNE_21052013_cle431731 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/04_BSV_VIGNE_21052013_cle431731.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/04_BSV_VIGNE_21052013_cle431731.html)]
- *28/05/2013* : 05_BSV_VIGNE_28052013_cle4b1112 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/05_BSV_VIGNE_28052013_cle4b1112.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/05_BSV_VIGNE_28052013_cle4b1112.html)]
- *04/06/2013* : 06_BSV_VIGNE_04062013_cle41f7e8 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/06_BSV_VIGNE_04062013_cle41f7e8.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/06_BSV_VIGNE_04062013_cle41f7e8.html)]
- *11/06/2013* : 07_BSV_VIGNE_11062013_cle43af58 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/07_BSV_VIGNE_11062013_cle43af58.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/07_BSV_VIGNE_11062013_cle43af58.html)]
- *18/06/2013* : 08_BSV_VIGNE_18062013_cle46f8bc [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/08_BSV_VIGNE_18062013_cle46f8bc.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/08_BSV_VIGNE_18062013_cle46f8bc.html)]
- *25/06/2013* : 09_BSV_VIGNE_25062013_cle4daee4 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/09_BSV_VIGNE_25062013_cle4daee4.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/09_BSV_VIGNE_25062013_cle4daee4.html)]
- *02/07/2013* : 10_BSV_VIGNE_02072013_cle4a337b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/10_BSV_VIGNE_02072013_cle4a337b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/10_BSV_VIGNE_02072013_cle4a337b.html)]
- *09/07/2013* : 11_BSV_VIGNE_09072013_cle486a11 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/11_BSV_VIGNE_09072013_cle486a11.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/11_BSV_VIGNE_09072013_cle486a11.html)]
- *16/07/2013* : 12_BSV_VIGNE_16072013_cle4cde31 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/12_BSV_VIGNE_16072013_cle4cde31.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/12_BSV_VIGNE_16072013_cle4cde31.html)]
- *23/07/2013* : 13_BSV_VIGNE_23072013_cle4c91ac [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/13_BSV_VIGNE_23072013_cle4c91ac.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/13_BSV_VIGNE_23072013_cle4c91ac.html)]
- *30/07/2013* : 14_BSV_VIGNE_30072013_cle4c24c3 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/14_BSV_VIGNE_30072013_cle4c24c3.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/14_BSV_VIGNE_30072013_cle4c24c3.html)]
- *29/08/2013* : 15_BSV_VIGNE_29082013_cle41e916 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/15_BSV_VIGNE_29082013_cle41e916.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/15_BSV_VIGNE_29082013_cle41e916.html)]
- *20/09/2013* : 16_BSV_VIGNE_20092013_cle46b5cc [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/16_BSV_VIGNE_20092013_cle46b5cc.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/16_BSV_VIGNE_20092013_cle46b5cc.html)]

### 2014

- *01/04/2014* : 01_BSV_VIGNE_010414_cle89858e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/01_BSV_VIGNE_010414_cle89858e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/01_BSV_VIGNE_010414_cle89858e.html)]
- *16/04/2014* : 02_BSV_VIGNE_16042014_cle46a463 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/02_BSV_VIGNE_16042014_cle46a463.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/02_BSV_VIGNE_16042014_cle46a463.html)]
- *30/04/2014* : 03_BSV_VIGNE_30042014_cle453f72 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/03_BSV_VIGNE_30042014_cle453f72.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/03_BSV_VIGNE_30042014_cle453f72.html)]
- *06/05/2014* : 04_BSV_VIGNE_060514_cle81a3a9 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/04_BSV_VIGNE_060514_cle81a3a9.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/04_BSV_VIGNE_060514_cle81a3a9.html)]
- *13/05/2014* : 05_BSV_VIGNE_130514_cle8e2d6a [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/05_BSV_VIGNE_130514_cle8e2d6a.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/05_BSV_VIGNE_130514_cle8e2d6a.html)]
- *20/05/2014* : 06_BSV_VIGNE_200514_cle8b29ec [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/06_BSV_VIGNE_200514_cle8b29ec.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/06_BSV_VIGNE_200514_cle8b29ec.html)]
- *27/05/2014* : 07_BSV_VIGNE_270514_cle8aa36e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/07_BSV_VIGNE_270514_cle8aa36e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/07_BSV_VIGNE_270514_cle8aa36e.html)]
- *03/06/2014* : 08_BSV_VIGNE_030614_cle83454b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/08_BSV_VIGNE_030614_cle83454b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/08_BSV_VIGNE_030614_cle83454b.html)]
- *10/06/2014* : 09_BSV_VIGNE_100614_cle89279d [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/09_BSV_VIGNE_100614_cle89279d.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/09_BSV_VIGNE_100614_cle89279d.html)]
- *17/06/2014* : 10_BSV_VIGNE_170614_cle86dfc7 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/10_BSV_VIGNE_170614_cle86dfc7.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/10_BSV_VIGNE_170614_cle86dfc7.html)]
- *24/06/2014* : 11_BSV_VIGNE_240614_cle8cd419 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/11_BSV_VIGNE_240614_cle8cd419.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/11_BSV_VIGNE_240614_cle8cd419.html)]
- *30/06/2014* : 12_BSV_VIGNE_300614_cle8f335b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/12_BSV_VIGNE_300614_cle8f335b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/12_BSV_VIGNE_300614_cle8f335b.html)]
- *15/07/2014* : 14_BSV_VIGNE_150714_cle8e8ae5 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/14_BSV_VIGNE_150714_cle8e8ae5.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/14_BSV_VIGNE_150714_cle8e8ae5.html)]
- *22/07/2014* : 15_BSV_VIGNE_220714_cle8186e1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/15_BSV_VIGNE_220714_cle8186e1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/15_BSV_VIGNE_220714_cle8186e1.html)]
- *29/07/2014* : 16_BSV_VIGNE_290714_cle8753fa [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/16_BSV_VIGNE_290714_cle8753fa.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/16_BSV_VIGNE_290714_cle8753fa.html)]
- *02/09/2014* : 17_BSV_VIGNE_020914_cle8d5766 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/17_BSV_VIGNE_020914_cle8d5766.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/17_BSV_VIGNE_020914_cle8d5766.html)]
- *23/09/2014* : 18_BSV_VIGNE_230914_cle8eeae6 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/18_BSV_VIGNE_230914_cle8eeae6.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/18_BSV_VIGNE_230914_cle8eeae6.html)]

### 2015

- *17/04/2015* : 01_VITI_Als_cle419ae8 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/01_VITI_Als_cle419ae8.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/01_VITI_Als_cle419ae8.html)]
- *23/04/2015* : 02_VITI_Als_cle44d4b1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/02_VITI_Als_cle44d4b1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/02_VITI_Als_cle44d4b1.html)]
- *05/05/2015* : 03_VITI_Als_cle4e22cf [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/03_VITI_Als_cle4e22cf.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/03_VITI_Als_cle4e22cf.html)]
- *12/05/2015* : 04_VITI_Als_cle4b751c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/04_VITI_Als_cle4b751c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/04_VITI_Als_cle4b751c.html)]
- *19/05/2015* : 05_VITI_Als_cle4dbea9 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/05_VITI_Als_cle4dbea9.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/05_VITI_Als_cle4dbea9.html)]
- *26/05/2015* : 06_VITI_Als_cle447656 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/06_VITI_Als_cle447656.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/06_VITI_Als_cle447656.html)]
- *02/06/2015* : 07_VITI_Als_cle4e6cf2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/07_VITI_Als_cle4e6cf2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/07_VITI_Als_cle4e6cf2.html)]
- *09/06/2015* : 08_VITI_Als_cle42b12d [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/08_VITI_Als_cle42b12d.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/08_VITI_Als_cle42b12d.html)]
- *16/06/2015* : 09_VITI_Als_cle425e45 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/09_VITI_Als_cle425e45.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/09_VITI_Als_cle425e45.html)]
- *23/06/2015* : 10_VITI_Als_cle471732 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/10_VITI_Als_cle471732.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/10_VITI_Als_cle471732.html)]
- *30/06/2015* : 11_VITI_Als_cle4ea8d4 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/11_VITI_Als_cle4ea8d4.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/11_VITI_Als_cle4ea8d4.html)]
- *07/07/2015* : 12_VITI_Als_cle4511bc [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/12_VITI_Als_cle4511bc.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/12_VITI_Als_cle4511bc.html)]
- *15/07/2015* : 13_VITI_Als_cle494b1a [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/13_VITI_Als_cle494b1a.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/13_VITI_Als_cle494b1a.html)]
- *20/07/2015* : 14_VITI_Als_cle4d95fd [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/14_VITI_Als_cle4d95fd.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/14_VITI_Als_cle4d95fd.html)]
- *28/07/2015* : 15_VITI_Als_cle411ca2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/15_VITI_Als_cle411ca2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/15_VITI_Als_cle411ca2.html)]
- *21/08/2015* : 16_VITI_Als_cle49f763 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/16_VITI_Als_cle49f763.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/16_VITI_Als_cle49f763.html)]
- *09/09/2015* : 17_VITI_Als_cle471787 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/17_VITI_Als_cle471787.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/17_VITI_Als_cle471787.html)]

### 2016

- *08/04/2016* : 01_BSV_VIGNE_080416_cle8e3e8c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/01_BSV_VIGNE_080416_cle8e3e8c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/01_BSV_VIGNE_080416_cle8e3e8c.html)]
- *29/04/2016* : als_vit_no2_2016-04-29_cle03ae1c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no2_2016-04-29_cle03ae1c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no2_2016-04-29_cle03ae1c.html)]
- *03/05/2016* : als_vit_no3_2016-05-03_cle08c6da [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no3_2016-05-03_cle08c6da.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no3_2016-05-03_cle08c6da.html)]
- *10/05/2016* : als_vit_no4_2016-05-10_cle076b2b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no4_2016-05-10_cle076b2b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no4_2016-05-10_cle076b2b.html)]
- *17/05/2016* : als_vit_no5_2016-05-17_cle0be11c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no5_2016-05-17_cle0be11c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no5_2016-05-17_cle0be11c.html)]
- *24/05/2016* : als_vit_no6_2016-05-24_cle0d7b38 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no6_2016-05-24_cle0d7b38.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no6_2016-05-24_cle0d7b38.html)]
- *31/05/2016* : als_vit_no7_2016-05-31_cle0169fc [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no7_2016-05-31_cle0169fc.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no7_2016-05-31_cle0169fc.html)]
- *07/06/2016* : als_vit_no8_2016-06-07_cle0a1d17 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no8_2016-06-07_cle0a1d17.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no8_2016-06-07_cle0a1d17.html)]
- *14/06/2016* : als_vit_no9_2016-06-14_cle0e23d2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no9_2016-06-14_cle0e23d2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no9_2016-06-14_cle0e23d2.html)]
- *21/06/2016* : als_vit_no10_2016-06-21_cle8ce13c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no10_2016-06-21_cle8ce13c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no10_2016-06-21_cle8ce13c.html)]
- *28/06/2016* : als_vit_no11_2016-06-28_cle8b37c9 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no11_2016-06-28_cle8b37c9.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no11_2016-06-28_cle8b37c9.html)]
- *05/07/2016* : als_vit_no12_2016-07-05_cle8d81f4 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no12_2016-07-05_cle8d81f4.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no12_2016-07-05_cle8d81f4.html)]
- *12/07/2016* : als_vit_no13_2016-07-12_cle815352 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no13_2016-07-12_cle815352.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no13_2016-07-12_cle815352.html)]
- *19/07/2016* : als_vit_no14_2016-07-19_cle833e73 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no14_2016-07-19_cle833e73.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no14_2016-07-19_cle833e73.html)]
- *26/07/2016* : als_vit_no15_2016-07-26_cle879161 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no15_2016-07-26_cle879161.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no15_2016-07-26_cle879161.html)]
- *18/08/2016* : als_vit_no16_2016-07-18_cle81ea31 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no16_2016-07-18_cle81ea31.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no16_2016-07-18_cle81ea31.html)]
- *29/08/2016* : als_vit_no17_2016-08-29_cle8e3e14 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_vit_no17_2016-08-29_cle8e3e14.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_vit_no17_2016-08-29_cle8e3e14.html)]

### 2017

- *04/04/2017* : alsace_vigne_no1_du_04-04-17_cle86283c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no1_du_04-04-17_cle86283c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no1_du_04-04-17_cle86283c.html)]
- *19/04/2017* : alsace_vigne_no2_du_19-04-17_cle8f1ebb [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no2_du_19-04-17_cle8f1ebb.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no2_du_19-04-17_cle8f1ebb.html)]
- *25/04/2017* : alsace03_BSV_VIGNE_250417_cle0358f2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace03_BSV_VIGNE_250417_cle0358f2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace03_BSV_VIGNE_250417_cle0358f2.html)] *(test_alea)*
- *03/05/2017* : alsace_vigne_no4_du_03-05-17_cle83b2b8 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no4_du_03-05-17_cle83b2b8.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no4_du_03-05-17_cle83b2b8.html)]
- *10/05/2017* : alsace_vigne_no5_du_10-05-17_cle811ec1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no5_du_10-05-17_cle811ec1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no5_du_10-05-17_cle811ec1.html)]
- *16/05/2017* : alsace_vigne_no6_du_16-05-17_cle8eb3bd [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no6_du_16-05-17_cle8eb3bd.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no6_du_16-05-17_cle8eb3bd.html)]
- *24/05/2017* : alsace_vigne_no7_du_24-05-17_cle81abe3 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no7_du_24-05-17_cle81abe3.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no7_du_24-05-17_cle81abe3.html)]
- *30/05/2017* : alsace_vigne_no8_du_30-05-17_cle8ba615 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no8_du_30-05-17_cle8ba615.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no8_du_30-05-17_cle8ba615.html)]
- *06/06/2017* : alsace_vigne_no9_du_06-06-17_cle84f7e9 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no9_du_06-06-17_cle84f7e9.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no9_du_06-06-17_cle84f7e9.html)]
- *13/06/2017* : alsace_vigne_no10_du_13-06-17_cle813fa7 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no10_du_13-06-17_cle813fa7.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no10_du_13-06-17_cle813fa7.html)]
- *20/06/2017* : alsace_vigne_no11_du_20-06-17_cle883572 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no11_du_20-06-17_cle883572.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no11_du_20-06-17_cle883572.html)]
- *27/06/2017* : alsace_vigne_no12_du_27-06-17_cle8993e8 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no12_du_27-06-17_cle8993e8.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no12_du_27-06-17_cle8993e8.html)]
- *04/07/2017* : alsace_vigne_no13_du_04-07-17_cle8d13ab [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no13_du_04-07-17_cle8d13ab.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no13_du_04-07-17_cle8d13ab.html)]
- *11/07/2017* : alsace_vigne_no14_du_11-07-17_cle8a64bc-1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no14_du_11-07-17_cle8a64bc-1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no14_du_11-07-17_cle8a64bc-1.html)]
- *18/07/2017* : alsace_vigne_no15_du_18-07-17_cle821946 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace_vigne_no15_du_18-07-17_cle821946.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace_vigne_no15_du_18-07-17_cle821946.html)]

### 2018

- *17/04/2018* : alsace_viti_n1_du_18-04-18_cle4a9772 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n1_du_18-04-18_cle4a9772.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n1_du_18-04-18_cle4a9772.html)]
- *02/05/2018* : alsace_viti_n2_du_02-05-18_cle4ea39c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n2_du_02-05-18_cle4ea39c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n2_du_02-05-18_cle4ea39c.html)]
- *07/05/2018* : alsace_viti_n3_du_07-05-18_cle4f8a33 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n3_du_07-05-18_cle4f8a33.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n3_du_07-05-18_cle4f8a33.html)]
- *15/05/2018* : alsace_viti_n4_du_16-05-18_cle4ee636 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n4_du_16-05-18_cle4ee636.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n4_du_16-05-18_cle4ee636.html)]
- *22/05/2018* : alsace_viti_n5_du_23-05-18_cle4cc594 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n5_du_23-05-18_cle4cc594.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n5_du_23-05-18_cle4cc594.html)]
- *29/05/2018* : alsace_viti_n6_du_30-05-18_cle4f9de4 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n6_du_30-05-18_cle4f9de4.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n6_du_30-05-18_cle4f9de4.html)]
- *05/06/2018* : alsace_viti_n7_du_06-06-18_cle4bd91b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n7_du_06-06-18_cle4bd91b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n7_du_06-06-18_cle4bd91b.html)]
- *12/06/2018* : alsace_viti_n8_du_12-06-18_cle43b366 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n8_du_12-06-18_cle43b366.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n8_du_12-06-18_cle43b366.html)]
- *19/06/2018* : alsace_viti_n9_du_21-06-18_cle4115c1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n9_du_21-06-18_cle4115c1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n9_du_21-06-18_cle4115c1.html)]
- *22/06/2018* : alsace_viti_n10_du_25-06-18_cle0b2bd1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n10_du_25-06-18_cle0b2bd1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n10_du_25-06-18_cle0b2bd1.html)]
- *25/06/2018* : alsace_viti_n11_du_25-06-18_cle0a98ab [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n11_du_25-06-18_cle0a98ab.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n11_du_25-06-18_cle0a98ab.html)]
- *03/07/2018* : alsace_viti_n12_du_03-07-18_cle07af54 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n12_du_03-07-18_cle07af54.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n12_du_03-07-18_cle07af54.html)]
- *10/07/2018* : alsace_viti_n13_du_10-07-18_cle0fa2c9 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n13_du_10-07-18_cle0fa2c9.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n13_du_10-07-18_cle0fa2c9.html)]
- *24/08/2018* : alsace_viti_n16_du_24-08-18_cle06544a [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n16_du_24-08-18_cle06544a.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n16_du_24-08-18_cle06544a.html)]
- *20/12/2018* : alsace_viti_n17_du_20-12-18-bilan_cle8861c9 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/alsace_viti_n17_du_20-12-18-bilan_cle8861c9.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/alsace_viti_n17_du_20-12-18-bilan_cle8861c9.html)]

### 2019

- *07/05/2019* : alsace_vigne_no1_du_07-05-19_cle8f17f1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no1_du_07-05-19_cle8f17f1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no1_du_07-05-19_cle8f17f1.html)]
- *14/05/2019* : alsace_vigne_no2_du_14-05-19_cle878a6e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no2_du_14-05-19_cle878a6e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no2_du_14-05-19_cle878a6e.html)]
- *21/05/2019* : alsace_vigne_no3_du_22-05-19_cle81b562 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no3_du_22-05-19_cle81b562.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no3_du_22-05-19_cle81b562.html)]
- *27/05/2019* : alsace_vigne_no4_du_27-05-19_cle82db9e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no4_du_27-05-19_cle82db9e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no4_du_27-05-19_cle82db9e.html)]
- *04/06/2019* : alsace_vigne_no5_du_04-06-19_cle8de69d [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no5_du_04-06-19_cle8de69d.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no5_du_04-06-19_cle8de69d.html)]
- *11/06/2019* : alsace_vigne_no6_du_11-06-19_cle8dd29f [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no6_du_11-06-19_cle8dd29f.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no6_du_11-06-19_cle8dd29f.html)]
- *18/06/2019* : alsace_vigne_no7_du_18-06-19_cle81858d [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no7_du_18-06-19_cle81858d.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no7_du_18-06-19_cle81858d.html)]
- *25/06/2019* : alsace_vigne_no8_du_25-06-19_cle818164 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no8_du_25-06-19_cle818164.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no8_du_25-06-19_cle818164.html)] *(test_d2kab)*
- *02/07/2019* : alsace_vigne_no9_du_02-07-19_cle814cc1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no9_du_02-07-19_cle814cc1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no9_du_02-07-19_cle814cc1.html)]
- *09/07/2019* : alsace_vigne_no10_du_09-07-19_cle82f834 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no10_du_09-07-19_cle82f834.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no10_du_09-07-19_cle82f834.html)]
- *15/07/2019* : alsace_vigne_no11_du_15-07-19_cle84bd4e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no11_du_15-07-19_cle84bd4e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no11_du_15-07-19_cle84bd4e.html)]
- *22/07/2019* : alsace_vigne_no12_du_22-07-19_cle8b1197 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no12_du_22-07-19_cle8b1197.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no12_du_22-07-19_cle8b1197.html)]
- *29/07/2019* : alsace_vigne_no13_du_29-07-19_cle853dc6 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no13_du_29-07-19_cle853dc6.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no13_du_29-07-19_cle853dc6.html)] *(test_d2kab)*
- *20/08/2019* : alsace_vigne_no14_du_20-08-19_cle8ee257 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/alsace_vigne_no14_du_20-08-19_cle8ee257.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/alsace_vigne_no14_du_20-08-19_cle8ee257.html)] *(test_d2kab)*

### 2020

- *13/03/2020* : alsace_vigne_no15_13-03-2020_cle8edd7e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no15_13-03-2020_cle8edd7e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no15_13-03-2020_cle8edd7e.html)]
- *21/04/2020* : BSV01_VITI_ALS_S17_2020_cle8e3d75 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/BSV01_VITI_ALS_S17_2020_cle8e3d75.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/BSV01_VITI_ALS_S17_2020_cle8e3d75.html)]
- *28/04/2020* : BSV02_VITI_ALS_S18_2020_cle8633ca [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/BSV02_VITI_ALS_S18_2020_cle8633ca.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/BSV02_VITI_ALS_S18_2020_cle8633ca.html)]
- *05/05/2020* : BSV03_VITI_ALS_S19_2020_cle81c1b3 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/BSV03_VITI_ALS_S19_2020_cle81c1b3.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/BSV03_VITI_ALS_S19_2020_cle81c1b3.html)]
- *12/05/2020* : N04_1205_BSV2020_VITI_ALS_cle066daf [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/N04_1205_BSV2020_VITI_ALS_cle066daf.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/N04_1205_BSV2020_VITI_ALS_cle066daf.html)]
- *19/05/2020* : alsace_vigne_no5_du_19-05-20_cle816531 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no5_du_19-05-20_cle816531.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no5_du_19-05-20_cle816531.html)]
- *26/05/2020* : alsace_vigne_no6_du_26-05-20_cle8e542a [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no6_du_26-05-20_cle8e542a.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no6_du_26-05-20_cle8e542a.html)]
- *03/06/2020* : alsace_vigne_no7_du_03-06-20_cle8ad13b [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no7_du_03-06-20_cle8ad13b.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no7_du_03-06-20_cle8ad13b.html)]
- *09/06/2020* : alsace_vigne_no8_du_10-06-20_cle84691d [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no8_du_10-06-20_cle84691d.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no8_du_10-06-20_cle84691d.html)]
- *16/06/2020* : alsace_vigne_no9_du_16-06-20_cle894b7a [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no9_du_16-06-20_cle894b7a.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no9_du_16-06-20_cle894b7a.html)]
- *23/06/2020* : alsace_vigne_no10_du_23-06-20_cle812596 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no10_du_23-06-20_cle812596.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no10_du_23-06-20_cle812596.html)] *(test_alea)*
- *30/06/2020* : alsace_vigne_no11_du_30-06-20_cle8fbbae [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no11_du_30-06-20_cle8fbbae.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no11_du_30-06-20_cle8fbbae.html)]
- *07/07/2020* : alsace_vigne_no12_du_07-07-20_cle8dfe5c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no12_du_07-07-20_cle8dfe5c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no12_du_07-07-20_cle8dfe5c.html)]
- *15/07/2020* : alsace_vigne_no13_du_15-07-20_cle8938f1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no13_du_15-07-20_cle8938f1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no13_du_15-07-20_cle8938f1.html)]
- *21/07/2020* : alsace_vigne_no14_du_21-07-20_cle88e185 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no14_du_21-07-20_cle88e185.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no14_du_21-07-20_cle88e185.html)]
- *24/11/2020* : alsace_vigne_no15_du_24-11-20-bilan-sanitaire_cle024975 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no15_du_24-11-20-bilan-sanitaire_cle024975.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no15_du_24-11-20-bilan-sanitaire_cle024975.html)]

### 2021

- *21/04/2021* : alsace_vigne_no1_du_21-04-21_cle81df2c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no1_du_21-04-21_cle81df2c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no1_du_21-04-21_cle81df2c.html)]
- *04/05/2021* : BSV02_VITI_ALS_S18_2021_cle85fdc2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSV02_VITI_ALS_S18_2021_cle85fdc2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSV02_VITI_ALS_S18_2021_cle85fdc2.html)]
- *12/05/2021* : BSV03_VITI_ALS_S19_2021_cle8198e1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSV03_VITI_ALS_S19_2021_cle8198e1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSV03_VITI_ALS_S19_2021_cle8198e1.html)]
- *19/05/2021* : BSV04_VITI_ALS_S20_2021-Aln4_cle8f3981 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSV04_VITI_ALS_S20_2021-Aln4_cle8f3981.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSV04_VITI_ALS_S20_2021-Aln4_cle8f3981.html)]
- *27/05/2021* : alsace_vigne_no5_du_27-05-21_cle8bc39d [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no5_du_27-05-21_cle8bc39d.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no5_du_27-05-21_cle8bc39d.html)]
- *01/06/2021* : alsace_vigne_no6_du_1-06-21_cle0c613c [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no6_du_1-06-21_cle0c613c.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no6_du_1-06-21_cle0c613c.html)]
- *09/06/2021* : alsace_vigne_no7_du_9-06-21_cle0aa174 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no7_du_9-06-21_cle0aa174.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no7_du_9-06-21_cle0aa174.html)]
- *16/06/2021* : alsace_vigne_no8_du_16-06-21_cle8d649e [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no8_du_16-06-21_cle8d649e.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no8_du_16-06-21_cle8d649e.html)]
- *23/06/2021* : BSV09_VITI_ALS_S25_2021-n9_cle4fa191 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSV09_VITI_ALS_S25_2021-n9_cle4fa191.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSV09_VITI_ALS_S25_2021-n9_cle4fa191.html)]
- *30/06/2021* : BSV10_VITI_ALS_S26_2021n10_cle44a1a1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSV10_VITI_ALS_S26_2021n10_cle44a1a1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSV10_VITI_ALS_S26_2021n10_cle44a1a1.html)]
- *07/07/2021* : alsace_vigne_no11_du_07-07-21_cle8fe768 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no11_du_07-07-21_cle8fe768.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no11_du_07-07-21_cle8fe768.html)]
- *13/07/2021* : alsace_vigne_no12_du_13-07-21_cle881525 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no12_du_13-07-21_cle881525.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no12_du_13-07-21_cle881525.html)]
- *21/07/2021* : alsace_vigne_no13_du_21-07-21_cle8e3ea2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no13_du_21-07-21_cle8e3ea2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no13_du_21-07-21_cle8e3ea2.html)]
- *27/07/2021* : alsace_vigne_no14_du_27-07-21_cle883711 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no14_du_27-07-21_cle883711.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no14_du_27-07-21_cle883711.html)]
- *18/08/2021* : alsace_vigne_no15_du_18-08-21_cle8e265a [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/alsace_vigne_no15_du_18-08-21_cle8e265a.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/alsace_vigne_no15_du_18-08-21_cle8e265a.html)]
- *15/12/2021* : BSVBilan_Vigne_ALS_2021_cle8bbbec [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2021/BSVBilan_Vigne_ALS_2021_cle8bbbec.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2021/BSVBilan_Vigne_ALS_2021_cle8bbbec.html)] *(test_bviti21)*

### 2022

- *27/04/2022* : alsace_viticulture_no1_du_27-04-22_cle84ca33 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no1_du_27-04-22_cle84ca33.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no1_du_27-04-22_cle84ca33.html)]
- *03/05/2022* : alsace_viticulture_no2_du_03-05-22_cle848b13 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no2_du_03-05-22_cle848b13.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no2_du_03-05-22_cle848b13.html)]
- *11/05/2022* : alsace_viticulture_no3_du_11-05-22_cle8664f7 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no3_du_11-05-22_cle8664f7.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no3_du_11-05-22_cle8664f7.html)]
- *18/05/2022* : alsace_viticulture_no4_du_18-05-22_cle86d7dd [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no4_du_18-05-22_cle86d7dd.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no4_du_18-05-22_cle86d7dd.html)]
- *25/05/2022* : alsace_viticulture_no5_du_25-05-22 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no5_du_25-05-22.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no5_du_25-05-22.html)]
- *01/06/2022* : alsace_viticulture_no6_du_01-06-22 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no6_du_01-06-22.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no6_du_01-06-22.html)]
- *09/06/2022* : alsace_viticulture_no7_du_09-06-22-2 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no7_du_09-06-22-2.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no7_du_09-06-22-2.html)]
- *14/06/2022* : alsace_viticulture_no8_du_14-06-22 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no8_du_14-06-22.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no8_du_14-06-22.html)]
- *21/06/2022* : alsace_viticulture_no9_du_22-06-22 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no9_du_22-06-22.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no9_du_22-06-22.html)]
- *28/06/2022* : alsace_viticulture_no10_du_29-06-22 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no10_du_29-06-22.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no10_du_29-06-22.html)]
- *05/07/2022* : alsace_viticulture_no11_du_06-07-22 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/alsace_viticulture_no11_du_06-07-22.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/alsace_viticulture_no11_du_06-07-22.html)]
- *12/07/2022* : bsv12_viti_als_s28_2022 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/bsv12_viti_als_s28_2022.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/bsv12_viti_als_s28_2022.html)]
- *20/07/2022* : bsv13_viti_als_s29_2022-1 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/bsv13_viti_als_s29_2022-1.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/bsv13_viti_als_s29_2022-1.html)]
- *07/12/2022* : bsvbilan_viti_als_2022 [[pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2022/bsvbilan_viti_als_2022.pdf)] [[html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2022/bsvbilan_viti_als_2022.html)]

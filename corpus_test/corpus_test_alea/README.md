# Corpus de test aléa

Le corpus de test D2KAB étant relativement homogène,
l'extension du processus d'indexation à l'ensemble des BSV collectés
automatiquement nécessite un corpus plus aléatoire.
150 autres BSV ont donc été tirés aléatoirement parmi l'ensemble des BSV
collectés jusqu'à ce jour (novembre 2020).

De par son caractère aléatoire, 8 documents n'ont pas de date précise,
10 ont une zone géographique inter-régionale voire nationale,
et 1 bulletin n'a pas de type de culture mentionné dans son contenu.

**Répartition :**

Année | Nombre de BSV
--- | ---:
2011 | 3
2012 | 2
2013 | 5
2014 | 1
2015 | 13
2016 | 10
2017 | 26
2018 | 25
2019 | 31
2020 | 26
Non datés | 8

*Alea corpus is composed of 150 PHBs randomly selected from the whole corpus.*
*That is to say the publication date may vary from 2009 to 2020.*
*No information are extracted or provided from those PHBs.*


## Les BSV
### ARA

#### Cultures fruitières

- BSV_RA_ARBO_no19du19062018_cle4af8d5-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2018/BSV_RA_ARBO_no19du19062018_cle4af8d5-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2018/BSV_RA_ARBO_no19du19062018_cle4af8d5-1.html)
- BSV_Presentation_du_reseau_2018_cle4a84c9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2018/BSV_Presentation_du_reseau_2018_cle4a84c9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2018/BSV_Presentation_du_reseau_2018_cle4a84c9.html)

#### Cultures fruitières – Fruits à Noyau

- BSV_Bilan_Fruits_a_noyau_2018_cle888e1e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_Bilan_Fruits_a_noyau_2018_cle888e1e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_Bilan_Fruits_a_noyau_2018_cle888e1e.html)

#### Cultures légumières, allium, PdT

- BSV_legumes_allium_pomme_de_terre_AURA_2020-9_cle0d26da [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2020/BSV_legumes_allium_pomme_de_terre_AURA_2020-9_cle0d26da.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2020/BSV_legumes_allium_pomme_de_terre_AURA_2020-9_cle0d26da.html)

#### Petits fruits

- BSV_petits-fruits-2019_N05_cle41f1d8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_petits-fruits-2019_N05_cle41f1d8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_petits-fruits-2019_N05_cle41f1d8.html)

#### Vignes

- BSV-18-17_cle8f1356-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2017/BSV-18-17_cle8f1356-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2017/BSV-18-17_cle8f1356-1.html)

#### ZNA

- BSV_ZNA_2018_N09_cle4f1dfc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2018/BSV_ZNA_2018_N09_cle4f1dfc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2018/BSV_ZNA_2018_N09_cle4f1dfc.html)

### Alsace

#### GC

- 05_GC_Als_cle8c3f61 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/05_GC_Als_cle8c3f61.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/05_GC_Als_cle8c3f61.html)

#### Horticulture

- 13_HORTI_Als_cle02386b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/13_HORTI_Als_cle02386b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/13_HORTI_Als_cle02386b.html)

#### Tabac

- 01_TB_Als_cle8c6253 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/01_TB_Als_cle8c6253.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/01_TB_Als_cle8c6253.html)

### BFC

#### Cassis

- Cassis_n_6_du_16_05_19_cle034491 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/Cassis_n_6_du_16_05_19_cle034491.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/Cassis_n_6_du_16_05_19_cle034491.html)

### Bourgogne

#### Cassis

- BSV_Cassis_n_05_du_10_mai_2012_cle09e263 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2012/BSV_Cassis_n_05_du_10_mai_2012_cle09e263.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2012/BSV_Cassis_n_05_du_10_mai_2012_cle09e263.html)

#### GC

- BSV_GC_n_15_du_10_mars_2015_cle0bc9b5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2015/BSV_GC_n_15_du_10_mars_2015_cle0bc9b5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2015/BSV_GC_n_15_du_10_mars_2015_cle0bc9b5.html)
- BSV_GC_n_22_du_30_avril_2013_cle82bfbf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2013/BSV_GC_n_22_du_30_avril_2013_cle82bfbf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2013/BSV_GC_n_22_du_30_avril_2013_cle82bfbf.html)

#### Productions horticoles

- BSV_Horticulture_n_14_du_21_octobre_2011_cle036c85 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2011/BSV_Horticulture_n_14_du_21_octobre_2011_cle036c85.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2011/BSV_Horticulture_n_14_du_21_octobre_2011_cle036c85.html)

#### ZNA

- BSV_ZNA_n_04_du_29_avril_2011_cle8fd486 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2011/BSV_ZNA_n_04_du_29_avril_2011_cle8fd486.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2011/BSV_ZNA_n_04_du_29_avril_2011_cle8fd486.html)
- BSV_ZNA_n_05_du_31_mai_2013_cle0dcf4b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2013/BSV_ZNA_n_05_du_31_mai_2013_cle0dcf4b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2013/BSV_ZNA_n_05_du_31_mai_2013_cle0dcf4b.html)
- BSV_ZNA_n_13_du_21_septembre_2012_cle82a3a6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2012/BSV_ZNA_n_13_du_21_septembre_2012_cle82a3a6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2012/BSV_ZNA_n_13_du_21_septembre_2012_cle82a3a6.html)

### Bretagne

#### GC

- BSV_bilan_ble_campagne_2014_2015_cle037c1e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2018/BSV_bilan_ble_campagne_2014_2015_cle037c1e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2018/BSV_bilan_ble_campagne_2014_2015_cle037c1e.html)
- BSV_no13_du_15-05-18_cle01162f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2018/BSV_no13_du_15-05-18_cle01162f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2018/BSV_no13_du_15-05-18_cle01162f.html)
- BSV_no19_du_23-06-2020_cle09c329 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2020/BSV_no19_du_23-06-2020_cle09c329.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2020/BSV_no19_du_23-06-2020_cle09c329.html)
- BSV_no7_du_31-03-20_cle835128 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2020/BSV_no7_du_31-03-20_cle835128.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2020/BSV_no7_du_31-03-20_cle835128.html)
- BSV_no31_du_05-11-19_cle05121a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_no31_du_05-11-19_cle05121a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_no31_du_05-11-19_cle05121a.html)

#### PdT

- BSV_no17_pommes_de_terre_cle8966e3-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2018/BSV_no17_pommes_de_terre_cle8966e3-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2018/BSV_no17_pommes_de_terre_cle8966e3-1.html)

### CVL

#### Arboriculture

- BSV_arboriculture_25_du_23_05_19_cle077bda [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_arboriculture_25_du_23_05_19_cle077bda.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_arboriculture_25_du_23_05_19_cle077bda.html)
- BSV_arboriculture_31_du_20_06_19_cle016821 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_arboriculture_31_du_20_06_19_cle016821.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_arboriculture_31_du_20_06_19_cle016821.html)

#### Céréales à paille

- BSV_Cereales_paille_14_cle09e79d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2017/BSV_Cereales_paille_14_cle09e79d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2017/BSV_Cereales_paille_14_cle09e79d.html)

#### Légumes

- BSV_legumes_05_cle827cb7-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2017/BSV_legumes_05_cle827cb7-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2017/BSV_legumes_05_cle827cb7-1.html)
- BSV_legumes_03_du_03-04-19_cle4b43ee [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_legumes_03_du_03-04-19_cle4b43ee.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_legumes_03_du_03-04-19_cle4b43ee.html)
- BSV_legumes_15_cle8b2b14-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2017/BSV_legumes_15_cle8b2b14-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2017/BSV_legumes_15_cle8b2b14-1.html)
- BSV_legumes_12_du_26-06-19__cle047171 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_legumes_12_du_26-06-19__cle047171.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_legumes_12_du_26-06-19__cle047171.html)
- BSV_legumes_12_du_22-07-20__cle0bb819 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2020/BSV_legumes_12_du_22-07-20__cle0bb819.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2020/BSV_legumes_12_du_22-07-20__cle0bb819.html)

#### Oléagineux

- BSV_oleagineux_08_du_23-10-18_cle8b615d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2018/BSV_oleagineux_08_du_23-10-18_cle8b615d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2018/BSV_oleagineux_08_du_23-10-18_cle8b615d.html)
- BSV_oleagineux_02_du_11-09-18_cle8aec1b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2018/BSV_oleagineux_02_du_11-09-18_cle8aec1b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2018/BSV_oleagineux_02_du_11-09-18_cle8aec1b.html)
- BSV_oleagineux_22_du_31-03-20_cle831bc8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2020/BSV_oleagineux_22_du_31-03-20_cle831bc8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2020/BSV_oleagineux_22_du_31-03-20_cle831bc8.html)
- BSV_oleagineux_16_cle0c1a18-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2016/BSV_oleagineux_16_cle0c1a18-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2016/BSV_oleagineux_16_cle0c1a18-1.html)

#### Viticulture

- BSV_viti_10_cle4e6966-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2017/BSV_viti_10_cle4e6966-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2017/BSV_viti_10_cle4e6966-2.html)

### Centre

#### Arboriculture

- BSV_arbo_27_cle4dff21 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2015/BSV_arbo_27_cle4dff21.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2015/BSV_arbo_27_cle4dff21.html)

#### Légumes

- BSV_legumes_03_cle872d86 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2015/BSV_legumes_03_cle872d86.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2015/BSV_legumes_03_cle872d86.html)

### Champagne-Ardenne

#### GC

- 2015-05-28_BSV_Grandes_Cultures_01_cle8ba414 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/2015-05-28_BSV_Grandes_Cultures_01_cle8ba414.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/2015-05-28_BSV_Grandes_Cultures_01_cle8ba414.html)

### Corse

#### Arboriculture

- BSV_Arboriculture_4_14mars2017_cle063599 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2017/BSV_Arboriculture_4_14mars2017_cle063599.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2017/BSV_Arboriculture_4_14mars2017_cle063599.html)
- BSV_Arboriculture_3_24_fevrier_2020-2-2_cle83ae58 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2020/BSV_Arboriculture_3_24_fevrier_2020-2-2_cle83ae58.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2020/BSV_Arboriculture_3_24_fevrier_2020-2-2_cle83ae58.html)

#### JEVI

- BSV_JEVI_4_24_juillet_2019_cle4fa1b8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2019/BSV_JEVI_4_24_juillet_2019_cle4fa1b8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2019/BSV_JEVI_4_24_juillet_2019_cle4fa1b8.html)

### Franche-Comté

#### GC

- BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No20_-_9_JUILLET_2013_cle8e731d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2013/BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No20_-_9_JUILLET_2013_cle8e731d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2013/BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No20_-_9_JUILLET_2013_cle8e731d.html)
- BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No14_-_28_MAI_2013_cle4c4dec [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2013/BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No14_-_28_MAI_2013_cle4c4dec.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2013/BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No14_-_28_MAI_2013_cle4c4dec.html)
- BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No10_-_30_avril_2013_cle858e15 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2013/BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No10_-_30_avril_2013_cle858e15.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2013/BULLETIN_DE_SANTE_DU_VEGETAL_GC_2013-_No10_-_30_avril_2013_cle858e15.html)
- bulletindesanteduvegetalgc2014-n9-15avril2014_cle066b5e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2014/bulletindesanteduvegetalgc2014-n9-15avril2014_cle066b5e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2014/bulletindesanteduvegetalgc2014-n9-15avril2014_cle066b5e.html)

#### Maïs

- BULLETIN_DE_SANTE_DU_VEGETAL_GC_2011-_No38-_6-12-2011_cle8d8ade [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2011/BULLETIN_DE_SANTE_DU_VEGETAL_GC_2011-_No38-_6-12-2011_cle8d8ade.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2011/BULLETIN_DE_SANTE_DU_VEGETAL_GC_2011-_No38-_6-12-2011_cle8d8ade.html)

### Grand-Est

#### Arboriculture

- BSV_5_VF_cle84611d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/BSV_5_VF_cle84611d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/BSV_5_VF_cle84611d.html)
- BSVarbo9_cle8977d6-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/BSVarbo9_cle8977d6-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/BSVarbo9_cle8977d6-1.html)
- lorraine_arboriculture_no11_du_13-05-20_cle8ee895 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/lorraine_arboriculture_no11_du_13-05-20_cle8ee895.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/lorraine_arboriculture_no11_du_13-05-20_cle8ee895.html)
- BSV_17-VF_cle81c713 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/BSV_17-VF_cle81c713.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/BSV_17-VF_cle81c713.html)

#### GC

- BSV_Betteraves_du_29_mai_2019_cle8fbfd9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_Betteraves_du_29_mai_2019_cle8fbfd9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_Betteraves_du_29_mai_2019_cle8fbfd9.html)
- BSV_Betteraves_du_22_juillet_2020_cle8647db [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2020/BSV_Betteraves_du_22_juillet_2020_cle8647db.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2020/BSV_Betteraves_du_22_juillet_2020_cle8647db.html)
- als_Grandes_Cultures_no32_2016-10-11_cle4e331a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_Grandes_Cultures_no32_2016-10-11_cle4e331a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_Grandes_Cultures_no32_2016-10-11_cle4e331a.html)
- char_Grandes_Cultures_no22_2016-07-07_cle02be6e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/char_Grandes_Cultures_no22_2016-07-07_cle02be6e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/char_Grandes_Cultures_no22_2016-07-07_cle02be6e.html)
- BSV05_GC_LOR_S13_2020_cle4419d7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/BSV05_GC_LOR_S13_2020_cle4419d7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/BSV05_GC_LOR_S13_2020_cle4419d7.html)
- alsace_gdes_cultures_no24_du_19-08-20_cle075fe6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_gdes_cultures_no24_du_19-08-20_cle075fe6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_gdes_cultures_no24_du_19-08-20_cle075fe6.html)
- char_Grandes_Cultures_no42_2016-11-24_cle0385bf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/char_Grandes_Cultures_no42_2016-11-24_cle0385bf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/char_Grandes_Cultures_no42_2016-11-24_cle0385bf.html)
- 20190403_LOR_BSV_Grandes_Cultures_cle8ff1e5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190403_LOR_BSV_Grandes_Cultures_cle8ff1e5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190403_LOR_BSV_Grandes_Cultures_cle8ff1e5.html)
- als_Grandes_Cultures_no10_2016-04-26_cle4d3231 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2016/als_Grandes_Cultures_no10_2016-04-26_cle4d3231.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2016/als_Grandes_Cultures_no10_2016-04-26_cle4d3231.html)

#### Légumes

- BSV_ge_legumes_no11_du_01-07-20_cle497eed [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/BSV_ge_legumes_no11_du_01-07-20_cle497eed.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/BSV_ge_legumes_no11_du_01-07-20_cle497eed.html)

#### Maraîchage

- BSVleg_n10_2017_cle03fb85 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/BSVleg_n10_2017_cle03fb85.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/BSVleg_n10_2017_cle03fb85.html)

#### Viticulture

- lorraine_vigne_no14_du_29-07-20_cle4bcf91 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/lorraine_vigne_no14_du_29-07-20_cle4bcf91.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/lorraine_vigne_no14_du_29-07-20_cle4bcf91.html)
- alsace03_BSV_VIGNE_250417_cle0358f2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/alsace03_BSV_VIGNE_250417_cle0358f2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/alsace03_BSV_VIGNE_250417_cle0358f2.html)
- alsace_vigne_no10_du_23-06-20_cle812596 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2020/alsace_vigne_no10_du_23-06-20_cle812596.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2020/alsace_vigne_no10_du_23-06-20_cle812596.html)
- BSV_2019_n14bis_cle0ca71b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/BSV_2019_n14bis_cle0ca71b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/BSV_2019_n14bis_cle0ca71b.html)

#### Culture non définie

- BSVcampagnols10_cle06d1a1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2017/BSVcampagnols10_cle06d1a1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2017/BSVcampagnols10_cle06d1a1.html)

### Guadeloupe

#### Banane, Cane à sucre

- BSV971_BanCas_N04_cle08e5ae [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17012/2020/BSV971_BanCas_N04_cle08e5ae.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17012/2020/BSV971_BanCas_N04_cle08e5ae.html)

#### Cultures vivrières

- BSV971_CulturesVivrieres-Bilan2018_cle81e161 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17012/2020/BSV971_CulturesVivrieres-Bilan2018_cle81e161.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17012/2020/BSV971_CulturesVivrieres-Bilan2018_cle81e161.html)

### HdF

#### Arboriculture fruitière

- BSV_AF_n30_du_25052018_cle0ca846 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2018/BSV_AF_n30_du_25052018_cle0ca846.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2018/BSV_AF_n30_du_25052018_cle0ca846.html)
- BSV_AF_5_15_mars_2019_cle452e6e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_AF_5_15_mars_2019_cle452e6e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_AF_5_15_mars_2019_cle452e6e.html)
- BSV_AF_n19_du_11052017-1_cle84e61b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2017/BSV_AF_n19_du_11052017-1_cle84e61b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2017/BSV_AF_n19_du_11052017-1_cle84e61b.html)
- BSV_AF_n34_du_16072019_cle0a1d81 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_AF_n34_du_16072019_cle0a1d81.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_AF_n34_du_16072019_cle0a1d81.html)

#### Cultures légumières

- BSV_Legumes_322019_2__cle44e13b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_Legumes_322019_2__cle44e13b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_Legumes_322019_2__cle44e13b.html)
- BSV_Legumes_082019_cle841c8f-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_Legumes_082019_cle841c8f-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_Legumes_082019_cle841c8f-1.html)

#### Fruits rouges

- bsv_01_fruits_rouges_25_avril_2018_cle8fdf59 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2018/bsv_01_fruits_rouges_25_avril_2018_cle8fdf59.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2018/bsv_01_fruits_rouges_25_avril_2018_cle8fdf59.html)
- BSV_FR_08_13082019_cle8c5d91 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_FR_08_13082019_cle8c5d91.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_FR_08_13082019_cle8c5d91.html)

#### PdT

- BSV_pomme_de_terre_8_du_10_mai_2016_cle0127ee [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2016/BSV_pomme_de_terre_8_du_10_mai_2016_cle0127ee.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2016/BSV_pomme_de_terre_8_du_10_mai_2016_cle0127ee.html)
- bsv_13_pdt_21_juin_cle8d177f-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2016/bsv_13_pdt_21_juin_cle8d177f-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2016/bsv_13_pdt_21_juin_cle8d177f-1.html)

### IdF

#### Arboriculture

- BSV_Arbo_06_20_cle882ae6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2020/BSV_Arbo_06_20_cle882ae6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2020/BSV_Arbo_06_20_cle882ae6.html)
- BSV_ARBO_2017_35_cle4d79ba [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2017/BSV_ARBO_2017_35_cle4d79ba.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2017/BSV_ARBO_2017_35_cle4d79ba.html)
- BSV_ARBO_2018_11_cle44752f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2018/BSV_ARBO_2018_11_cle44752f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2018/BSV_ARBO_2018_11_cle44752f.html)

#### GC, PdT, Leg.Indus

- BSV_GRANDES_CULTURES_2017_No5_cle8b8eff [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2017/BSV_GRANDES_CULTURES_2017_No5_cle8b8eff.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2017/BSV_GRANDES_CULTURES_2017_No5_cle8b8eff.html)
- BSV_GRANDES_CULTURES_2018_No38_cle0b961f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2018/BSV_GRANDES_CULTURES_2018_No38_cle0b961f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2018/BSV_GRANDES_CULTURES_2018_No38_cle0b961f.html)

#### Maraîchage

- BSV_MARAICHAGE_2019_07_cle0cc493 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_MARAICHAGE_2019_07_cle0cc493.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_MARAICHAGE_2019_07_cle0cc493.html)
- BSV_MARAICHAGE_2018-17_cle04f12d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2018/BSV_MARAICHAGE_2018-17_cle04f12d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2018/BSV_MARAICHAGE_2018-17_cle04f12d.html)

### Lorraine

#### GC, ZNA

- BSVcampagnols3_cle8d3542 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/BSVcampagnols3_cle8d3542.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/BSVcampagnols3_cle8d3542.html)

#### Viticulture

- BSVviti12_cle89fa1e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2015/BSVviti12_cle89fa1e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2015/BSVviti12_cle89fa1e.html)

### Normandie

#### Betterave

- BSV_no4_cle0e4cd5-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2016/BSV_no4_cle0e4cd5-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2016/BSV_no4_cle0e4cd5-1.html)

#### Colza

- BSV_Colza_no25-14nov_cle0f148b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2018/BSV_Colza_no25-14nov_cle0f148b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2018/BSV_Colza_no25-14nov_cle0f148b.html)
- BSV_Colza_no08-05avril18_cle866147 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2018/BSV_Colza_no08-05avril18_cle866147.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2018/BSV_Colza_no08-05avril18_cle866147.html)
- BSV_Colza_no24-20nov2019_cle8157b8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_Colza_no24-20nov2019_cle8157b8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_Colza_no24-20nov2019_cle8157b8.html)

#### Cultures légumières

- 2017_BSV_manche_10_sem21_cle8d2aee [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2017/2017_BSV_manche_10_sem21_cle8d2aee.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2017/2017_BSV_manche_10_sem21_cle8d2aee.html)
- 2017_BSV_manche_33_sem44_cle8f1de5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2017/2017_BSV_manche_33_sem44_cle8f1de5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2017/2017_BSV_manche_33_sem44_cle8f1de5.html)

#### Cultures ornementales

- BSV_Ornement_Normandie_n-09-2017_cle03389c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2017/BSV_Ornement_Normandie_n-09-2017_cle03389c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2017/BSV_Ornement_Normandie_n-09-2017_cle03389c.html)

#### Céréales

- 20160718-BSV_30_20_Cereales_Normandie_cle095921 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2016/20160718-BSV_30_20_Cereales_Normandie_cle095921.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2016/20160718-BSV_30_20_Cereales_Normandie_cle095921.html)
- BSV_01-42_cereales_Normandie_2020_cle83b764 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/BSV_01-42_cereales_Normandie_2020_cle83b764.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/BSV_01-42_cereales_Normandie_2020_cle83b764.html)
- BSV_bilan_cereales_Normandie_R2015_V2_cle0e2acd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2015/BSV_bilan_cereales_Normandie_R2015_V2_cle0e2acd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2015/BSV_bilan_cereales_Normandie_R2015_V2_cle0e2acd.html)

### Nouvelle-Aquitaine

#### Châtaigner

- BSV_NA_CHATAIGNIER_GSO_09_20190919_cle868eba [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_CHATAIGNIER_GSO_09_20190919_cle868eba.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_CHATAIGNIER_GSO_09_20190919_cle868eba.html)

#### Cultures légumières

- BSV_NA_CULTURES__LEGUMIERES-Nord_Nouvelle_Aquitaine_n_28_cle424df5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2017/BSV_NA_CULTURES__LEGUMIERES-Nord_Nouvelle_Aquitaine_n_28_cle424df5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2017/BSV_NA_CULTURES__LEGUMIERES-Nord_Nouvelle_Aquitaine_n_28_cle424df5.html)
- BSV_NA_CULTURES_LEGUMIERES-Nord_NA__20_20170830_cle02661b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2017/BSV_NA_CULTURES_LEGUMIERES-Nord_NA__20_20170830_cle02661b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2017/BSV_NA_CULTURES_LEGUMIERES-Nord_NA__20_20170830_cle02661b.html)

#### GC

- BSV_GC_NA_Limousin_18_20200901_cle088995 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2020/BSV_GC_NA_Limousin_18_20200901_cle088995.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2020/BSV_GC_NA_Limousin_18_20200901_cle088995.html)
- BSV_NA_GC_Aquitaine_N5_20180322_cle4a4445 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2018/BSV_NA_GC_Aquitaine_N5_20180322_cle4a4445.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2018/BSV_NA_GC_Aquitaine_N5_20180322_cle4a4445.html)
- BSV_NA_GC_AQUITAINE_N09_20190404_cle0279a3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_GC_AQUITAINE_N09_20190404_cle0279a3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_GC_AQUITAINE_N09_20190404_cle0279a3.html)

#### Kiwi

- BSV_NA_KIWI_9_20200409_cle0a82f1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2020/BSV_NA_KIWI_9_20200409_cle0a82f1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2020/BSV_NA_KIWI_9_20200409_cle0a82f1.html)
- BSV_NA_KIWI_15_20190613_cle86b7d5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_KIWI_15_20190613_cle86b7d5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_KIWI_15_20190613_cle86b7d5.html)
- BSV_KIWI_NA_N_3_2017-02-23_cle483144 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2017/BSV_KIWI_NA_N_3_2017-02-23_cle483144.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2017/BSV_KIWI_NA_N_3_2017-02-23_cle483144.html)

#### Maraîchage

- BSV_MARAICHAGE_Nord_NA_24_20201021_cle8b8312 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2020/BSV_MARAICHAGE_Nord_NA_24_20201021_cle8b8312.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2020/BSV_MARAICHAGE_Nord_NA_24_20201021_cle8b8312.html)

#### Petits fruits

- BSV_NA_PETITS_FRUITS_05_20190313-1_cle8c1a16 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_PETITS_FRUITS_05_20190313-1_cle8c1a16.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_PETITS_FRUITS_05_20190313-1_cle8c1a16.html)
- BSV_PETITS_FRUITS_NA_01_20200114_cle04ef97 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2020/BSV_PETITS_FRUITS_NA_01_20200114_cle04ef97.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2020/BSV_PETITS_FRUITS_NA_01_20200114_cle04ef97.html)

#### Pommier / Poirier

- BSV_NA_POMMIER_NORD_18_20170719_cle4a8443 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2017/BSV_NA_POMMIER_NORD_18_20170719_cle4a8443.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2017/BSV_NA_POMMIER_NORD_18_20170719_cle4a8443.html)
- BSV_NA_POMMIER_POIRIER_Sud_18_20180726_cle8d7184 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2018/BSV_NA_POMMIER_POIRIER_Sud_18_20180726_cle8d7184.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2018/BSV_NA_POMMIER_POIRIER_Sud_18_20180726_cle8d7184.html)
- BSV_NA_POMMIER_POIRIER_Sud_8_20190404_cle0177e8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_POMMIER_POIRIER_Sud_8_20190404_cle0177e8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_POMMIER_POIRIER_Sud_8_20190404_cle0177e8.html)
- BSV_NA_POMMIER_POIRIER_LIMOUSIN_01_20200124_cle837b7a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2020/BSV_NA_POMMIER_POIRIER_LIMOUSIN_01_20200124_cle837b7a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2020/BSV_NA_POMMIER_POIRIER_LIMOUSIN_01_20200124_cle837b7a.html)

#### Vigne

- BSV_NA_VIGNE_Limousin_06_20180515_cle841333 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2018/BSV_NA_VIGNE_Limousin_06_20180515_cle841333.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2018/BSV_NA_VIGNE_Limousin_06_20180515_cle841333.html)
- BSV_NA_VIGNE_Sud_Aquitaine_17_20190716_cle841a78 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_17_20190716_cle841a78.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_VIGNE_Sud_Aquitaine_17_20190716_cle841a78.html)
- BSV_NA_VIGNE_Limousin_18_20180807_cle8eb5f7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2018/BSV_NA_VIGNE_Limousin_18_20180807_cle8eb5f7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2018/BSV_NA_VIGNE_Limousin_18_20180807_cle8eb5f7.html)
- BSV_NA_VIGNE_CHARENTE_03_20170419_cle86a6e5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2017/BSV_NA_VIGNE_CHARENTE_03_20170419_cle86a6e5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2017/BSV_NA_VIGNE_CHARENTE_03_20170419_cle86a6e5.html)

### Occitanie

#### Ail

- bsv_ail_mp_n11_06062019_cle816b36 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_ail_mp_n11_06062019_cle816b36.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_ail_mp_n11_06062019_cle816b36.html)

#### Arboriculture

- bsv_arbo_n25_19092017_cle4b1c13 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2017/bsv_arbo_n25_19092017_cle4b1c13.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2017/bsv_arbo_n25_19092017_cle4b1c13.html)

#### GC

- bsv_grandes_cultures_n27_18052017_cle88cbb1-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2017/bsv_grandes_cultures_n27_18052017_cle88cbb1-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2017/bsv_grandes_cultures_n27_18052017_cle88cbb1-1.html)

#### Vigne

- bsv_viti_mp_hautegaronnetarnetgaronne_n07_12052020_cle0cfad7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2020/bsv_viti_mp_hautegaronnetarnetgaronne_n07_12052020_cle0cfad7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2020/bsv_viti_mp_hautegaronnetarnetgaronne_n07_12052020_cle0cfad7.html)
- bsv_vigne_n10_fronton_07062017-3_cle0a711e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2017/bsv_vigne_n10_fronton_07062017-3_cle0a711e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2017/bsv_vigne_n10_fronton_07062017-3_cle0a711e.html)
- bsv_viti_mp_gaillac_n07_12052020_cle0ee886 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2020/bsv_viti_mp_gaillac_n07_12052020_cle0ee886.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2020/bsv_viti_mp_gaillac_n07_12052020_cle0ee886.html)
- bsv_viticulture_no10_du_23_mai_2018_cle0c892f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_viticulture_no10_du_23_mai_2018_cle0c892f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_viticulture_no10_du_23_mai_2018_cle0c892f.html)
- bsv_vigne_n1_fronton_23032017_cle825f73 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2017/bsv_vigne_n1_fronton_23032017_cle825f73.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2017/bsv_vigne_n1_fronton_23032017_cle825f73.html)

### PACA

#### JEVI

- BSV_ZNA-PO_20190527_N2_cle048919 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/BSV_ZNA-PO_20190527_N2_cle048919.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/BSV_ZNA-PO_20190527_N2_cle048919.html)

#### Maraîchage

- bsv_maraichage_no11_du_05_Juin_2020_cle012212 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2020/bsv_maraichage_no11_du_05_Juin_2020_cle012212.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2020/bsv_maraichage_no11_du_05_Juin_2020_cle012212.html)
- BSV_maraichage_no20_du_23_Octobre_2020_cle81e1d1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2020/BSV_maraichage_no20_du_23_Octobre_2020_cle81e1d1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2020/BSV_maraichage_no20_du_23_Octobre_2020_cle81e1d1.html)

#### Oléiculture

- BSV_Olivier_20200902_N13_cle8395b6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2020/BSV_Olivier_20200902_N13_cle8395b6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2020/BSV_Olivier_20200902_N13_cle8395b6.html)

#### Tomate d’industrie

- BSV_Tomate_d_industrie_no3_cle4bdce9-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2018/BSV_Tomate_d_industrie_no3_cle4bdce9-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2018/BSV_Tomate_d_industrie_no3_cle4bdce9-2.html)

### Pays de la Loire

#### JEVI

- 19bsv_jevi_20181108_cle871314 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2018/19bsv_jevi_20181108_cle871314.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2018/19bsv_jevi_20181108_cle871314.html)
- 05bsv_jevi_20190502_cle8f22b6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2019/05bsv_jevi_20190502_cle8f22b6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2019/05bsv_jevi_20190502_cle8f22b6.html)

### Poitou-Charentes

#### Arboriculture – pommier

- bsv_arbo_66_2015_cle479316 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2015/bsv_arbo_66_2015_cle479316.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2015/bsv_arbo_66_2015_cle479316.html)
- bsv_arbo_75_2015_cle429b97 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2015/bsv_arbo_75_2015_cle429b97.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2015/bsv_arbo_75_2015_cle429b97.html)

#### Vigne

- 119_BSV_Charentes_2015-07-28_cle88a1b6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2015/119_BSV_Charentes_2015-07-28_cle88a1b6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2015/119_BSV_Charentes_2015-07-28_cle88a1b6.html)

### Localisation non définie

#### Arboriculture – fruits transformés

- BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no_1_du_13-03-2018_cle0bf736 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2018/BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no_1_du_13-03-2018_cle0bf736.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2018/BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no_1_du_13-03-2018_cle0bf736.html)
- BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no_17_du_26-06-2018_cle861251 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2018/BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no_17_du_26-06-2018_cle861251.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2018/BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no_17_du_26-06-2018_cle861251.html)
- BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no15_du_17-06-2020_cle0d4baf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2020/BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no15_du_17-06-2020_cle0d4baf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2020/BSV_Arboriculture-Fruits_transformes__Bretagne-Normandie-Pays_de_la_Loire_no15_du_17-06-2020_cle0d4baf.html)

#### Céréales à paille

- 55_02_note_commune_resistance_fongicides_maladies_cereales_2006_cle86a116 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2018/55_02_note_commune_resistance_fongicides_maladies_cereales_2006_cle86a116.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2018/55_02_note_commune_resistance_fongicides_maladies_cereales_2006_cle86a116.html)

#### GC

- Note_nationale_meth_alternative_adventices_GC_validee-2_cle06dc5b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2018/Note_nationale_meth_alternative_adventices_GC_validee-2_cle06dc5b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2018/Note_nationale_meth_alternative_adventices_GC_validee-2_cle06dc5b.html)

#### Lin fibre

- BSV_LIN_FIBRE_2020_No6_cle0365b1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2020/BSV_LIN_FIBRE_2020_No6_cle0365b1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2020/BSV_LIN_FIBRE_2020_No6_cle0365b1.html)

#### Lin oléagineux

- BSV_Grandes_Cultures_Lin__Oleagineux_n2_NR_17102017_cle4cd66c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2017/BSV_Grandes_Cultures_Lin__Oleagineux_n2_NR_17102017_cle4cd66c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2017/BSV_Grandes_Cultures_Lin__Oleagineux_n2_NR_17102017_cle4cd66c.html)
- BSV_Grandes_Cultures_Lin__Oleagineux_n19_NR_02052018_cle06c988 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2018/BSV_Grandes_Cultures_Lin__Oleagineux_n19_NR_02052018_cle06c988.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2018/BSV_Grandes_Cultures_Lin__Oleagineux_n19_NR_02052018_cle06c988.html)
- BSV_Lin_bilan_cle8a2881 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2017/BSV_Lin_bilan_cle8a2881.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2017/BSV_Lin_bilan_cle8a2881.html)
- BSV_Lin_09_cle07fbd6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2018/BSV_Lin_09_cle07fbd6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2018/BSV_Lin_09_cle07fbd6.html)

#### Maïs

- annexe_au_BSV_GC_No16_desherbage_alternatif_mais_cle81f45a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/annexe_au_BSV_GC_No16_desherbage_alternatif_mais_cle81f45a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/annexe_au_BSV_GC_No16_desherbage_alternatif_mais_cle81f45a.html)

#### Tabac

- bsv_na_tabac_03_20180523_cle8e53e1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_na_tabac_03_20180523_cle8e53e1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_na_tabac_03_20180523_cle8e53e1.html)

#### Culture non définie

- Note_nat-_BSV_Aromia_bungii_juin_2018_v2_cle09d4e1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2018/Note_nat-_BSV_Aromia_bungii_juin_2018_v2_cle09d4e1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2018/Note_nat-_BSV_Aromia_bungii_juin_2018_v2_cle09d4e1.html)

# Corpus de test VESPA

Le développement de l'indexation dans le cadre du projet Vespa
a donné lieu à une extraction manuelle de date pour 500 bulletins
de santé du végétal, parmi les 19448 BSV collectés dans le cadre de ce projet.

Les régions et types de cultures ont été manuellement extraits
au moment des collectes de ces BSV.

On dispose donc d'un corpus permettant de tester l'extraction
automatique de dates. Ces bulletins ayant été collectés de 2009 à 2015,
les zones géographiques correspondent au découpage régional
avant la réforme du 1er janvier 2016.

Année | Nombre de BSV
--- | ---:
2009 | 23
2010 | 59
2011 | 88
2012 | 98
2013 | 111
2014 | 93
Non datés | 28

En ce qui concerne les types de culture, il s'agit de rubriques
indiquées dans les pages web de téléchargement (Grandes cultures,
Viticulture, Jardins, ...).
Chaque rubrique a été associée manuellement à un ensemble de concepts
issus du thésaurus french crop usage. À noter que les rubriques
varient d'une région à l'autre et qu'il n'y a pas de liste harmonisée
des types de cultures.
C'est pour cette raison que nous avons créé le thésaurus
[french crop usage](https://agroportal.lirmm.fr/ontologies/CROPUSAGE).

Trois de ces bulletins ne contiennent pas de texte (c'est à dire que le
contenu du BSV est sous forme d'image - éventuellement scannée).
Ces trois bulletins ne sont donc pas disponibles au format html.

*Vespa corpus gathers 500 PHBs collected in the whole French territory*
*between 2009 and 2015 and manually indexed.*
*A PHB is indexed by its crop categories and its location,*
*that is to say, its French administrative unit.*
*The publication date were manually extracted.*


## Les BSV
### Alsace

#### Arboriculture

- 15_BSV_ARBO_130612_cle82d761 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2012/15_BSV_ARBO_130612_cle82d761.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2012/15_BSV_ARBO_130612_cle82d761.html)

#### Grandes cultures

- 27_BSV_GC_260814_cle4c8181 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/27_BSV_GC_260814_cle4c8181.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/27_BSV_GC_260814_cle4c8181.html)
- 06_BSV_GC_05042011_cle8889ba [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/06_BSV_GC_05042011_cle8889ba.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/06_BSV_GC_05042011_cle8889ba.html)
- 21_BSV_GC_20072011_cle835b6b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/21_BSV_GC_20072011_cle835b6b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/21_BSV_GC_20072011_cle835b6b.html)
- 18_BSV_GC_02072013_cle8e434e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/18_BSV_GC_02072013_cle8e434e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/18_BSV_GC_02072013_cle8e434e.html)
- 11_BSV_GC_17052010_cle85a2fb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2010/11_BSV_GC_17052010_cle85a2fb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2010/11_BSV_GC_17052010_cle85a2fb.html)
- 07_BSV_GC_16042013_cle83b3df [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/07_BSV_GC_16042013_cle83b3df.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/07_BSV_GC_16042013_cle83b3df.html)
- 11_BSV_GC_19052009_cle85f46f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2009/11_BSV_GC_19052009_cle85f46f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2009/11_BSV_GC_19052009_cle85f46f.html)
- 26_BSV_GC_17092013_cle847136 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2013/26_BSV_GC_17092013_cle847136.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2013/26_BSV_GC_17092013_cle847136.html)

#### Légumes

- 21_BSV_LEG_0809011_cle81ee2d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2011/21_BSV_LEG_0809011_cle81ee2d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2011/21_BSV_LEG_0809011_cle81ee2d.html)

#### Tabac

- 09_BSV_TB_260914_cle448c88 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/09_BSV_TB_260914_cle448c88.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/09_BSV_TB_260914_cle448c88.html)

#### Zones non-Agricoles

- 08_BSV_ZNA_080714_cle0bae61 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1142/2014/08_BSV_ZNA_080714_cle0bae61.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1142/2014/08_BSV_ZNA_080714_cle0bae61.html)

### Aquitaine

#### Arboriculture

- BSV_N_16-ARBO-2013.07.04 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_16-ARBO-2013.07.04.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_16-ARBO-2013.07.04.html)
- BSV_N_12-ARBO-15.05.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_12-ARBO-15.05.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_12-ARBO-15.05.2012.html)

#### Bulletins spéciaux

- Note_trichogramme_2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/Note_trichogramme_2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/Note_trichogramme_2014.html)

#### Châtaigne

- BSV_5_chataigne_28aout2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_5_chataigne_28aout2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_5_chataigne_28aout2013.html)

#### Fraise-Framboise

- BSV_N_18-Fraise_Framboise-2014.10.02 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_18-Fraise_Framboise-2014.10.02.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_18-Fraise_Framboise-2014.10.02.html)
- BSV_N_10-Fraise_Framboise-2013.07.03 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_10-Fraise_Framboise-2013.07.03.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_10-Fraise_Framboise-2013.07.03.html)
- BSV_N_06-Fraise-Framboise-19.04.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_06-Fraise-Framboise-19.04.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_06-Fraise-Framboise-19.04.2012.html)
- BSV_No1-MaraichageFraise-Framboise_cle8d6cc7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2011/BSV_No1-MaraichageFraise-Framboise_cle8d6cc7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2011/BSV_No1-MaraichageFraise-Framboise_cle8d6cc7.html)

#### Grandes cultures

- BSV_N_44-Grandes_Cultures-2014.11.27 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_44-Grandes_Cultures-2014.11.27.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_44-Grandes_Cultures-2014.11.27.html)
- BSV_N_22-Grandes_Cultures-25.07.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_22-Grandes_Cultures-25.07.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_22-Grandes_Cultures-25.07.2012.html)
- BSV_N_41-Grandes_Cultures-2013.11.07 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_41-Grandes_Cultures-2013.11.07.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_41-Grandes_Cultures-2013.11.07.html)
- BSV_N_5-Grandes_Cultures-2013.03.14 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_5-Grandes_Cultures-2013.03.14.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_5-Grandes_Cultures-2013.03.14.html)
- BSV_No28-Grandes_cultures_cle08bd81 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2011/BSV_No28-Grandes_cultures_cle08bd81.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2011/BSV_No28-Grandes_cultures_cle08bd81.html)
- BSV_N_19-Grandes_Cultures-2014.06.19 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_19-Grandes_Cultures-2014.06.19.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_19-Grandes_Cultures-2014.06.19.html)
- BSV_No2-Grandes_Cultures_cle8a9796-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2011/BSV_No2-Grandes_Cultures_cle8a9796-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2011/BSV_No2-Grandes_Cultures_cle8a9796-1.html)
- BSV_N_9-Grandes_Cultures-2013.04.11 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_9-Grandes_Cultures-2013.04.11.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_9-Grandes_Cultures-2013.04.11.html)
- BSV_N_23-Grandes_Cultures-2014.07.18 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_23-Grandes_Cultures-2014.07.18.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_23-Grandes_Cultures-2014.07.18.html)

#### Légumes plein champ

- BSV_N_4-Legumes_de_plein_champ-2014.04.30 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_4-Legumes_de_plein_champ-2014.04.30.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_4-Legumes_de_plein_champ-2014.04.30.html)
- BSV_N_17-Legumes_de_plein_champ-2013.08.22 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_17-Legumes_de_plein_champ-2013.08.22.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_17-Legumes_de_plein_champ-2013.08.22.html)

#### Maraîchage - Pomme de Terre

- BSV_N_10-Maraichage_PDT-2013.05.13 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_10-Maraichage_PDT-2013.05.13.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_10-Maraichage_PDT-2013.05.13.html)
- BSV_N_8-Maraichage_PDT-2014.05.07 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_12-Maraichage_PDT-2014.05.07.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_12-Maraichage_PDT-2014.05.07.html)
- BSV_N_12-Maraichage_PDT-2014.05.28 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_12-Maraichage_PDT-2014.05.28.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_12-Maraichage_PDT-2014.05.28.html)
- BSV_N_34_-_Maraichage-BILAN-2eme_partie_-_30.11.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_34_-_Maraichage-BILAN-2eme_partie_-_30.11.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_34_-_Maraichage-BILAN-2eme_partie_-_30.11.2012.html)

#### Melon

- BSV_N_26-Melon_Bilan-2013.11.08 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_26-Melon_Bilan-2013.11.08.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_26-Melon_Bilan-2013.11.08.html)
- BSV_N_15-Melon-2013.07.04 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_15-Melon-2013.07.04.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_15-Melon-2013.07.04.html)

#### Noisettes

- BSV_N_08-Noisette-27.04.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_08-Noisette-27.04.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_08-Noisette-27.04.2012.html)
- BSV_N_06-NOISETTES-2014.04.25 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_06-NOISETTES-2014.04.25.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_06-NOISETTES-2014.04.25.html)

#### Pépinières-Horticulture

- BSV_N_1-Pepi-Bilan_2012-22.01.2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_1-Pepi-Bilan_2012-22.01.2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_1-Pepi-Bilan_2012-22.01.2013.html)
- BSV_N_2-Horticulture-Pepinieres-21.03.2011 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2011/BSV_N_2-Horticulture-Pepinieres-21.03.2011.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2011/BSV_N_2-Horticulture-Pepinieres-21.03.2011.html)

#### Tabac

- BSV_N_10_Tabac-29.08.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_10_Tabac-29.08.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_10_Tabac-29.08.2012.html)
- BSV_N_12-TABAC-Bilan-Aquitaine-Limousin-Auvergne-2013.11.28 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_12-TABAC-Bilan-Aquitaine-Limousin-Auvergne-2013.11.28.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_12-TABAC-Bilan-Aquitaine-Limousin-Auvergne-2013.11.28.html)
- BSV_N_9-TABAC-Aquitaine-Limousin-Auvergne-2013.08.08 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_9-TABAC-Aquitaine-Limousin-Auvergne-2013.08.08.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_9-TABAC-Aquitaine-Limousin-Auvergne-2013.08.08.html)
- BSV_N_06-Tabac-04.07.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_06-Tabac-04.07.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_06-Tabac-04.07.2012.html)

#### Viticulture

- BSV_N_13-Viticulture-03.07.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_13-Viticulture-03.07.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_13-Viticulture-03.07.2012.html)
- BSV_N_18-Viticulture-07.08.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_18-Viticulture-07.08.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_18-Viticulture-07.08.2012.html)
- BSV_N_02-viticulture-17.04.2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2012/BSV_N_02-viticulture-17.04.2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2012/BSV_N_02-viticulture-17.04.2012.html)
- BSV_No20-Viticulture_cle0b8b97-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2011/BSV_No20-Viticulture_cle0b8b97-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2011/BSV_No20-Viticulture_cle0b8b97-1.html)

#### Zones non-Agricoles

- BSV_N_2_-_ZNA_-_2013.04.25 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_2_-_ZNA_-_2013.04.25.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_2_-_ZNA_-_2013.04.25.html)
- BSV_N_10_-_ZNA_-_Bilan-2014.12.11 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_10_-_ZNA_-_Bilan-2014.12.11.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_10_-_ZNA_-_Bilan-2014.12.11.html)
- BSV_N_6_-_ZNA_-_2013.09.17 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_N_6_-_ZNA_-_2013.09.17.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_N_6_-_ZNA_-_2013.09.17.html)

### Auvergne

#### Ail

- BSV_Ail_No8_2014_cle4e683d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2014/BSV_Ail_No8_2014_cle4e683d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2014/BSV_Ail_No8_2014_cle4e683d.html)

#### Grandes cultures

- BSV_AUVERGNE_N_27_DU_21_08_12_cle8bcdf5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2012/BSV_AUVERGNE_N_27_DU_21_08_12_cle8bcdf5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2012/BSV_AUVERGNE_N_27_DU_21_08_12_cle8bcdf5.html)
- BSV_AUVERGNE_N_10_DU_2_04_13_cle81ad49 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2013/BSV_AUVERGNE_N_10_DU_2_04_13_cle81ad49.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2013/BSV_AUVERGNE_N_10_DU_2_04_13_cle81ad49.html)
- BSV_AUVERGNE_N_11_DU_9_04_13_cle857112 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2013/BSV_AUVERGNE_N_11_DU_9_04_13_cle857112.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2013/BSV_AUVERGNE_N_11_DU_9_04_13_cle857112.html)
- BSV_AUVERGNE_N_02_DU_05_02_13-2_cle439548 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2013/BSV_AUVERGNE_N_02_DU_05_02_13-2_cle439548.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2013/BSV_AUVERGNE_N_02_DU_05_02_13-2_cle439548.html)
- BSV_AUVERGNE_N_7_DU_23_03_10_cle8cb95a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2010/BSV_AUVERGNE_N_7_DU_23_03_10_cle8cb95a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2010/BSV_AUVERGNE_N_7_DU_23_03_10_cle8cb95a.html)
- BSV_GC1_2010_02_09_cle851931 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2010/BSV_GC1_2010_02_09_cle851931.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2010/BSV_GC1_2010_02_09_cle851931.html)
- BSV_AUVERGNE_N_6_DU_16_03_10_cle867d57 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2010/BSV_AUVERGNE_N_6_DU_16_03_10_cle867d57.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2010/BSV_AUVERGNE_N_6_DU_16_03_10_cle867d57.html)
- BSV_AUVERGNE_N_5_du_11_03_14_cle8461ba [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2014/BSV_AUVERGNE_N_5_du_11_03_14_cle8461ba.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2014/BSV_AUVERGNE_N_5_du_11_03_14_cle8461ba.html)
- BSV_AUVERGNE_N_14_DU_30_04_13_cle897e81 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2013/BSV_AUVERGNE_N_14_DU_30_04_13_cle897e81.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2013/BSV_AUVERGNE_N_14_DU_30_04_13_cle897e81.html)
- BSV_GC12_2009_05_12_cle89f412-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2009/BSV_GC12_2009_05_12_cle89f412-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2009/BSV_GC12_2009_05_12_cle89f412-1.html)
- BSV_AUVERGNE_N_27_DU_10_08_10_cle8d9eb8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2010/BSV_AUVERGNE_N_27_DU_10_08_10_cle8d9eb8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2010/BSV_AUVERGNE_N_27_DU_10_08_10_cle8d9eb8.html)
- BSV_AUVERGNE_N_6_du_18_03_14_cle893c87 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2014/BSV_AUVERGNE_N_6_du_18_03_14_cle893c87.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2014/BSV_AUVERGNE_N_6_du_18_03_14_cle893c87.html)

#### Prairies

- BSVCT8_nov_2010_cle09c9a2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2010/BSVCT8_nov_2010_cle09c9a2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2010/BSVCT8_nov_2010_cle09c9a2.html)
- BSVCTavril2011_cle84c138 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2011/BSVCTavril2011_cle84c138.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2011/BSVCTavril2011_cle84c138.html)

#### Viticulture

- BSV_no10_cle86dd7f-4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2013/BSV_no10_cle86dd7f-4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2013/BSV_no10_cle86dd7f-4.html)

#### Zones non-Agricoles

- BSV_ZNA_N06_05_07_2012_cle034a47 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2012/BSV_ZNA_N06_05_07_2012_cle034a47.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2012/BSV_ZNA_N06_05_07_2012_cle034a47.html)
- BSV_ZNA_N3_06_08_2010_cle4c36c7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2010/BSV_ZNA_N3_06_08_2010_cle4c36c7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2010/BSV_ZNA_N3_06_08_2010_cle4c36c7.html)
- BSV_ZNA_N7_03_08_2011_cle431c59 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2011/BSV_ZNA_N7_03_08_2011_cle431c59.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2011/BSV_ZNA_N7_03_08_2011_cle431c59.html)

### Basse-Normandie

#### Arboriculture

- BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no10_du_21-05-2013_cle43573f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no10_du_21-05-2013_cle43573f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no10_du_21-05-2013_cle43573f.html)
- BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_06-03-2012_cle0ca15b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_06-03-2012_cle0ca15b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_06-03-2012_cle0ca15b.html)

#### Betterave

- BSV_no2_cle086bf7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2014/BSV_no2_cle086bf7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2014/BSV_no2_cle086bf7.html)

#### Bulletins spéciaux

- Note_nationale_abeilles_et_pollinisateurs_cle4f1286 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2009/Note_nationale_abeilles_et_pollinisateurs_cle4f1286.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2009/Note_nationale_abeilles_et_pollinisateurs_cle4f1286.html)

#### Céréales

- BSV_13_cereales_Normandie_R2012-2_cle88c41b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_13_cereales_Normandie_R2012-2_cle88c41b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_13_cereales_Normandie_R2012-2_cle88c41b.html)
- BSV_8-25_cereales_Normandie_R2014_cle81391a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_8-25_cereales_Normandie_R2014_cle81391a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_8-25_cereales_Normandie_R2014_cle81391a.html)

#### Horticulture

- BSV_Ornement_Normandie_n-15-2012_cle04386f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_Ornement_Normandie_n-15-2012_cle04386f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_Ornement_Normandie_n-15-2012_cle04386f.html)
- BSV_Ornement_Normandie_n-25-2014_cle08f311 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV_Ornement_Normandie_n-25-2014_cle08f311.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV_Ornement_Normandie_n-25-2014_cle08f311.html)
- BSV_Ornement_Normandie_n-20-2014_cle0bad54 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV_Ornement_Normandie_n-20-2014_cle0bad54.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV_Ornement_Normandie_n-20-2014_cle0bad54.html)
- BSV_Ornement_Normandie_n-08-2012_cle0693f1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_Ornement_Normandie_n-08-2012_cle0693f1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_Ornement_Normandie_n-08-2012_cle0693f1.html)

#### Légumes - Calvados,Haute-Normandie

- 2013_BSV_calvados_htenormandie_06_noteAbeille_cle0c95a3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/2013_BSV_calvados_htenormandie_06_noteAbeille_cle0c95a3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/2013_BSV_calvados_htenormandie_06_noteAbeille_cle0c95a3.html)

#### Légumes - Manche

- 2013_BSV_manche_20_cle86aa6e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/2013_BSV_manche_20_cle86aa6e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/2013_BSV_manche_20_cle86aa6e.html)
- 2013_BSV_manche_33_cle8d1fbe [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/2013_BSV_manche_33_cle8d1fbe.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/2013_BSV_manche_33_cle8d1fbe.html)

#### Oléagineux

- BSV_colza_Normandie_V1_N_o13_SEMAINE_8_du_15_AU_21-2-12-1_cle0d6d77 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_colza_Normandie_V1_N_o13_SEMAINE_8_du_15_AU_21-2-12-1_cle0d6d77.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_colza_Normandie_V1_N_o13_SEMAINE_8_du_15_AU_21-2-12-1_cle0d6d77.html)
- BSV_No11_SEMAINE_45_du_30-10_au_5-11-2013_V1-1_cle47fa8d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_No11_SEMAINE_45_du_30-10_au_5-11-2013_V1-1_cle47fa8d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_No11_SEMAINE_45_du_30-10_au_5-11-2013_V1-1_cle47fa8d.html)

#### Pomme de Terre

- BSV_pdt_14_12_cle8b18c9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV_pdt_14_12_cle8b18c9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV_pdt_14_12_cle8b18c9.html)

#### Protéagineux

- BSV_2_proteagineux_Normandie_2012_cle8c9949 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_2_proteagineux_Normandie_2012_cle8c9949.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_2_proteagineux_Normandie_2012_cle8c9949.html)

### Bourgogne

#### Grandes cultures

- BSV_GC_n_13_du_26_fevrier_2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2013/BSV_GC_n_13_du_26_fevrier_2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2013/BSV_GC_n_13_du_26_fevrier_2013.html)
- BSV_26BOU_GC_2014_03_04_16 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2014/BSV_26BOU_GC_2014_03_04_16.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2014/BSV_26BOU_GC_2014_03_04_16.html)
- BSV_26BOU_GC_2010_02_23_09 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2010/BSV_26BOU_GC_2010_02_23_09.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2010/BSV_26BOU_GC_2010_02_23_09.html)
- BSV_26BOU_GC_2010_03_16_12 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2010/BSV_26BOU_GC_2010_03_16_12.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2010/BSV_26BOU_GC_2010_03_16_12.html)
- BSV_GC_n_07_du_15_octobre_2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2013/BSV_GC_n_07_du_15_octobre_2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2013/BSV_GC_n_07_du_15_octobre_2013.html)
- BSV_GC_n_21_du_12_avril_2011 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2011/BSV_GC_n_21_du_12_avril_2011.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2011/BSV_GC_n_21_du_12_avril_2011.html)
- BSV_26BOU_GC_2014_05_20_27 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2014/BSV_26BOU_GC_2014_05_20_27.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2014/BSV_26BOU_GC_2014_05_20_27.html)

#### Horticulture

- BSV_Horti_n_06_du_11_mai_2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2012/BSV_Horti_n_06_du_11_mai_2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2012/BSV_Horti_n_06_du_11_mai_2012.html)
- BSV_Horticulture_n_01_du_14_mars_2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2014/BSV_Horticulture_n_01_du_14_mars_2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2014/BSV_Horticulture_n_01_du_14_mars_2014.html)
- BSV_Horti_n_01_du_02_mars_2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2012/BSV_Horti_n_01_du_02_mars_2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2012/BSV_Horti_n_01_du_02_mars_2012.html)
- BSV_Horticulture_n_03_du_11_avril_2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2014/BSV_Horticulture_n_03_du_11_avril_2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2014/BSV_Horticulture_n_03_du_11_avril_2014.html)
- BSV_Horticulture_n_14_du_10_octobre_2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2014/BSV_Horticulture_n_14_du_10_octobre_2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2014/BSV_Horticulture_n_14_du_10_octobre_2014.html)

#### Légumes

- BSV_Legumes_n_09_du_20_decembre_2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2012/BSV_Legumes_n_09_du_20_decembre_2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2012/BSV_Legumes_n_09_du_20_decembre_2012.html)

#### Viticulture

- BSV_Vigne_n_05_du_29_avril_2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2014/BSV_Vigne_n_05_du_29_avril_2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2014/BSV_Vigne_n_05_du_29_avril_2014.html)
- BSV_Vigne_7_-_17_mai_11_cle82a642 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2011/BSV_Vigne_7_-_17_mai_11_cle82a642.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2011/BSV_Vigne_7_-_17_mai_11_cle82a642.html)

#### Zones non-Agricoles

- BSV_ZNA_n_12_du_18_octobre_2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2013/BSV_ZNA_n_12_du_18_octobre_2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2013/BSV_ZNA_n_12_du_18_octobre_2013.html)
- BSV_ZNA_n_10_du_13_septembre_2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2013/BSV_ZNA_n_10_du_13_septembre_2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2013/BSV_ZNA_n_10_du_13_septembre_2013.html)
- BSV_ZNA_n_07_du_28_juin_2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1173/2013/BSV_ZNA_n_07_du_28_juin_2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1173/2013/BSV_ZNA_n_07_du_28_juin_2013.html)

### Bretagne

#### Arboriculture

- 130709_bu-arbo17_cle4212ae [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2013/130709_bu-arbo17_cle4212ae.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2013/130709_bu-arbo17_cle4212ae.html)
- BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_11-03-2014_cle0ad871 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_11-03-2014_cle0ad871.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_11-03-2014_cle0ad871.html)
- BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no20_du_22-07-2014_cle491adf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no20_du_22-07-2014_cle491adf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no20_du_22-07-2014_cle491adf.html)
- 110426_bu-arbo08_cle4d146c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2011/110426_bu-arbo08_cle4d146c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2011/110426_bu-arbo08_cle4d146c.html)
- BSV_Arboricultuure-Fruits_Transformes__Bretagne-Normandie-Pays_de_la_Loire_no19_du_27-07-2010_bis_cle0dce1f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2010/BSV_Arboricultuure-Fruits_Transformes__Bretagne-Normandie-Pays_de_la_Loire_no19_du_27-07-2010_bis_cle0dce1f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2010/BSV_Arboricultuure-Fruits_Transformes__Bretagne-Normandie-Pays_de_la_Loire_no19_du_27-07-2010_bis_cle0dce1f.html)

#### Grandes cultures

- 130319_BSV_Lin__Oleagineux_n2_F_cle4b3994 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2013/130319_BSV_Lin__Oleagineux_n2_F_cle4b3994.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2013/130319_BSV_Lin__Oleagineux_n2_F_cle4b3994.html)
- 131009_BSV_Lin__Oleagineux_1_cle8152f3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2013/131009_BSV_Lin__Oleagineux_1_cle8152f3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2013/131009_BSV_Lin__Oleagineux_1_cle8152f3.html)
- BSV_no9_du_15-04-14_cle81cb1c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2014/BSV_no9_du_15-04-14_cle81cb1c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2014/BSV_no9_du_15-04-14_cle81cb1c.html)
- 130507_BSV_Lin__Oleagineux_n9_F_cle4bfe17 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2013/130507_BSV_Lin__Oleagineux_n9_F_cle4bfe17.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2013/130507_BSV_Lin__Oleagineux_n9_F_cle4bfe17.html)
- BSV_no12_du_06-05-14_cle0cdc21 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2014/BSV_no12_du_06-05-14_cle0cdc21.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2014/BSV_no12_du_06-05-14_cle0cdc21.html)
- 1300514_bu_gc12_cle02f1cf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2013/1300514_bu_gc12_cle02f1cf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2013/1300514_bu_gc12_cle02f1cf.html)
- BSV_no15_du_27-05-14_cle0f3e54 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2014/BSV_no15_du_27-05-14_cle0f3e54.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2014/BSV_no15_du_27-05-14_cle0f3e54.html)

#### Légumes frais

- BSV_No9_legumes_frais_7_juin_2013_cle8a42f7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2013/BSV_No9_legumes_frais_7_juin_2013_cle8a42f7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2013/BSV_No9_legumes_frais_7_juin_2013_cle8a42f7.html)
- BSV_legumes_frais_no8_18_mai_2012_cle8f7dbc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2012/BSV_legumes_frais_no8_18_mai_2012_cle8f7dbc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2012/BSV_legumes_frais_no8_18_mai_2012_cle8f7dbc.html)
- BSV_No15_legumes_frais_19_juillet_2013_cle81afe3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2013/BSV_No15_legumes_frais_19_juillet_2013_cle81afe3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2013/BSV_No15_legumes_frais_19_juillet_2013_cle81afe3.html)

#### Légumes industrie

- BSV_Legumes_industries_2012_No13_-_version_2_cle8bf191-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2012/BSV_Legumes_industries_2012_No13_-_version_2_cle8bf191-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2012/BSV_Legumes_industries_2012_No13_-_version_2_cle8bf191-1.html)
- BSV_Legumes_industries_2014_No13_-_version_2_cle87f962 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2014/BSV_Legumes_industries_2014_No13_-_version_2_cle87f962.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2014/BSV_Legumes_industries_2014_No13_-_version_2_cle87f962.html)

#### Pomme de Terre

- BSV_No_6_pomme_de_terre_16_mai_2014_cle064521 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2014/BSV_No_6_pomme_de_terre_16_mai_2014_cle064521.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2014/BSV_No_6_pomme_de_terre_16_mai_2014_cle064521.html)
- BSV_PdT_n08_20120601_v2_cle82d17c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2012/BSV_PdT_n08_20120601_v2_cle82d17c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2012/BSV_PdT_n08_20120601_v2_cle82d17c.html)
- BSV_No_12_pomme_de_terre_04_juillet_2014_cle0a2ac3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2014/BSV_No_12_pomme_de_terre_04_juillet_2014_cle0a2ac3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2014/BSV_No_12_pomme_de_terre_04_juillet_2014_cle0a2ac3.html)

#### Zones non-Agricoles

- BSV_Cultures_Ornementales_et_ZNA_No5_du_13_06_14_cle85d729 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2014/BSV_Cultures_Ornementales_et_ZNA_No5_du_13_06_14_cle85d729.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2014/BSV_Cultures_Ornementales_et_ZNA_No5_du_13_06_14_cle85d729.html)

### Centre

#### Arboriculture

- BSV_arbo_17 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2014/BSV_arbo_17.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2014/BSV_arbo_17.html)
- BSV_Arbo_Centre_6_11-03-14_cle414993 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_Arbo_Centre_6_11-03-14_cle414993.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_Arbo_Centre_6_11-03-14_cle414993.html)
- BSV_arbo_04 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2013/BSV_arbo_04.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2013/BSV_arbo_04.html)
- BSV_Arbo_27 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2012/BSV_Arbo_27.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2012/BSV_Arbo_27.html)
- BSV_Arboriculture_36 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_Arboriculture_36.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_Arboriculture_36.html)
- BSV_Arbo_Centre_16_11-04-26_cle06b771 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_Arbo_Centre_16_11-04-26_cle06b771.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_Arbo_Centre_16_11-04-26_cle06b771.html)

#### Betterave

- BSV_Betterave_no14_cle8a2eff [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2010/BSV_Betterave_no14_cle8a2eff.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2010/BSV_Betterave_no14_cle8a2eff.html)
- BSV_Betterave_12_001 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2013/BSV_Betterave_12_001.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2013/BSV_Betterave_12_001.html)
- BSV_Betterave_02_002 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2012/BSV_Betterave_02_002.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2012/BSV_Betterave_02_002.html)

#### Bulletins spéciaux

- Note_nationale_abeilles [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2009/Note_nationale_abeilles.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2009/Note_nationale_abeilles.html)

#### Céréales

- bsv_cereales_paille_2010_s23_cle8b462b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2010/bsv_cereales_paille_2010_s23_cle8b462b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2010/bsv_cereales_paille_2010_s23_cle8b462b.html)
- BSV_Cereales_a_paille_2011_S13-1_cle0d7463 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_Cereales_a_paille_2011_S13-1_cle0d7463.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_Cereales_a_paille_2011_S13-1_cle0d7463.html)
- BSV_cereales_paille_20_001 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2013/BSV_cereales_paille_20_001.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2013/BSV_cereales_paille_20_001.html)
- BSV_cereales_paille_07_003 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_cereales_paille_07_003.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_cereales_paille_07_003.html)
- BSV_Cereales_a_paille_2010_S42_cle011f61 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2010/BSV_Cereales_a_paille_2010_S42_cle011f61.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2010/BSV_Cereales_a_paille_2010_S42_cle011f61.html)

#### Lin

- BSV_lin_18 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2014/BSV_lin_18.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2014/BSV_lin_18.html)
- BSV_lin_01 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2014/BSV_lin_01.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2014/BSV_lin_01.html)

#### Légumes

- BSV_legumes_27_001 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2013/BSV_legumes_27_001.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2013/BSV_legumes_27_001.html)
- BSV_legumes_no24_cle46aed9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_legumes_no24_cle46aed9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_legumes_no24_cle46aed9.html)
- bsv_legumes_17 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2013/bsv_legumes_17.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2013/bsv_legumes_17.html)
- COMPIL-BSV_-_Legumes_no18_cle04e91c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/COMPIL-BSV_-_Legumes_no18_cle04e91c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/COMPIL-BSV_-_Legumes_no18_cle04e91c.html)
- BSV_legumes_11_002 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2012/BSV_legumes_11_002.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2012/BSV_legumes_11_002.html)

#### Maïs

- BSV_mais_07 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2013/BSV_mais_07.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2013/BSV_mais_07.html)
- BSV_Mais_no4s22_cle04eb4b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_Mais_no4s22_cle04eb4b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_Mais_no4s22_cle04eb4b.html)

#### Oléagineux

- BSV_oleagineux_09_002 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2012/BSV_oleagineux_09_002.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2012/BSV_oleagineux_09_002.html)
- BSV_oleagineux_05_002 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2012/BSV_oleagineux_05_002.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2012/BSV_oleagineux_05_002.html)
- Avertissement_Colza_Centre9_20090324_cle4c1325 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2009/Avertissement_Colza_Centre9_20090324_cle4c1325.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2009/Avertissement_Colza_Centre9_20090324_cle4c1325.html)
- BSV_Grandes_Cultures_Oleagineux_n_o8_20101026_JC_cle8c24d2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2010/BSV_Grandes_Cultures_Oleagineux_n_o8_20101026_JC_cle8c24d2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2010/BSV_Grandes_Cultures_Oleagineux_n_o8_20101026_JC_cle8c24d2.html)

#### Pomme de Terre

- Pomme_de_terre_Centre_2009_no16_cle477634 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2009/Pomme_de_terre_Centre_2009_no16_cle477634.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2009/Pomme_de_terre_Centre_2009_no16_cle477634.html)
- BSV_Pomme_de_terre_22 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2014/BSV_Pomme_de_terre_22.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2014/BSV_Pomme_de_terre_22.html)

#### Protéagineux

- BSV_Pois_Feverole_2011_no6_V2_cle8d1e15 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_Pois_Feverole_2011_no6_V2_cle8d1e15.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_Pois_Feverole_2011_no6_V2_cle8d1e15.html)
- BSV_Pois_Feverole_2011_no8_cle4d5c14 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_Pois_Feverole_2011_no8_cle4d5c14.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_Pois_Feverole_2011_no8_cle4d5c14.html)
- BSV_proteagineux_14_002 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2012/BSV_proteagineux_14_002.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2012/BSV_proteagineux_14_002.html)
- BSV_Pois_Feverole_2011_no9_cle411b1a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/BSV_Pois_Feverole_2011_no9_cle411b1a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/BSV_Pois_Feverole_2011_no9_cle411b1a.html)
- BSV_pois_08_cle47b73e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2010/BSV_pois_08_cle47b73e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2010/BSV_pois_08_cle47b73e.html)

#### Viticulture

- BSV_Viti_06 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2013/BSV_Viti_06.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2013/BSV_Viti_06.html)

#### Zones non-Agricoles

- BSV_ZNA_02_002 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2012/BSV_ZNA_02_002.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2012/BSV_ZNA_02_002.html)

### Champagne-Ardenne

#### Colza - Tournesol

- 100311_BSV_Colza_tournesol [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2010/100311_BSV_Colza_tournesol.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2010/100311_BSV_Colza_tournesol.html)
- 100624_BSV_Colza_tournesol [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2010/100624_BSV_Colza_tournesol.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2010/100624_BSV_Colza_tournesol.html)
- 100916_BSV_Colza_Tournesol_S37 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2010/100916_BSV_Colza_Tournesol_S37.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2010/100916_BSV_Colza_Tournesol_S37.html)

#### Céréales

- 091008_Bsv_cereales [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2009/091008_Bsv_cereales.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2009/091008_Bsv_cereales.html)
- 091015_Bsv_cereales [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2009/091015_Bsv_cereales.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2009/091015_Bsv_cereales.html)

#### Céréales - Protéagineux Maïs

- 100701_bsv_mais [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2010/100701_bsv_mais.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2010/100701_bsv_mais.html)

#### Grandes cultures

- 37_-_BSV_Grandes_cultures_S46_cle883289 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2013/37_-_BSV_Grandes_cultures_S46_cle883289.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2013/37_-_BSV_Grandes_cultures_S46_cle883289.html)
- 3_-_BSV_Grandes_cultures_S11_cle8cbba8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2013/3_-_BSV_Grandes_cultures_S11_cle8cbba8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2013/3_-_BSV_Grandes_cultures_S11_cle8cbba8.html)
- 11_-_BSV_Grandes_cultures_S18-2-1_cle8d5b6e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2014/11_-_BSV_Grandes_cultures_S18-2-1_cle8d5b6e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2014/11_-_BSV_Grandes_cultures_S18-2-1_cle8d5b6e.html)
- 38_-_BSV_Grandes_cultures_S45_cle8c1431 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2012/38_-_BSV_Grandes_cultures_S45_cle8c1431.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2012/38_-_BSV_Grandes_cultures_S45_cle8c1431.html)

#### Viticulture

- BSV_2011_n5_cle453b96 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2011/BSV_2011_n5_cle453b96.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2011/BSV_2011_n5_cle453b96.html)

#### Zones non-Agricoles

- 2_-_BSV_ZNA_S14_cle097ddd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2013/2_-_BSV_ZNA_S14_cle097ddd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2013/2_-_BSV_ZNA_S14_cle097ddd.html)

### Corse

#### Agrumes-Kiwi

- BSV_Agrumes_Kiwi_2_5juin2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2013/BSV_Agrumes_Kiwi_2_5juin2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2013/BSV_Agrumes_Kiwi_2_5juin2013.html)
- BSV_Agrumes_Kiwi_1_27mai2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2013/BSV_Agrumes_Kiwi_1_27mai2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2013/BSV_Agrumes_Kiwi_1_27mai2013.html)
- 01F57Zd3vFWg1itsp44SIT8E [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2011/01F57Zd3vFWg1itsp44SIT8E.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2011/01F57Zd3vFWg1itsp44SIT8E.html)

#### Arboriculture

- ED30Szj0vP8Pl70r449z76rF [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2010/ED30Szj0vP8Pl70r449z76rF.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2010/ED30Szj0vP8Pl70r449z76rF.html)
- 5CFA3U3t631V6Z5ZdLm3MyU1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2012/5CFA3U3t631V6Z5ZdLm3MyU1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2012/5CFA3U3t631V6Z5ZdLm3MyU1.html)
- 8E040h806ch6Smo9MEVkK80q [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2011/8E040h806ch6Smo9MEVkK80q.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2011/8E040h806ch6Smo9MEVkK80q.html)

#### Maraîchage

- ACAB6BCBEMIh9FN9YXn91xWQ [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2010/ACAB6BCBEMIh9FN9YXn91xWQ.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2010/ACAB6BCBEMIh9FN9YXn91xWQ.html)
- 2CCEeMC2B19v784m364OMugE [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2012/2CCEeMC2B19v784m364OMugE.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2012/2CCEeMC2B19v784m364OMugE.html)

#### Viticulture

- E99025SV4XDTwiyKO3pG6i7C [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2010/E99025SV4XDTwiyKO3pG6i7C.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2010/E99025SV4XDTwiyKO3pG6i7C.html)

### Franche-Comté

#### Grandes cultures

- BSV_43FCO_GC_2009_07_28_20 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2009/BSV_43FCO_GC_2009_07_28_20.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2009/BSV_43FCO_GC_2009_07_28_20.html)
- BSV_43FCO_GC_2013_04_09_07 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2013/BSV_43FCO_GC_2013_04_09_07.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2013/BSV_43FCO_GC_2013_04_09_07.html)
- Lettre_liaison_Ecophyto_1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2011/Lettre_liaison_Ecophyto_1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2011/Lettre_liaison_Ecophyto_1.html)
- BSV_43FCO_GC_2010_05_10_12 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2010/BSV_43FCO_GC_2010_05_10_12.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2010/BSV_43FCO_GC_2010_05_10_12.html)
- BSV_43FCO_GC_2009_10_19_27 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2009/BSV_43FCO_GC_2009_10_19_27.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2009/BSV_43FCO_GC_2009_10_19_27.html)
- BSV_43FCO_GC_2014_05_20_14 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2014/BSV_43FCO_GC_2014_05_20_14.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2014/BSV_43FCO_GC_2014_05_20_14.html)
- BSV_43FCO_GC_2010_10_18_29 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2010/BSV_43FCO_GC_2010_10_18_29.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2010/BSV_43FCO_GC_2010_10_18_29.html)

#### Viticulture

- BSV_FC_Vigne3_170412 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2012/BSV_FC_Vigne3_170412.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2012/BSV_FC_Vigne3_170412.html)
- JV_19_du_060813 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2013/JV_19_du_060813.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2013/JV_19_du_060813.html)
- BSV_FC_Vigne10_070611 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2011/BSV_FC_Vigne10_070611.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2011/BSV_FC_Vigne10_070611.html)
- BSV_FC_Vigne12_210611 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2011/BSV_FC_Vigne12_210611.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2011/BSV_FC_Vigne12_210611.html)
- BSV_Vigne_FC_11_-16_juin_09 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2009/BSV_Vigne_FC_11_-16_juin_09.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2009/BSV_Vigne_FC_11_-16_juin_09.html)
- note_nationale_Vigne_mildiou_oidium_2011_Vdef [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2011/note_nationale_Vigne_mildiou_oidium_2011_Vdef.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2011/note_nationale_Vigne_mildiou_oidium_2011_Vdef.html)
- BSV_FC_Vigne8_210512 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16394/2012/BSV_FC_Vigne8_210512.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16394/2012/BSV_FC_Vigne8_210512.html)

### Guadeloupe

#### Horticulture

- BSV971_Horticulture_ANTHURIUM_25112014-N03_cle03f3b2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17012/2014/BSV971_Horticulture_ANTHURIUM_25112014-N03_cle03f3b2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17012/2014/BSV971_Horticulture_ANTHURIUM_25112014-N03_cle03f3b2.html)

### Haute-Normandie

#### Arboriculture et petits fruits

- BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no12_du_15-05-2012_cle415e3e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no12_du_15-05-2012_cle415e3e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no12_du_15-05-2012_cle415e3e.html)
- BSV_Arboricultuure-Fruits_Transformes__Bretagne-Normandie-Pays_de_la_Loire_no9_du_03-05-2011_cle0fe21f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/BSV_Arboricultuure-Fruits_Transformes__Bretagne-Normandie-Pays_de_la_Loire_no9_du_03-05-2011_cle0fe21f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/BSV_Arboricultuure-Fruits_Transformes__Bretagne-Normandie-Pays_de_la_Loire_no9_du_03-05-2011_cle0fe21f.html)

#### Betterave

- BSV_No18_cle86aa14-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_No18_cle86aa14-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_No18_cle86aa14-2.html)
- BSV_no13_cle855127 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2014/BSV_no13_cle855127.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2014/BSV_no13_cle855127.html)
- BSV_no7_cle013b3f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1152/2014/BSV_no7_cle013b3f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1152/2014/BSV_no7_cle013b3f.html)

#### Céréales

- BSV_13_cereales_Normandie_cle01ccd2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/BSV_13_cereales_Normandie_cle01ccd2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/BSV_13_cereales_Normandie_cle01ccd2.html)
- BSV_11-3_cereales_Normandie_R2014_cle83cb2e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV_11-3_cereales_Normandie_R2014_cle83cb2e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV_11-3_cereales_Normandie_R2014_cle83cb2e.html)
- BSV_8_cereales_Normandie_R2012_cle0a7838 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/BSV_8_cereales_Normandie_R2012_cle0a7838.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/BSV_8_cereales_Normandie_R2012_cle0a7838.html)
- BSV_9_cereales_Normandie_cle81b469 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/BSV_9_cereales_Normandie_cle81b469.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/BSV_9_cereales_Normandie_cle81b469.html)
- BSV_4_cereales_Normandie_cle87b83e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_4_cereales_Normandie_cle87b83e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_4_cereales_Normandie_cle87b83e.html)
- BSV_2_cereales_Normandie_R2011_cle02a37b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/BSV_2_cereales_Normandie_R2011_cle02a37b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/BSV_2_cereales_Normandie_R2011_cle02a37b.html)

#### Horticulture

- BSV_Ornement_Normandie_n-17-2011_cle095ecd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/BSV_Ornement_Normandie_n-17-2011_cle095ecd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/BSV_Ornement_Normandie_n-17-2011_cle095ecd.html)
- BSV_Ornement_Normandie_n-20-2012_cle04a5f3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_Ornement_Normandie_n-20-2012_cle04a5f3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_Ornement_Normandie_n-20-2012_cle04a5f3.html)
- BSV_Ornement_Normandie_n-05-2011_cle0cf7d2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/BSV_Ornement_Normandie_n-05-2011_cle0cf7d2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/BSV_Ornement_Normandie_n-05-2011_cle0cf7d2.html)
- BSV_Ornement_Normandie_n-22-2012_cle0bf1cb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_Ornement_Normandie_n-22-2012_cle0bf1cb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_Ornement_Normandie_n-22-2012_cle0bf1cb.html)
- BSV_Ornement_Normandie_n-8-2010_cle4d3b61 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/BSV_Ornement_Normandie_n-8-2010_cle4d3b61.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/BSV_Ornement_Normandie_n-8-2010_cle4d3b61.html)
- BSV_Ornement_Normandie_11_cle0f71e8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_Ornement_Normandie_11_cle0f71e8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_Ornement_Normandie_11_cle0f71e8.html)
- BSV_Ornement_Normandie_10_cle0b371a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_Ornement_Normandie_10_cle0b371a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_Ornement_Normandie_10_cle0b371a.html)

#### Lin

- BSV_09_cle4c32d5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_09_cle4c32d5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_09_cle4c32d5.html)
- BSV_LIN_09_08_cle87e7eb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_LIN_09_08_cle87e7eb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_LIN_09_08_cle87e7eb.html)
- BSV6_cle8d4471 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2014/BSV6_cle8d4471.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2014/BSV6_cle8d4471.html)
- Bsv_lin_18_a_cle09d729 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/Bsv_lin_18_a_cle09d729.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/Bsv_lin_18_a_cle09d729.html)
- BSV_LIN_2013_Normandie_No9_cle4ef1af [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_LIN_2013_Normandie_No9_cle4ef1af.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_LIN_2013_Normandie_No9_cle4ef1af.html)

#### Légumes

- 2012_BSV_calvados_htenormandie_29_cle81977c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/2012_BSV_calvados_htenormandie_29_cle81977c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/2012_BSV_calvados_htenormandie_29_cle81977c.html)
- 2011_BSV_manche_36_cle8c9518 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/2011_BSV_manche_36_cle8c9518.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/2011_BSV_manche_36_cle8c9518.html)
- 2012_BSV_calvados_htenormandie_26_cle823e9b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/2012_BSV_calvados_htenormandie_26_cle823e9b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/2012_BSV_calvados_htenormandie_26_cle823e9b.html)
- 2010_BSV_manche_27_cle843f82 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/2010_BSV_manche_27_cle843f82.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/2010_BSV_manche_27_cle843f82.html)
- 2011_BSV_calvados_htenormandie_27_cle8a66fc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/2011_BSV_calvados_htenormandie_27_cle8a66fc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/2011_BSV_calvados_htenormandie_27_cle8a66fc.html)
- 2009_BSV_manche_07_cle8ef897 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/2009_BSV_manche_07_cle8ef897.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/2009_BSV_manche_07_cle8ef897.html)
- 2010_BSV_calvados_htenormandie_18_cle891d53 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/2010_BSV_calvados_htenormandie_18_cle891d53.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/2010_BSV_calvados_htenormandie_18_cle891d53.html)

#### Légumes - Calvados,Haute-Normandie

- 2013_BSV_Bilan2012_Calvados_htenormandie_cle0352d6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/2013_BSV_Bilan2012_Calvados_htenormandie_cle0352d6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/2013_BSV_Bilan2012_Calvados_htenormandie_cle0352d6.html)

#### Légumes - Manche

- 2014_BSV_manche_37_cle8ad953 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/2014_BSV_manche_37_cle8ad953.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/2014_BSV_manche_37_cle8ad953.html)

#### Oléagineux

- BSV_colza_normandie_No8_semaine_43_du_17-10_au_23-10-2012_v1_cle0cf2cd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2012/BSV_colza_normandie_No8_semaine_43_du_17-10_au_23-10-2012_v1_cle0cf2cd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2012/BSV_colza_normandie_No8_semaine_43_du_17-10_au_23-10-2012_v1_cle0cf2cd.html)
- BSV_colza_Normandie_V1_N_o15_SEMAINE_10_du_29-2_au_6-3-12_cle0a6d5f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2012/BSV_colza_Normandie_V1_N_o15_SEMAINE_10_du_29-2_au_6-3-12_cle0a6d5f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2012/BSV_colza_Normandie_V1_N_o15_SEMAINE_10_du_29-2_au_6-3-12_cle0a6d5f.html)
- BSV_N_o2_COLZA_RECOLTE_2011_NORMANDIE_semaine_36_draaf-JRV_-_cle0f613b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/BSV_N_o2_COLZA_RECOLTE_2011_NORMANDIE_semaine_36_draaf-JRV_-_cle0f613b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/BSV_N_o2_COLZA_RECOLTE_2011_NORMANDIE_semaine_36_draaf-JRV_-_cle0f613b.html)
- BSV_colza_Normandie_V1-2_N_o7_SEMAINE_42_du_12-10-11_au_18-10-11_cle8ef65b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/BSV_colza_Normandie_V1-2_N_o7_SEMAINE_42_du_12-10-11_au_18-10-11_cle8ef65b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/BSV_colza_Normandie_V1-2_N_o7_SEMAINE_42_du_12-10-11_au_18-10-11_cle8ef65b.html)
- BSV_colza_normandie_No6_semaine_41_du_3-10_au_9-10-2012_v1_cle8199f1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_colza_normandie_No6_semaine_41_du_3-10_au_9-10-2012_v1_cle8199f1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_colza_normandie_No6_semaine_41_du_3-10_au_9-10-2012_v1_cle8199f1.html)
- BSV_OLEAGINEUX_No9-14_V1-1_cle4c733d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2014/BSV_OLEAGINEUX_No9-14_V1-1_cle4c733d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2014/BSV_OLEAGINEUX_No9-14_V1-1_cle4c733d.html)
- BSV_No25_SEMAINE_18_DU_24-4-13_AU_1-5-13_V1_cle83ad28 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_No25_SEMAINE_18_DU_24-4-13_AU_1-5-13_V1_cle83ad28.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_No25_SEMAINE_18_DU_24-4-13_AU_1-5-13_V1_cle83ad28.html)
- BSV_OLEAGINEUX_No24-14_V1_cle0ec611 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV_OLEAGINEUX_No24-14_V1_cle0ec611.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV_OLEAGINEUX_No24-14_V1_cle0ec611.html)
- BSV-OLEAGINEUX-No2-14_cle4a8175 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV-OLEAGINEUX-No2-14_cle4a8175.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV-OLEAGINEUX-No2-14_cle4a8175.html)
- BSV_N_o26_COLZA_RECOLTE_2011_DRAAF_NORMANDIE_sem_17_v1_cle8f4aef [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/BSV_N_o26_COLZA_RECOLTE_2011_DRAAF_NORMANDIE_sem_17_v1_cle8f4aef.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/BSV_N_o26_COLZA_RECOLTE_2011_DRAAF_NORMANDIE_sem_17_v1_cle8f4aef.html)

#### Pomme de Terre

- BSV_pdt_13_17_cle8cef33 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_pdt_13_17_cle8cef33.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_pdt_13_17_cle8cef33.html)
- BSV_pdt_10_02_cle8c52fe [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/BSV_pdt_10_02_cle8c52fe.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/BSV_pdt_10_02_cle8c52fe.html)
- bilanBSV_11_cle4f1ed3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/bilanBSV_11_cle4f1ed3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/bilanBSV_11_cle4f1ed3.html)
- BSV_pdt_09_17_cle8f8bac [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_pdt_09_17_cle8f8bac.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_pdt_09_17_cle8f8bac.html)
- BSV_pdt_09_13_cle841d32 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_pdt_09_13_cle841d32.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_pdt_09_13_cle841d32.html)
- BSV_pdt_09_01_cle8a7831 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_pdt_09_01_cle8a7831.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_pdt_09_01_cle8a7831.html)

#### Protéagineux

- BSV_no4_proteagineux_Normandie_R2013_ok_cle8b7b1e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_no4_proteagineux_Normandie_R2013_ok_cle8b7b1e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_no4_proteagineux_Normandie_R2013_ok_cle8b7b1e.html)
- BSV_11_proteagineux_Normandie_cle8c8ae4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2010/BSV_11_proteagineux_Normandie_cle8c8ae4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2010/BSV_11_proteagineux_Normandie_cle8c8ae4.html)
- BSV_11_proteagineux_Normandie_2012_cle888cde-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2012/BSV_11_proteagineux_Normandie_2012_cle888cde-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2012/BSV_11_proteagineux_Normandie_2012_cle888cde-1.html)
- BSV_12_proteagineux_Normandie_R2011_cle02d111 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/BSV_12_proteagineux_Normandie_R2011_cle02d111.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/BSV_12_proteagineux_Normandie_R2011_cle02d111.html)
- BSV_10_proteagineux_Normandie_2012_cle8bc461 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2012/BSV_10_proteagineux_Normandie_2012_cle8bc461.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2012/BSV_10_proteagineux_Normandie_2012_cle8bc461.html)

#### Zones non-Agricoles

- BSV_ZNA_1107_cle088366 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2011/BSV_ZNA_1107_cle088366.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2011/BSV_ZNA_1107_cle088366.html)
- BSV_ZNA_03_cle0b3cb8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_ZNA_03_cle0b3cb8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_ZNA_03_cle0b3cb8.html)
- BSV_Normandie_no2-2014_reduit_cle8658d4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2014/BSV_Normandie_no2-2014_reduit_cle8658d4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2014/BSV_Normandie_no2-2014_reduit_cle8658d4.html)
- BSV_ZNA_06_cle0d4871 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2009/BSV_ZNA_06_cle0d4871.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2009/BSV_ZNA_06_cle0d4871.html)

### Languedoc-Roussillon

#### Riz

- Pomacea_fiche_terrain_riz_paysage_ecran [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17005/2014/Pomacea_fiche_terrain_riz_paysage_ecran.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17005/2014/Pomacea_fiche_terrain_riz_paysage_ecran.html)

#### Viticulture

- BSV_Viti_No19_du_29_juillet_2014_cle08937a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17005/2014/BSV_Viti_No19_du_29_juillet_2014_cle08937a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17005/2014/BSV_Viti_No19_du_29_juillet_2014_cle08937a.html)

### Limousin

#### Châtaigne

- BSV_5_chataigne_28aout2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2013/BSV_5_chataigne_28aout2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2013/BSV_5_chataigne_28aout2013.html)

#### Fraise-Framboise Aquitaine-Limousin

- BSV_N_14-Fraise_Framboise-2014.08.07 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1179/2014/BSV_N_14-Fraise_Framboise-2014.08.07.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1179/2014/BSV_N_14-Fraise_Framboise-2014.08.07.html)

#### Grandes cultures

- BSV_GC_13_LIM_27avril2010_cle088887-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2010/BSV_GC_13_LIM_27avril2010_cle088887-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2010/BSV_GC_13_LIM_27avril2010_cle088887-1.html)

#### Horticulture

- BSV04-Horticulture-Pepinieres-22-04-2011_cle0a8923 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2011/BSV04-Horticulture-Pepinieres-22-04-2011_cle0a8923.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2011/BSV04-Horticulture-Pepinieres-22-04-2011_cle0a8923.html)

#### Maraîchage, Pomme de Terre - Aquitaine-Limousin


#### Noix

- BSV_NOIX_SO_9_06092013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2013/BSV_NOIX_SO_9_06092013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2013/BSV_NOIX_SO_9_06092013.html)
- BSV_NOIX_SO_6_24_juin_2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2014/BSV_NOIX_SO_6_24_juin_2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2014/BSV_NOIX_SO_6_24_juin_2014.html)

#### Pommes-Poires

- BSV_POMPOI_3_LIM_15mars2011_cle05e836 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2011/BSV_POMPOI_3_LIM_15mars2011_cle05e836.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2011/BSV_POMPOI_3_LIM_15mars2011_cle05e836.html)
- BSV_POMPOIR_12_18juin2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2014/BSV_POMPOIR_12_18juin2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2014/BSV_POMPOIR_12_18juin2014.html)
- BSV_POMME_15_LIM_7aout2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2014/BSV_POMME_15_LIM_7aout2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2014/BSV_POMME_15_LIM_7aout2014.html)
- BSV_POMPOI_15_7juin2013 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2013/BSV_POMPOI_15_7juin2013.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2013/BSV_POMPOI_15_7juin2013.html)

### Lorraine

#### Arboriculture fruitière

- BSV_arbo_1_cle0a7af5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2010/BSV_arbo_1_cle0a7af5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2010/BSV_arbo_1_cle0a7af5.html)
- BSVarbo3_cle8618a5-3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2013/BSVarbo3_cle8618a5-3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2013/BSVarbo3_cle8618a5-3.html)
- BSVarbo15_cle897df1-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2011/BSVarbo15_cle897df1-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2011/BSVarbo15_cle897df1-2.html)
- BSVarbo5_cle8dc254-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2012/BSVarbo5_cle8dc254-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2012/BSVarbo5_cle8dc254-1.html)
- BSVarbo10_cle814295 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2010/BSVarbo10_cle814295.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2010/BSVarbo10_cle814295.html)
- BSVarbo12 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2014/BSVarbo12.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2014/BSVarbo12.html)

#### Grandes cultures

- BSVn10_26-10-11_2_cle0f4be8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2011/BSVn10_26-10-11_2_cle0f4be8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2011/BSVn10_26-10-11_2_cle0f4be8.html)
- 18_01_BSV_3_26-08-09_cle0992a8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2009/18_01_BSV_3_26-08-09_cle0992a8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2009/18_01_BSV_3_26-08-09_cle0992a8.html)
- BSVno30_18-06-14_cle428c1a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2014/BSVno30_18-06-14_cle428c1a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2014/BSVno30_18-06-14_cle428c1a.html)
- BSVn10 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2014/BSVn10.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2014/BSVn10.html)

#### Maraîchage

- BSVleg22_cle8a58a1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2012/BSVleg22_cle8a58a1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2012/BSVleg22_cle8a58a1.html)
- BSVleg18 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2014/BSVleg18.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2014/BSVleg18.html)
- BSVleg9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2014/BSVleg9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2014/BSVleg9.html)
- BSVleg3_cle037b91-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2013/BSVleg3_cle037b91-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2013/BSVleg3_cle037b91-1.html)
- BSVleg2_cle055aaa [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2013/BSVleg2_cle055aaa.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2013/BSVleg2_cle055aaa.html)

#### Pépinières-Horticulture

- BSV15horti-pepi [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2014/BSV15horti-pepi.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2014/BSV15horti-pepi.html)
- BSV16horti-pepi_cle0ce528 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2010/BSV16horti-pepi_cle0ce528.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2010/BSV16horti-pepi_cle0ce528.html)
- BSV7horti-pepi_cle8cbea5-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2012/BSV7horti-pepi_cle8cbea5-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2012/BSV7horti-pepi_cle8cbea5-2.html)
- BSV19horti-pepi_cle02119a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2011/BSV19horti-pepi_cle02119a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2011/BSV19horti-pepi_cle02119a.html)

#### Viticulture

- BSVviti2_cle8f5f39-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2012/BSVviti2_cle8f5f39-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2012/BSVviti2_cle8f5f39-1.html)
- BSVviti20 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2014/BSVviti20.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2014/BSVviti20.html)
- BSVviti13_cle8b413b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2011/BSVviti13_cle8b413b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2011/BSVviti13_cle8b413b.html)

#### Zones non-Agricoles

- BSV7ZNA [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1137/2014/BSV7ZNA.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1137/2014/BSV7ZNA.html)

### Martinique

#### Toutes cultures

- BSV_juin2013_cle08ce1a-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17054/2013/BSV_juin2013_cle08ce1a-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17054/2013/BSV_juin2013_cle08ce1a-1.html)

### Midi-Pyrenées

#### Ail

- 09_bsv_ail_03052012_V3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2012/09_bsv_ail_03052012_V3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2012/09_bsv_ail_03052012_V3.html)
- 05_bsv_ail_05042012_V3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2012/05_bsv_ail_05042012_V3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2012/05_bsv_ail_05042012_V3.html)

#### Arboriculture

- Bulletin-de-sante-du-vegetal-no24 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no24.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no24.html)

#### Grandes cultures

- BSV_GC_MP_10_4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2010/BSV_GC_MP_10_4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2010/BSV_GC_MP_10_4.html)
- BSV_GC_MP_10_11 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2010/BSV_GC_MP_10_11.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2010/BSV_GC_MP_10_11.html)
- bsv_grandes_cultures_N15_28_02_13_V3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2013/bsv_grandes_cultures_N15_28_02_13_V3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2013/bsv_grandes_cultures_N15_28_02_13_V3.html)
- bsv_grandes_cultures_colza_N6_V3-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2013/bsv_grandes_cultures_colza_N6_V3-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2013/bsv_grandes_cultures_colza_N6_V3-2.html)
- Bulletin-de-sante-du-vegetal-no24-1675 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no24-1675.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no24-1675.html)

#### Maraîchage

- BSV_MARAICHAGE_N12_17072014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2014/BSV_MARAICHAGE_N12_17072014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2014/BSV_MARAICHAGE_N12_17072014.html)

#### Melon

- BSV_MELON_N_13_20062013_V3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2013/BSV_MELON_N_13_20062013_V3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2013/BSV_MELON_N_13_20062013_V3.html)
- BSV_MELON_N_18_26072012_V3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2012/BSV_MELON_N_18_26072012_V3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2012/BSV_MELON_N_18_26072012_V3.html)

#### Noisettes

- BSV_No16-NOISETTES-2013.08.09 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2013/BSV_No16-NOISETTES-2013.08.09.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2013/BSV_No16-NOISETTES-2013.08.09.html)

#### Noix

- BSV_NOIX_SO_2_23avril2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q1190/2014/BSV_NOIX_SO_2_23avril2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q1190/2014/BSV_NOIX_SO_2_23avril2014.html)

#### Viticulture - Aveyron

- Bulletin-de-sante-du-vegetal-no12-1877 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no12-1877.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no12-1877.html)

#### Viticulture - Cahors, Lot

- Bulletin-de-sante-du-vegetal-no16-1927 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no16-1927.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no16-1927.html)

#### Viticulture - Fronton

- Bulletin-de-sante-du-vegetal-no19-2055 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no19-2055.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no19-2055.html)

#### Viticulture - Gaillac

- Bulletin-de-sante-du-vegetal-no19-2230 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no19-2230.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no19-2230.html)
- Bulletin-de-sante-du-vegetal-no8-1760 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no8-1760.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no8-1760.html)

#### Viticulture - Gascogne, St Mont, Madiran

- Bulletin-de-sante-du-vegetal-no16-2046 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no16-2046.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no16-2046.html)
- Bulletin-de-sante-du-vegetal-no1-771 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2010/Bulletin-de-sante-du-vegetal-no1-771.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2010/Bulletin-de-sante-du-vegetal-no1-771.html)

#### Viticulture - Raisin de Table

- Bulletin-de-sante-du-vegetal-no12-1820 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no12-1820.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no12-1820.html)
- BSV_RAISIN_N15_08072014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2014/BSV_RAISIN_N15_08072014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2014/BSV_RAISIN_N15_08072014.html)
- 02_bsv_raisin_29032012_V3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2012/02_bsv_raisin_29032012_V3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2012/02_bsv_raisin_29032012_V3.html)
- BSV_RAISIN_N17_22072014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2014/BSV_RAISIN_N17_22072014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2014/BSV_RAISIN_N17_22072014.html)
- 04_bsv_raisin_12042012_V3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2012/04_bsv_raisin_12042012_V3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2012/04_bsv_raisin_12042012_V3.html)

#### Zones non-Agricoles

- Bulletin-de-sante-du-vegetal-no1-1842 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16393/2011/Bulletin-de-sante-du-vegetal-no1-1842.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16393/2011/Bulletin-de-sante-du-vegetal-no1-1842.html)

### Nord-Pas-De-Calais

#### Arboriculture

- BSV_Arboriculture_no24_du_27_mai_2011_cle0225f1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_Arboriculture_no24_du_27_mai_2011_cle0225f1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_Arboriculture_no24_du_27_mai_2011_cle0225f1.html)
- BSV_Arboriculture_no49_du_02092014_cle82df79 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2014/BSV_Arboriculture_no49_du_02092014_cle82df79.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2014/BSV_Arboriculture_no49_du_02092014_cle82df79.html)
- BSV_Arboriculture_no9_du_15042013_cle84d1cd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2013/BSV_Arboriculture_no9_du_15042013_cle84d1cd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2013/BSV_Arboriculture_no9_du_15042013_cle84d1cd.html)
- BSV_Arboriculture_no38_du_27_septembre_2011_cle81baae [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_Arboriculture_no38_du_27_septembre_2011_cle81baae.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_Arboriculture_no38_du_27_septembre_2011_cle81baae.html)
- BSV_Arboriculture_no42_du_01072014_cle88a477 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2014/BSV_Arboriculture_no42_du_01072014_cle88a477.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2014/BSV_Arboriculture_no42_du_01072014_cle88a477.html)
- BSV_Arboriculture_no39_du_11_juin_2012_cle8ccdff [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_Arboriculture_no39_du_11_juin_2012_cle8ccdff.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_Arboriculture_no39_du_11_juin_2012_cle8ccdff.html)
- BSV_Arboriculture_n_34_du_1er_juin_2012_cle81dcf7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_Arboriculture_n_34_du_1er_juin_2012_cle81dcf7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_Arboriculture_n_34_du_1er_juin_2012_cle81dcf7.html)
- BSV_Arboriculture_no47_du_27_juin_2012_cle85d7b3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_Arboriculture_no47_du_27_juin_2012_cle85d7b3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_Arboriculture_no47_du_27_juin_2012_cle85d7b3.html)
- BSV_Arboriculture_no7_du_4_avril_2011_cle0ba19b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_Arboriculture_no7_du_4_avril_2011_cle0ba19b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_Arboriculture_no7_du_4_avril_2011_cle0ba19b.html)

#### Grandes cultures

- 15BSVGC05062012_cle0b33a1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/15BSVGC05062012_cle0b33a1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/15BSVGC05062012_cle0b33a1.html)
- 34BSVGC15102013_cle0947e2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2013/34BSVGC15102013_cle0947e2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2013/34BSVGC15102013_cle0947e2.html)
- Note_nationale_Ambroisie_cle82f191 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/Note_nationale_Ambroisie_cle82f191.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/Note_nationale_Ambroisie_cle82f191.html)
- 17BSVGC15062011_cle0751f9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/17BSVGC15062011_cle0751f9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/17BSVGC15062011_cle0751f9.html)
- 29BSVGC140910_cle8963c8-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2010/29BSVGC140910_cle8963c8-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2010/29BSVGC140910_cle8963c8-1.html)

#### Légumes

- BSV_Legumes_no33_cle4e7e9d-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_Legumes_no33_cle4e7e9d-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_Legumes_no33_cle4e7e9d-1.html)
- BSV_Legumes_no3_cle01ea33-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_Legumes_no3_cle01ea33-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_Legumes_no3_cle01ea33-1.html)
- BSV_Legumes_no30_cle49eb49 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2010/BSV_Legumes_no30_cle49eb49.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2010/BSV_Legumes_no30_cle49eb49.html)
- BSV_Legumes_no13_cle4531f2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_Legumes_no13_cle4531f2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_Legumes_no13_cle4531f2.html)
- BSV_Legumes_no21_cle49da1f-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_Legumes_no21_cle49da1f-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_Legumes_no21_cle49da1f-2.html)
- BSV_Legumes_no15_cle443171-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_Legumes_no15_cle443171-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_Legumes_no15_cle443171-2.html)
- BSV_Legumes_no7_cle0912d2-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_Legumes_no7_cle0912d2-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_Legumes_no7_cle0912d2-1.html)

#### Pomme de Terre

- BSV_pomme_de_terre_n_16_du_20juin_2012_cle872188 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_pomme_de_terre_n_16_du_20juin_2012_cle872188.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_pomme_de_terre_n_16_du_20juin_2012_cle872188.html)
- BSVPDT20130723_num31_30_juillet_2013_cle457261 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2013/BSVPDT20130723_num31_30_juillet_2013_cle457261.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2013/BSVPDT20130723_num31_30_juillet_2013_cle457261.html)
- BSV_pomme_de_terre_n_o7_du_24_avril_2012_cle0cdf41 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_pomme_de_terre_n_o7_du_24_avril_2012_cle0cdf41.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_pomme_de_terre_n_o7_du_24_avril_2012_cle0cdf41.html)
- BSV_pomme_de_terre_n_o5_du_12_avril_2011-1_cle08321f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_pomme_de_terre_n_o5_du_12_avril_2011-1_cle08321f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_pomme_de_terre_n_o5_du_12_avril_2011-1_cle08321f.html)
- BSVPDT20130415_num05_cle0ec9c7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2013/BSVPDT20130415_num05_cle0ec9c7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2013/BSVPDT20130415_num05_cle0ec9c7.html)
- BSV_pomme_de_terre_n_o25_du_24_aout_2010_cle04fc6d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2010/BSV_pomme_de_terre_n_o25_du_24_aout_2010_cle04fc6d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2010/BSV_pomme_de_terre_n_o25_du_24_aout_2010_cle04fc6d.html)

#### Zones non-Agricoles

- BSV_JEV_2012-no5_cle422e12 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2012/BSV_JEV_2012-no5_cle422e12.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2012/BSV_JEV_2012-no5_cle422e12.html)

### Pays-De-La-Loire

#### Arboriculture

- bsv_arboriculture_20110420-1_10__cle0cab87 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2011/bsv_arboriculture_20110420-1_10__cle0cab87.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2011/bsv_arboriculture_20110420-1_10__cle0cab87.html)
- 2010_flash_2_cle066a15 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2010/2010_flash_2_cle066a15.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2010/2010_flash_2_cle066a15.html)

#### Fruits transformés

- BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no11_du_09-05-2012_cle4117c7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16961/2012/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no11_du_09-05-2012_cle4117c7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16961/2012/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no11_du_09-05-2012_cle4117c7.html)
- BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_26-03-2013_note_national_cle8e19a6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16954/2013/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_26-03-2013_note_national_cle8e19a6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16954/2013/BSV_Arboriculture-Fruits_transformes_Bretagne-Normandie-Pays_de_la_Loire_no2_du_26-03-2013_note_national_cle8e19a6.html)

#### Grandes cultures

- 30_bsv_gc_14_06_2011_cle0b2ea9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2011/30_bsv_gc_14_06_2011_cle0b2ea9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2011/30_bsv_gc_14_06_2011_cle0b2ea9.html)
- 22_bsv_gc_04_05_2010_cle022e2c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2010/22_bsv_gc_04_05_2010_cle022e2c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2010/22_bsv_gc_04_05_2010_cle022e2c.html)
- BSV_GC_du_11_fevrier_2014_2_cle01b924 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2014/BSV_GC_du_11_fevrier_2014_2_cle01b924.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2014/BSV_GC_du_11_fevrier_2014_2_cle01b924.html)
- BSV_GC_02-09-2014_28_cle05178c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2014/BSV_GC_02-09-2014_28_cle05178c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2014/BSV_GC_02-09-2014_28_cle05178c.html)
- bsv_grandescultures_20110503_24__cle083e91 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2011/bsv_grandescultures_20110503_24__cle083e91.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2011/bsv_grandescultures_20110503_24__cle083e91.html)
- bsv_grandescultures_20120306_4__cle49ab89 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2012/bsv_grandescultures_20120306_4__cle49ab89.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2012/bsv_grandescultures_20120306_4__cle49ab89.html)
- 2009_13__cle8c15cd-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2010/2009_13__cle8c15cd-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2010/2009_13__cle8c15cd-1.html)

#### Horticulture

- bsv_09_ornement_20140711_cle84eab1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2014/bsv_09_ornement_20140711_cle84eab1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2014/bsv_09_ornement_20140711_cle84eab1.html)
- bsv_3_Flash_Ornement20140728_cle8e64ac [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2014/bsv_3_Flash_Ornement20140728_cle8e64ac.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2014/bsv_3_Flash_Ornement20140728_cle8e64ac.html)
- 2010_6__cle0c39b8-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2010/2010_6__cle0c39b8-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2010/2010_6__cle0c39b8-1.html)

#### Légumes

- 20130606_bsv_maraichage_13_cle46d94d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2013/20130606_bsv_maraichage_13_cle46d94d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2013/20130606_bsv_maraichage_13_cle46d94d.html)
- 20130912_bsv_maraichage_26_cle4ed671 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2013/20130912_bsv_maraichage_26_cle4ed671.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2013/20130912_bsv_maraichage_26_cle4ed671.html)
- bsvmaraichage_20130329_3_cle8d1bdc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2013/bsvmaraichage_20130329_3_cle8d1bdc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2013/bsvmaraichage_20130329_3_cle8d1bdc.html)
- 2010_7__cle0fe148 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2010/2010_7__cle0fe148.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2010/2010_7__cle0fe148.html)
- 2010_6__cle0c39b8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2010/2010_6__cle0c39b8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2010/2010_6__cle0c39b8.html)

#### Viticulture

- 14_bsv_vigne_05_juillet_2012_cle856631 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2012/14_bsv_vigne_05_juillet_2012_cle856631.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2012/14_bsv_vigne_05_juillet_2012_cle856631.html)
- bsv_viti_11_20_06_2013_cle0ffbf1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2013/bsv_viti_11_20_06_2013_cle0ffbf1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2013/bsv_viti_11_20_06_2013_cle0ffbf1.html)
- 2009_2__cle0ed43b-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2009/2009_2__cle0ed43b-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2009/2009_2__cle0ed43b-1.html)

#### Zones non-Agricoles

- bsv_zna_20130801_9_cle8215c1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2013/bsv_zna_20130801_9_cle8215c1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2013/bsv_zna_20130801_9_cle8215c1.html)
- bsv_zna_mai_2011_4__cle81e41d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2011/bsv_zna_mai_2011_4__cle81e41d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2011/bsv_zna_mai_2011_4__cle81e41d.html)
- bsv_aout_2010-2_cle0a9541 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2010/bsv_aout_2010-2_cle0a9541.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2010/bsv_aout_2010-2_cle0a9541.html)

### Picardie

#### Arboriculture

- BSV_Arboriculture_FT_no6_du_24032014_cle448887 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2014/BSV_Arboriculture_FT_no6_du_24032014_cle448887.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2014/BSV_Arboriculture_FT_no6_du_24032014_cle448887.html)
- BSV_Arboriculture_no26_du_13052014_cle81fe48 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2014/BSV_Arboriculture_no26_du_13052014_cle81fe48.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2014/BSV_Arboriculture_no26_du_13052014_cle81fe48.html)
- BSV_Arboriculture_no24_du_27_mai_2011_cle0225f1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_Arboriculture_no24_du_27_mai_2011_cle0225f1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_Arboriculture_no24_du_27_mai_2011_cle0225f1.html)
- BSV_Arboriculture_no10_du_12_avril_2011_cle8d917f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_Arboriculture_no10_du_12_avril_2011_cle8d917f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_Arboriculture_no10_du_12_avril_2011_cle8d917f.html)
- BSV_Arboriculture_no38_du_27_septembre_2011_cle81baae [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2011/BSV_Arboriculture_no38_du_27_septembre_2011_cle81baae.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2011/BSV_Arboriculture_no38_du_27_septembre_2011_cle81baae.html)
- BSV_Arboriculture_FT_no36_du_05062014_cle076171 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2014/BSV_Arboriculture_FT_no36_du_05062014_cle076171.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2014/BSV_Arboriculture_FT_no36_du_05062014_cle076171.html)

#### Grandes cultures

- BSV_GC_no29_28_09_2010_cle023d57 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2010/BSV_GC_no29_28_09_2010_cle023d57.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2010/BSV_GC_no29_28_09_2010_cle023d57.html)
- bsv_30_gc_11_septembre_12_cle0bd3b2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2012/bsv_30_gc_11_septembre_12_cle0bd3b2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2012/bsv_30_gc_11_septembre_12_cle0bd3b2.html)
- BSV_GC_n_o20_20_07_2010_cle8cac67 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2010/BSV_GC_n_o20_20_07_2010_cle8cac67.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2010/BSV_GC_n_o20_20_07_2010_cle8cac67.html)
- BSV_GC_No37_25_10_2011_cle0c88d7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2011/BSV_GC_No37_25_10_2011_cle0c88d7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2011/BSV_GC_No37_25_10_2011_cle0c88d7.html)
- bsv_12_gc_10_mai_12_cle8df51d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2012/bsv_12_gc_10_mai_12_cle8df51d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2012/bsv_12_gc_10_mai_12_cle8df51d.html)
- BSV_GC_no7_29_03_2011_cle47d754 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2011/BSV_GC_no7_29_03_2011_cle47d754.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2011/BSV_GC_no7_29_03_2011_cle47d754.html)
- BSV_GC_no20_du_23.06.09_cle87a316 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2009/BSV_GC_no20_du_23.06.09_cle87a316.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2009/BSV_GC_no20_du_23.06.09_cle87a316.html)
- bsv_27_gc_27_aout_2013_cle0d56de [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2013/bsv_27_gc_27_aout_2013_cle0d56de.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2013/bsv_27_gc_27_aout_2013_cle0d56de.html)
- bsv_25_gc_22_juillet_2014_cle0d9193 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2014/bsv_25_gc_22_juillet_2014_cle0d9193.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2014/bsv_25_gc_22_juillet_2014_cle0d9193.html)
- BSV_GC_No40_15_11_2011_cle0c17ed [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2011/BSV_GC_No40_15_11_2011_cle0c17ed.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2011/BSV_GC_No40_15_11_2011_cle0c17ed.html)
- bsv_34_gc_09-octobre-12_cle819bcd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2012/bsv_34_gc_09-octobre-12_cle819bcd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2012/bsv_34_gc_09-octobre-12_cle819bcd.html)

#### Légumes

- BSV_Legumes_no13ERRATUM_cle816453 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2012/BSV_Legumes_no13ERRATUM_cle816453.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2012/BSV_Legumes_no13ERRATUM_cle816453.html)
- BSV_Legumes_no29_cle43abde [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2010/BSV_Legumes_no29_cle43abde.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2010/BSV_Legumes_no29_cle43abde.html)
- BSV_Legumes_no5_cle0a7ff2-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2012/BSV_Legumes_no5_cle0a7ff2-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2012/BSV_Legumes_no5_cle0a7ff2-1.html)
- Bul_leg_no4_cle4be3c9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16987/2009/Bul_leg_no4_cle4be3c9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16987/2009/Bul_leg_no4_cle4be3c9.html)
- Note_nationale_Ambroisie_1_BSV_21_cle812a7a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2011/Note_nationale_Ambroisie_1_BSV_21_cle812a7a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2011/Note_nationale_Ambroisie_1_BSV_21_cle812a7a.html)

#### Pomme de Terre

- bsv_13_pdt_25_juin_13_cle453463 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2013/bsv_13_pdt_25_juin_13_cle453463.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2013/bsv_13_pdt_25_juin_13_cle453463.html)
- bsv_24_pdt_10_septembre_2013_cle8c5281 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2013/bsv_24_pdt_10_septembre_2013_cle8c5281.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2013/bsv_24_pdt_10_septembre_2013_cle8c5281.html)
- BSV_PDT_no_9_31_05_2012_cle8994c1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2012/BSV_PDT_no_9_31_05_2012_cle8994c1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2012/BSV_PDT_no_9_31_05_2012_cle8994c1.html)
- bsv_09_pdt_28_mai_13_cle07bd36 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2013/bsv_09_pdt_28_mai_13_cle07bd36.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2013/bsv_09_pdt_28_mai_13_cle07bd36.html)
- bsv_02_pdt_11_fev_14_cle0c474f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2014/bsv_02_pdt_11_fev_14_cle0c474f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2014/bsv_02_pdt_11_fev_14_cle0c474f.html)

#### Zones non-Agricoles

- bsv_40_zna__mai_2014_01_cle8af58a-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2014/bsv_40_zna__mai_2014_01_cle8af58a-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2014/bsv_40_zna__mai_2014_01_cle8af58a-1.html)
- bsv_32_aout_2013_01_ambroisie_cle8311f4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13950/2013/bsv_32_aout_2013_01_ambroisie_cle8311f4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13950/2013/bsv_32_aout_2013_01_ambroisie_cle8311f4.html)

### Poitou-Charentes

#### Arboriculture

- bsvarbo_N6_2011-04-18_cle441e5f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2011/bsvarbo_N6_2011-04-18_cle441e5f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2011/bsvarbo_N6_2011-04-18_cle441e5f.html)
- bsv_arbo_61_2014_cle471ee4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2014/bsv_arbo_61_2014_cle471ee4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2014/bsv_arbo_61_2014_cle471ee4.html)
- bsv_arbo_41_2013_cle46e2d1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2013/bsv_arbo_41_2013_cle46e2d1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2013/bsv_arbo_41_2013_cle46e2d1.html)

#### Arboriculture fruitière Aquitaine

- BSV_No18-ARBO-2013-08-01_cle8db614 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2013/BSV_No18-ARBO-2013-08-01_cle8db614.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2013/BSV_No18-ARBO-2013-08-01_cle8db614.html)
- BSV_N_04-ARBO-2014-03-06_cle811cb8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2014/BSV_N_04-ARBO-2014-03-06_cle811cb8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2014/BSV_N_04-ARBO-2014-03-06_cle811cb8.html)

#### Châtaigne

- BSV_2_chataigne_28juin2012_cle4ef6f6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2012/BSV_2_chataigne_28juin2012_cle4ef6f6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2012/BSV_2_chataigne_28juin2012_cle4ef6f6.html)
- BSV_1_chataigne_7juin2013_cle03a2d9 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2013/BSV_1_chataigne_7juin2013_cle03a2d9.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2013/BSV_1_chataigne_7juin2013_cle03a2d9.html)
- BSV_3_chataigne_30juillet2013_cle85215d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2013/BSV_3_chataigne_30juillet2013_cle85215d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2013/BSV_3_chataigne_30juillet2013_cle85215d.html)

#### Grandes cultures

- BSV-GC-PC73_cle485a11 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2011/BSV-GC-PC73_cle485a11.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2011/BSV-GC-PC73_cle485a11.html)
- BSV-GC-PC26_cle4d3356 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2010/BSV-GC-PC26_cle4d3356.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2010/BSV-GC-PC26_cle4d3356.html)
- BSV-GC-PC60_cle4dea1f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2011/BSV-GC-PC60_cle4dea1f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2011/BSV-GC-PC60_cle4dea1f.html)
- AnnexeBSV-GC-PC193_cle87c231 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2014/AnnexeBSV-GC-PC193_cle87c231.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2014/AnnexeBSV-GC-PC193_cle87c231.html)
- BSV-GC-PC55_cle42626f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2011/BSV-GC-PC55_cle42626f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2011/BSV-GC-PC55_cle42626f.html)
- BSV-GC-PC95_cle431161 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2012/BSV-GC-PC95_cle431161.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2012/BSV-GC-PC95_cle431161.html)
- BSV-GC-PC113_cle0b113d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2012/BSV-GC-PC113_cle0b113d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2012/BSV-GC-PC113_cle0b113d.html)
- BSV-GC-PC86_cle45c4ce [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2011/BSV-GC-PC86_cle45c4ce.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2011/BSV-GC-PC86_cle45c4ce.html)
- BSV-GC-PC94_cle479b11 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2012/BSV-GC-PC94_cle479b11.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2012/BSV-GC-PC94_cle479b11.html)
- BSV-GC-PC52_cle46fa13 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2011/BSV-GC-PC52_cle46fa13.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2011/BSV-GC-PC52_cle46fa13.html)

#### Lin-Oléagineux

- BSV_Grandes_Cultures_Lin__Oleagineux_n11_MG_20132205_VF-1_cle0f1c58 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2013/BSV_Grandes_Cultures_Lin__Oleagineux_n11_MG_20132205_VF-1_cle0f1c58.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2013/BSV_Grandes_Cultures_Lin__Oleagineux_n11_MG_20132205_VF-1_cle0f1c58.html)

#### Légumes

- BSV_Cultures_Legumieres_N58_cle08ed2d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2012/BSV_Cultures_Legumieres_N58_cle08ed2d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2012/BSV_Cultures_Legumieres_N58_cle08ed2d.html)
- BSV_Cultures_Legumieres_No117_cle8542c6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2014/BSV_Cultures_Legumieres_No117_cle8542c6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2014/BSV_Cultures_Legumieres_No117_cle8542c6.html)
- BSV_Cultures_Legumieres_No139_cle82191f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2014/BSV_Cultures_Legumieres_No139_cle82191f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2014/BSV_Cultures_Legumieres_No139_cle82191f.html)
- note_nationale_cle831acf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2011/note_nationale_cle831acf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2011/note_nationale_cle831acf.html)
- BSV_Legumes_no5_du_8_juillet_2010_cle8f441d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2010/BSV_Legumes_no5_du_8_juillet_2010_cle8f441d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2010/BSV_Legumes_no5_du_8_juillet_2010_cle8f441d.html)

#### Noix

- BSV_NOIX_SO_10_31_juillet_2014_cle083658 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2014/BSV_NOIX_SO_10_31_juillet_2014_cle083658.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2014/BSV_NOIX_SO_10_31_juillet_2014_cle083658.html)
- BSV_NOIX_SO_11_16aout2012_cle0fdb85 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2012/BSV_NOIX_SO_11_16aout2012_cle0fdb85.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2012/BSV_NOIX_SO_11_16aout2012_cle0fdb85.html)

#### Tabac

- BSV_Poitou-Charente_No30-1_cle4f3663 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2013/BSV_Poitou-Charente_No30-1_cle4f3663.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2013/BSV_Poitou-Charente_No30-1_cle4f3663.html)

#### Viticulture - Charentes

- 48_BSV_Charentes_2012-05-22_cle0282c6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2012/48_BSV_Charentes_2012-05-22_cle0282c6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2012/48_BSV_Charentes_2012-05-22_cle0282c6.html)
- 42_BSV_Charentes_2011-12-21_cle02f587 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2011/42_BSV_Charentes_2011-12-21_cle02f587.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2011/42_BSV_Charentes_2011-12-21_cle02f587.html)

#### Viticulture Haut-Poitou

- 57_BSV_Haut-Poitou_2013-05-22_cle88c1ab [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17009/2013/57_BSV_Haut-Poitou_2013-05-22_cle88c1ab.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17009/2013/57_BSV_Haut-Poitou_2013-05-22_cle88c1ab.html)

### Provence-Alpes-Côte d'Azur

#### Arboriculture

- BSV_PACA_Arbo_4_6_mars_2013_cle081942 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/BSV_PACA_Arbo_4_6_mars_2013_cle081942.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/BSV_PACA_Arbo_4_6_mars_2013_cle081942.html)
- BSVArbo_3_-_26_mai_2010_cle8f1c11 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2010/BSVArbo_3_-_26_mai_2010_cle8f1c11.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2010/BSVArbo_3_-_26_mai_2010_cle8f1c11.html)
- BSV_PACA_Arbo_13_3_aout_2011_cle83d227 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2011/BSV_PACA_Arbo_13_3_aout_2011_cle83d227.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2011/BSV_PACA_Arbo_13_3_aout_2011_cle83d227.html)

#### Grandes cultures

- BSV_GC_n7_21avril11_cle825355 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2011/BSV_GC_n7_21avril11_cle825355.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2011/BSV_GC_n7_21avril11_cle825355.html)

#### Horticulture

- BSVHorti_230910_cle01f644 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2010/BSVHorti_230910_cle01f644.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2010/BSVHorti_230910_cle01f644.html)
- horti12_cle0346d5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2011/horti12_cle0346d5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2011/horti12_cle0346d5.html)

#### Maraîchage

- bsv_maraichage_no55_du_20_septembre_2013_cle043712 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/bsv_maraichage_no55_du_20_septembre_2013_cle043712.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/bsv_maraichage_no55_du_20_septembre_2013_cle043712.html)
- bsv_maraichage_no72_du_2_juin_2014_cle8a3b46 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2014/bsv_maraichage_no72_du_2_juin_2014_cle8a3b46.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2014/bsv_maraichage_no72_du_2_juin_2014_cle8a3b46.html)
- bsv_maraichage_no40_du_8_02_2013_cle03a587 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/bsv_maraichage_no40_du_8_02_2013_cle03a587.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/bsv_maraichage_no40_du_8_02_2013_cle03a587.html)

#### Oléagineux

- BSV_PACA_1_18032014_cle862463 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2014/BSV_PACA_1_18032014_cle862463.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2014/BSV_PACA_1_18032014_cle862463.html)
- BSV_olive_PACA_No_10_26oct2010_cle02435e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2010/BSV_olive_PACA_No_10_26oct2010_cle02435e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2010/BSV_olive_PACA_No_10_26oct2010_cle02435e.html)
- BSVPACA3_200710_cle031cb3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2010/BSVPACA3_200710_cle031cb3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2010/BSVPACA3_200710_cle031cb3.html)
- BSV_PACA_12_01102013_cle0519e6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/BSV_PACA_12_01102013_cle0519e6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/BSV_PACA_12_01102013_cle0519e6.html)
- BSV_PACA_5_03072012_cle819e12 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2012/BSV_PACA_5_03072012_cle819e12.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2012/BSV_PACA_5_03072012_cle819e12.html)
- BSV_Olivier_PACA_12_31102014_cle8d1ad1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2014/BSV_Olivier_PACA_12_31102014_cle8d1ad1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2014/BSV_Olivier_PACA_12_31102014_cle8d1ad1.html)

#### Tomate industrie

- BSV_Tomate_d_industrie_no8_cle46d127-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2012/BSV_Tomate_d_industrie_no8_cle46d127-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2012/BSV_Tomate_d_industrie_no8_cle46d127-1.html)
- BSV_Tomate_d_industrie_no8_cle46d127-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/BSV_Tomate_d_industrie_no8_cle46d127-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/BSV_Tomate_d_industrie_no8_cle46d127-2.html)
- BSV_Tomate_d_industrie_no7_cle46f48c-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/BSV_Tomate_d_industrie_no7_cle46f48c-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/BSV_Tomate_d_industrie_no7_cle46f48c-1.html)
- BSV_Tomate_d_industrie_no3-2_cle8599f7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/BSV_Tomate_d_industrie_no3-2_cle8599f7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/BSV_Tomate_d_industrie_no3-2_cle8599f7.html)

#### Viticulture

- BSVViti_no5_30042013_cle0539dc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/BSVViti_no5_30042013_cle0539dc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/BSVViti_no5_30042013_cle0539dc.html)
- BSVViti_no25_26092013_cle45dbdd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2013/BSVViti_no25_26092013_cle45dbdd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2013/BSVViti_no25_26092013_cle45dbdd.html)

#### Zones non-Agricoles

- BSV_ZNAetPO-53_cle8973eb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2014/BSV_ZNAetPO-53_cle8973eb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2014/BSV_ZNAetPO-53_cle8973eb.html)

### Rhône-Alpes

#### Arboriculture

- BSV_RA_Arbono17du07062011_cle057aed [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2011/BSV_RA_Arbono17du07062011_cle057aed.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2011/BSV_RA_Arbono17du07062011_cle057aed.html)
- Note_nationale_BSV_Ambroisie_2011_cle8ded6f-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2011/Note_nationale_BSV_Ambroisie_2011_cle8ded6f-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2011/Note_nationale_BSV_Ambroisie_2011_cle8ded6f-2.html)
- BSV_RA_Arbono23du17072012_cle0139a2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2012/BSV_RA_Arbono23du17072012_cle0139a2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2012/BSV_RA_Arbono23du17072012_cle0139a2.html)
- BSV_RA_Arbono20du26062012_cle0e141e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2012/BSV_RA_Arbono20du26062012_cle0e141e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2012/BSV_RA_Arbono20du26062012_cle0e141e.html)
- BSV_RA_Arbono30du10092013_cle067bb6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2013/BSV_RA_Arbono30du10092013_cle067bb6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2013/BSV_RA_Arbono30du10092013_cle067bb6.html)
- BSV_bilan_2013_Fruits_a_coque_cle823adf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2013/BSV_bilan_2013_Fruits_a_coque_cle823adf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2013/BSV_bilan_2013_Fruits_a_coque_cle823adf.html)
- BSV_RA_Arbono22du02072013_cle01ede7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2013/BSV_RA_Arbono22du02072013_cle01ede7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2013/BSV_RA_Arbono22du02072013_cle01ede7.html)
- BSV_RA_Arbono16du03062014_cle0a9b1f [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2014/BSV_RA_Arbono16du03062014_cle0a9b1f.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2014/BSV_RA_Arbono16du03062014_cle0a9b1f.html)

#### Grandes cultures

- BSV_RA_GC_no04_du_15_03_2012_cle8a6195 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2012/BSV_RA_GC_no04_du_15_03_2012_cle8a6195.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2012/BSV_RA_GC_no04_du_15_03_2012_cle8a6195.html)
- BSV_RA_GC_no11_du_11_04_2013_cle8386ed [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2013/BSV_RA_GC_no11_du_11_04_2013_cle8386ed.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2013/BSV_RA_GC_no11_du_11_04_2013_cle8386ed.html)
- BSV_RA_GC_no10_du_26_04_2012_cle8938d3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2012/BSV_RA_GC_no10_du_26_04_2012_cle8938d3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2012/BSV_RA_GC_no10_du_26_04_2012_cle8938d3.html)
- BSV_RA_GC_no13_du_14102010_cle45f21b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2010/BSV_RA_GC_no13_du_14102010_cle45f21b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2010/BSV_RA_GC_no13_du_14102010_cle45f21b.html)
- BSV_RA_GC_no23_du_17_02_2011_cle8c13b8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2011/BSV_RA_GC_no23_du_17_02_2011_cle8c13b8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2011/BSV_RA_GC_no23_du_17_02_2011_cle8c13b8.html)
- BSV_RA_GC_no03_du_08_03_2012_cle81452d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2012/BSV_RA_GC_no03_du_08_03_2012_cle81452d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2012/BSV_RA_GC_no03_du_08_03_2012_cle81452d.html)
- BSV_RA_GC_no07_du_14_03_2013_cle84665e-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2013/BSV_RA_GC_no07_du_14_03_2013_cle84665e-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2013/BSV_RA_GC_no07_du_14_03_2013_cle84665e-1.html)

#### Maraîchage

- BSV_RA_2013_MARAICH_07_cle07818c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2013/BSV_RA_2013_MARAICH_07_cle07818c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2013/BSV_RA_2013_MARAICH_07_cle07818c.html)
- BSV_RA_2014_MARAICH_09_cle0489d1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2014/BSV_RA_2014_MARAICH_09_cle0489d1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2014/BSV_RA_2014_MARAICH_09_cle0489d1.html)

#### Pépinières-Horticulture

- ambroisie_cle8ebc61 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2013/ambroisie_cle8ebc61.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2013/ambroisie_cle8ebc61.html)
- BSV_3_cle0c8d2b-2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2011/BSV_3_cle0c8d2b-2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2011/BSV_3_cle0c8d2b-2.html)

#### Viticulture

- BSV-viti-RA-no-17-30-07_2013_cle8ea985 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2013/BSV-viti-RA-no-17-30-07_2013_cle8ea985.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2013/BSV-viti-RA-no-17-30-07_2013_cle8ea985.html)

#### Zones non-Agricoles

- 9_Bulletin_ZNA_2012_cle84b784 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q463/2012/9_Bulletin_ZNA_2012_cle84b784.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q463/2012/9_Bulletin_ZNA_2012_cle84b784.html)

### la Réunion

#### Canne à sucre

- BSV-CAS-oct-2012 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17070/2012/BSV-CAS-oct-2012.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17070/2012/BSV-CAS-oct-2012.html)

#### Cultures fruitières

- BSV-Fruits-decembre-2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17070/2015/BSV-Fruits-decembre-2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17070/2015/BSV-Fruits-decembre-2014.html)

#### Maraîchage

- BSV-maraichage-juillet-2014 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q17070/2014/BSV-maraichage-juillet-2014.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q17070/2014/BSV-maraichage-juillet-2014.html)

### Île-De-France

#### Arboriculture

- BSV_arboriculture_no09_du_14_avril_2011_cle87424a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2011/BSV_arboriculture_no09_du_14_avril_2011_cle87424a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2011/BSV_arboriculture_no09_du_14_avril_2011_cle87424a.html)
- BSV_arboriculture_no12_du_28_avril_2011_cle88e81d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2011/BSV_arboriculture_no12_du_28_avril_2011_cle88e81d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2011/BSV_arboriculture_no12_du_28_avril_2011_cle88e81d.html)
- pdf_BSV_arboriculture_numero_12_du_19_avril_2010_cle8f14ec [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2010/pdf_BSV_arboriculture_numero_12_du_19_avril_2010_cle8f14ec.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2010/pdf_BSV_arboriculture_numero_12_du_19_avril_2010_cle8f14ec.html)
- bsv_2014_arbo_39_cle4cea53 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2014/bsv_2014_arbo_39_cle4cea53.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2014/bsv_2014_arbo_39_cle4cea53.html)
- Note_technique_D_Suzukii_revision_2014_cle8a6134 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2014/Note_technique_D_Suzukii_revision_2014_cle8a6134.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2014/Note_technique_D_Suzukii_revision_2014_cle8a6134.html)
- pdf_BSV_arboriculture_numero_14_du_26_avril_2010_cle826524 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2010/pdf_BSV_arboriculture_numero_14_du_26_avril_2010_cle826524.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2010/pdf_BSV_arboriculture_numero_14_du_26_avril_2010_cle826524.html)
- pdf_BSV_arboriculture_numero_27_du_10_juin_2010_cle8fe4b7-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2010/pdf_BSV_arboriculture_numero_27_du_10_juin_2010_cle8fe4b7-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2010/pdf_BSV_arboriculture_numero_27_du_10_juin_2010_cle8fe4b7-1.html)

#### GrandesCultures - Pommes de Terre - Légumes industriels

- BSV_grandes_cultures_12-36_cle41c945 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2012/BSV_grandes_cultures_12-36_cle41c945.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2012/BSV_grandes_cultures_12-36_cle41c945.html)
- BSV_grandes_cultures_13-36_cle4e21eb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2013/BSV_grandes_cultures_13-36_cle4e21eb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2013/BSV_grandes_cultures_13-36_cle4e21eb.html)
- pdf_BSV_no14_du_27_mai_2010_cle813bfb-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2010/pdf_BSV_no14_du_27_mai_2010_cle813bfb-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2010/pdf_BSV_no14_du_27_mai_2010_cle813bfb-1.html)
- BSV_grandes_cultures_13-32_cle427651 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2013/BSV_grandes_cultures_13-32_cle427651.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2013/BSV_grandes_cultures_13-32_cle427651.html)
- Note_nationale_BSV_Hannetons_et_vers_blancs_cle8bf378-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14103/2013/Note_nationale_BSV_Hannetons_et_vers_blancs_cle8bf378-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14103/2013/Note_nationale_BSV_Hannetons_et_vers_blancs_cle8bf378-1.html)
- BSV_grandes_cultures_14-27_cle4a89da [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2014/BSV_grandes_cultures_14-27_cle4a89da.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2014/BSV_grandes_cultures_14-27_cle4a89da.html)
- BSV_grandes_cultures_12-30_cle4692e1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2012/BSV_grandes_cultures_12-30_cle4692e1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2012/BSV_grandes_cultures_12-30_cle4692e1.html)
- BSV_grandes_cultures_12-20_cle48bba7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2012/BSV_grandes_cultures_12-20_cle48bba7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2012/BSV_grandes_cultures_12-20_cle48bba7.html)

#### Maraîchage

- BSV_maraich_12-09_cle09e4cd [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2012/BSV_maraich_12-09_cle09e4cd.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2012/BSV_maraich_12-09_cle09e4cd.html)
- modification_BSV_maraichage_13-03_cle8eed1c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2013/modification_BSV_maraichage_13-03_cle8eed1c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2013/modification_BSV_maraichage_13-03_cle8eed1c.html)
- BSV_maraichage_14-04_cle058f22 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2014/BSV_maraichage_14-04_cle058f22.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2014/BSV_maraichage_14-04_cle058f22.html)
- BSV_maraichage_no06_du_29_juin_2011_cle013a3c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2011/BSV_maraichage_no06_du_29_juin_2011_cle013a3c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2011/BSV_maraichage_no06_du_29_juin_2011_cle013a3c.html)
- BSV_maraich_12-20_cle082e21 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2012/BSV_maraich_12-20_cle082e21.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2012/BSV_maraich_12-20_cle082e21.html)
- BSV_maraichage_no02_du_4_mai_2011_cle83f85e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2011/BSV_maraichage_no02_du_4_mai_2011_cle83f85e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2011/BSV_maraichage_no02_du_4_mai_2011_cle83f85e.html)
- BSV_maraichage_13-04_cle0b1abb [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2013/BSV_maraichage_13-04_cle0b1abb.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2013/BSV_maraichage_13-04_cle0b1abb.html)

#### Pépinières-Horticulture

- BSV_HP_13-02_cle04e1ee [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2013/BSV_HP_13-02_cle04e1ee.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2013/BSV_HP_13-02_cle04e1ee.html)

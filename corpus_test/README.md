# Constitution de corpus de test

Les corpus de test sont extraits de l'ensemble des BSV collectés
dans le cadre des projets VESPA et D2KAB, entre 2009 et aujourd'hui.
La collecte s'effectue sur l'ensemble des régions de france métropolitaine
ainsi que sur Mayotte et la Guadeloupe.

Les listes des BSV de ce dépôt contiennent les liens de téléchargement
qui permettent de les récupérer, que ce soit avec des outils dédiés
ou des scripts, comme par exemple :

```bash
cat CorpusMaraichage.md | \
  sed -e '/\[html\]/!d;s/^.*\[html\](\([^)]*\)).*$/\1/' | \
  xargs wget
```

**Remarques concernant le téléchargement des corpus :**

- l'ensemble des pdf de ces corpus de test représente environ 1,2Go
- les versions html de ces bulletins de santé du végétal peuvent varier
  au fur et à mesure de l'évolution de l'outil de conversion
  [pdf2blocks](https://gitlab.irstea.fr/copain/pdf2blocs).

### [Corpus de test VESPA](corpus_test_vespa/)

Corpus de test vespa: un sous ensemble du corpus vespa composé de 500 BSV
dont les index ont été construits manuellement. Ce corpus de test a été utilisé
pour vérifier la qualité de processus d'indexation automatique utilisé
pour indexer l'ensemble du corpus vespa.

Le développement de l'indexation dans le cadre du projet Vespa a donné lieu
à une extraction manuelle de date pour 500 bulletins de santé du végétal,
parmi les 19448 BSV collectés dans le cadre de ce projet.

Les régions et types de cultures ont été manuellement extraits lors des collectes de ces BSV.

*Vespa corpus gathers 500 PHBs collected in the whole French territory between 2009 and 2015 and manually indexed. A PHB is indexed by its crop categories and its location, that is to say, its French administrative unit. The publication dates were manually extracted.*

### [Corpus de test D2KAB](corpus_test_d2kab/)

Dans le cadre du projet D2KAB, un corpus de 230 BSV a été constitué manuellement.
Ce corpus visant à extraire des informations sur la base de reconnaissance de vocabulaire,
il porte sur des bulletins traitant de maraîchage, de viticulture et de grandes cultures uniquement.

Il ne contient que des BSV (il ne contient pas de notes nationales sur un ravageur,
une adventice, ...). De fait, tous les bulletins qu'il contient ont une date de publication précise.

*D2KAB corpus is composed of 230 PHBs collected in 2019. Those PHBs were manually selected to cover the whole French territory and to represent 3 crop categories: arable crops, vegetables and grapevines. Thus the crop categories and the French administrative unit are already identified. The publication date was automatically extracted.*

### [Corpus de test aléa](corpus_test_alea/)

Le corpus de test D2KAB étant relativement homogène, l'extension du processus d'indexation
à l'ensemble des BSV collectés automatiquement nécessite un corpus plus aléatoire.
150 autres BSV ont donc été tirés aléatoirement parmi l'ensemble des BSV collectés
jusqu'à ce jour (novembre 2020).

*Alea corpus is composed of 150 PHBs randomly selected from the whole corpus. That is to say the publication date may vary from 2009 to 2020. No information are extracted or provided from those PHBs.*

### [Corpus de test viti alsace](corpus_test_vitials/)

Corpus de test comprenant tous les BSV de viticulture en Alsace collectés
entre 2011 et 2022. Il représente un corpus homogène sur le type de culture
et la zone géographique sur une durée de 10 ans. Il contient 187 bulletins.

*Test corpus containing all PHBs about viticulture on Alsace territory*
*collected from 2011 to 2022. It is homogeneous on crop type and*
*geographic zone for a 10 years period. It contains 187 PHBs.*

### [Corpus de test des bilans de viticulture 2021](corpus_test_bviti21/)

Corpus des 18 BSV bilan pour la viticulture en 2021. Ce corpus de test
représente un unique type de culture sur une seule année pour l'ensemble
tu territoire concerné par la viticulture.

*Test corpus composed of the 18 end of year statements for viticulture*
*about the year 2021. It's about an only culture type during one year*
*for the whole french territory concerned by viticulture.*

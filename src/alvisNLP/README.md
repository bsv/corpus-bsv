# About the project

[**French Crop Usage**](https://gitlab.irstea.fr/copain/frenchcropusage/-/tree/master/) (FCU) is a thesaurus of crop organized by usage. The thesaurus was modelled using the SKOS vocabulary proposed by the W3C. Each concept is defined by a set of labels, notes and hierarchical links. 

[**BBCH-based Plant Phenological Description Ontology**](https://gitlab.irstea.fr/copain/d2kab/-/blob/testTermExtraction/resources/thesaurus/ppdo/ppdo_20210726.rdf) (PPDO) an ontology that extends skos model to store bbch phenological scales and other scales. PPDO ontology contains already two BBCH scales:
  - the BBCH general scale that could be used for any crop that do not have an BBCH individual scale.
  - the BBCH global scale, a new scalewhich contains all the phenological stages from the BBCH general scale, the BBCH inidvidual scale of grapevine and the BBCH individual scale of cereals.
More information about this ontology and associated datasets are available on http://ontology.irstea.fr/pmwiki.php/Site/Ppdo.

Currently, both benchmarks are used during the French ANR D2KAB project. The global purpose of the project is to :
* Create semantic annotations and index a new version of the corpus of *Plant Health Bulletins* [(this branch)](https://gitlab.irstea.fr/-/ide/project/copain/d2kab/edit/testTermExtraction/-/README.md)
* Enhance terminology coverage of semantic resources [(see)](https://gitlab.irstea.fr/copain/frenchcropusage/-/blob/enrichissementThesaurus/README.md)
* Modelize the results with a Web Annotation Data Model (W3C)

As mentionned before, this branch is dedicted to a semantic annotation of the bulletins. The workflow is created with [AlvisNLP engine](https://bibliome.github.io/alvisnlp/) (*Bibliome*) and includes several modules (files with *.plan* extension). Generally, it consists of projecting the terminological part of semantic resources onto the textual version of the bulletins. Please, check the installation requirements and necessary commands before running the code.


# Getting started

- **Prerequisites**

	- [AlvisNLP engine](https://github.com/Bibliome/alvisnlp)
	- [TreeTagger](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/) + [French.par](https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/french.par.gz)
	- [YaTeA](https://metacpan.org/dist/Lingua-YaTeA)
	- Python libraries :
		- [Stanza](https://stanfordnlp.github.io/stanza/installation_usage.html)


- **How to run the code**

	- Make sure to update the default parameters file of your AlvisNLP installation, you can find it in `folder-of-your-alvisnlp/share/default-param-values.xml`. In particular you should change:
		- Your local paths to Treetagger in module **TreeTagger**:

			```xml
			<treeTaggerExecutable>/Users/.../tree-tagger/bin/tree-tagger</treeTaggerExecutable>
			<parFile>/Users/.../tree-tagger/lib/french.par</parFile>
			```

		- Your local paths to Yatea in modules **YateaExtractor**, **TomapTrain**:

			```xml
			<yateaExecutable>/Users/.../YaTeA/local/bin/yatea</yateaExecutable>
			<perlLib>/Users/.../YaTeA/lib/perl5/site_perl</perlLib>
			<rcFile>/Users/.../YaTeA/etc/yatea/yatea.rc</rcFile>
			<configDir>/Users/.../YaTeA/usr/share/YaTeA/config</configDir>
			<localeDir>/Users/.../YaTeA/usr/share/YaTeA/locale</localeDir>
			```

		- Your local path to python in module **Stanza**:

			```xml
			<python>/usr/bin/python3</python>
			<alvisnlpPythonDirectory>folder-of-your-alvisnlp/share/alvisnlp-0.11.0-SNAPSHOT<alvisnlpPythonDirectory>
			```

	- The whole pipeline will use a config file which contains various information (which corpus should be processed, where to store the preprocessed dump, and various options for the processing). Update the exemple file `migale.config` as needed.

	- To run the whole pipeline use:

		```
		bash main.sh
		```

- **Automatic annotations**
	- Annotations are modelled in RDF and serialized in Turtle, they are exported as `output/annotations/automatic-annotations.ttl`
	- This is an example of the data structure for an Entity of type "Culture" (i.e a Crop). For more details see this [chowlk diagram](https://www6.inrae.fr/reseau-in-ovive/content/download/3560/34682/version/2/file/2023-10-11_semantic-linked-data.pdf#page=12).

		```ttl
		d2kab:abc8f445-0d12-4be9-820f-8d6a304e5929
					a               d2kab:Culture , prov:Entity , oa:Annotation , d2kab:AutomaticAnnotation ;
					oa:hasBody      <http://ontology.inrae.fr/frenchcropusage/Panais> ;
					oa:hasTarget    d2kab:abc8f445-0d12-4be9-820f-8d6a304e5929_rsel ;
					oa:motivatedBy  oa:identifying .

		d2kab:abc8f445-0d12-4be9-820f-8d6a304e5929_sel
						a          oa:TextQuoteSelector , oa:TextPositionSelector , oa:XPathSelector ;
						rdf:value  "//P[@num='17']" ;
						oa:end     "6486"^^xsd:int ;
						oa:exact   "panais" ;
						oa:prefix  "La pression mouche de la carotte reste très faible dans les parcelles de carotte et" ;
						oa:start   "6480"^^xsd:int ;
						oa:suffix  "suivies : 0,5 mouche/ plaque ont été capturée à Ste-Gemmes-sur-Loire (49) et Blaison-Gohier (49)." .

		d2kab:abc8f445-0d12-4be9-820f-8d6a304e5929_rsel
						a               oa:ResourceSelection ;
						oa:hasSelector  d2kab:abc8f445-0d12-4be9-820f-8d6a304e5929_sel ;
						oa:hasSource    <http://ontology.inrae.fr/bsv/resources/Q16994/2018/20181011_bsvmaraichage_27_cle09c363> .
		```
	- In the example above, the three subjects represent respectively the annotation, the target, and the selector. If the annotation is linked to a body using the `oa:hasBody` predicate, the body corresponds to the normalization of the Entity.
	- Annotations are checked using the [check-anno.plan](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/modules/check-anno.plan) module to see if they pass some verification checks. Error messages that appear during this phase should be monitored to see what problems were encountered.

## Method

In this section, we give an overview of the actions taken by `main.sh`. More specific comments are included in the script to detail each command.

* [**Load and preprocess bulletins**](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/modules/preprocess.plan)

	- (preliminary step) The Plant Health Bulletins have been transformed from their initial format (pdf) into a textual format (html) with [pdf2blocs](https://gitlab.irstea.fr/copain/pdf2blocs) in order to facilitate their textual preprocessing. They have also been divided into different corpora by : types of mentioned crops (vines, legumes, etc.), method of their selection (manual or random).
	- List of the various corpora (see [the README dedicated to corpora description](https://forgemia.inra.fr/bsv/corpus-bsv/-/tree/master/corpus_test)):
		- test D2KAB
		- test VESPA
		- test ALEA
		- test Viti Alsace
		- test Bilans Viti 2021

	NB : A bulletin can belong to more than one corpus.
	
	- The [preprocessing plan](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/modules/preprocess.plan) is called by `main.sh` using parameters which determine 1) which corpus should be preprocessed 2) after which module should the data structure be stored 3) where to store the dump.

	- Roughly, these are the actions performed by the plan:
		1) Read the html bulletins and transform them into AlvisNLP documents using the stylesheet [html2alvisnlp.xslt](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/resources/segmentation/html2alvisnlp.xslt).
		2) To each element in the html layer add the @xpath-short feature. The feature will be used to target the elements inside which entities appear.
		3) Add the bulletin metadata. This is done using the file `resources/corpus/bulletin-metadata.tsv` which contains information pulled from the BSV endpoint by the `utils/query-endpoints.py` script.
		4) Segmentation using Stanza
		5) Selecting badly segmented tokens and resegmenting them using WoSMig
		6) Adding part-of-speech (POS) tags with TreeTagger. This is necessary because YaTeA uses TreeTagger tags (and not Stanza's).
		7) Parsing using Stanza (also redoes the lemmatization and part-of-speech tagging based on the new segmentation). The syntactic structure (which can be accessed through `documents.sections.relations:dependencie.tuples`) is not currently used for the semantic annotation (but can be used for terminological extraction and to test patterns of extraction). It can be deactivated by changing the value of the parse parameter to false. The module in itself should not be deactivated as the lemmas and part-of-speech are used.
		8) Preparing the data for Conllu export (a form of tabular export)
		9) Dumping the data structure (this means that the preprocessing only has to be run once per corpus, even if the semantic annotation evolves).

	- At this point the data structure will contain three layers : html, sentences and words.

* [**Semantic annotation**](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/main-annotation.plan)

	* **RDF Projection**

		The first method is our baseline. It reads source SKOS terminologies or OWL ontologies and searches for class and concept labels in sections. The subject parameter specifies which text of the section should be matched. In our case, we match entry keys against word lemmas.

		As a result, the *projector* creates an annotation for each matched entry and adds these annotations to a target layer. The created annotations will have a feature containing the *URI* of the matched class or concept in the language we are interested in.

		- Ressources used:
			- Crops : [French Crop Usage](https://gitlab.irstea.fr/copain/frenchcropusage/-/tree/master/) for vernacular names and terms describing a crop's usage, and [Taxref](https://taxref.mnhn.fr/sparql) (for scientific names) such as *Vitis Vinifera*.
			- Living Organisms (Pests, Vectors and Pathogens) : We use two sources to create our entries: [a manually created lexicon](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/resources/pests/d2kab/Bioagresseurs_Vigne_2023-08-03.tsv) which we enrich with added information from [Taxref](https://taxref.mnhn.fr/sparql) (vernacular names, alternative names of the taxa...) and [NCBI](https://www.ncbi.nlm.nih.gov/taxonomy). Currently, this list is limited to bioagressors of Grapevine. The lexicon is processed and enriched using the script `resources/pests/d2kab/build_bioagressors_tables.py` (this is done automatically when running `main.sh`). Our second source of information is a pruned version of the NCBI obtained by selecting taxa prefixed by specific paths (i.e children of taxa identified as relevant).

				Example of the prefix path for insects, found in `resources/pests/ncbi/insects-root-paths.txt`:

				```
				/ncbi:1/ncbi:131567/ncbi:2759/ncbi:33154/ncbi:33208/ncbi:6072/ncbi:33213/ncbi:33317/ncbi:1206794/ncbi:88770/ncbi:6656/ncbi:197563/ncbi:197562/ncbi:6960
				```
			- Diseases : We use [a manually compiled lexicon](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/resources/pests/d2kab/maladiesVigne_17-10-2023.tsv).
			- Localities : We use [Wikidata](https://www.wikidata.org/) (specifically, the preferred labels of regions, departments and communes).
			- Cultivars : For Grapevine cultivars we use [Base de données des collections RFCV](https://bioweb.supagro.inra.fr/collections_vigne/Home.php) hosted by [VitiOeno](vitioeno.mistea.inrae.fr/).

		- Depending on the resource, the parameters for projection vary (projected on form vs lemma, lemmatizing the resource, allowing space and/or dashes, ignoring case...).
		- Some of the resources' entries introducing too much noise in the annotation (eg *BSV* in living organisms, *En* in localities). These entries are listed in blocklists that you can find in the same folder as the resource that they pertain to.


  * **Patterns**

		In addition to the previous method, we have defined patterns to capture entities not covered by the existing labels. This is the mecanism used to detect and normalize Dates (see [modules/patterns/dates.plan](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/modules/patterns/dates.plan)). The following pattern identifies mentions of week numbers.

		```xml
		<semaine class="PatternMatcher">
			<layer>words</layer>
			<pattern>
			[str:lower(@form)=="semaine"]
			(value:[@form=~"^\\d{1,2}$"])</pattern>
			<actions>
				<createAnnotation layer="week" features='ISO=("YYYY-W" ^ group:value)'/>
			</actions>
		</semaine>
		```

		NB : The form of the mention is not always sufficient to provid a full normalization of the date. In the example above, the year will be absent. A module will be added to predict these missing info based on the context of the bulletin.
		
		A sequence of patterns is used to identify days, months and years, and to combine them into full dates.


	* **ToMap Projection**

		The third method is called *ToMap* which stands for classifying terms among categories by comparing the syntactic structure of the term and labels of the categories. This task is useful to normalize named entities or extend existing terminologies. For instance ToMap is used in conjunction with a term extractor to detect named entities and attribute them to categories in a large ontology. Here's a short description of how it works :

		- **Exact match** : if the candidate term matches exactly a proxy term, then the candidate term is assigned the same identifier as the matched proxy term.

		- **Same head proxies** : the algorithm searches for proxy terms that share at least one significant head token with the candidate term. If no proxy term shares a significant head with the candidate significant heads, then the candidate is rejected. Candidate terms that have no significant head are thus rejected.

		- **Similarity** : a similarity is computed between the candidate term and each proxy term that shares a significant head token. The output of the algorithm is a set of attributions that contains the following information:
			- the proxy term that shares a significant head token;
			- the shared significant head token;
			- the category identifier associated with the proxy;
			- the token head of the candidate;
			- the similarity between the candidate term and the proxy term.    

		Prior to classifying, we need to make sure that we have run the [train-tomap.plan](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/train-tomap.plan) which analyzes the syntactic structure of the proxy terms (=labels of concepts) and stores them in `resources/tomap` as a file with *.tomap* extension. If you have run `main.sh``, this is done for you.


		Then, after all that, we can start annotating with *ToMap*. More particularly, we apply the module *TomapProjector* which searches for the terms specified by *yateaFile* (in YaTeA XML output format). It classifies the terms (generated by *train-tomap.plan*) with the help of the *ToMap classifier* and traverses the term syntactic tree from top to bottom and stops when it finds a head not in the [graylist](https://forgemia.inra.fr/bsv/corpus-bsv/-/blob/master/src/alvisNLP/resources/tomap/tomap-graylist.txt).

		Example for classifying yatea terms with the tomap classifier for Crops:

		```xml
		<classify class="TomapProjector">
			<subject layer="words" feature="lemma2"/>
			<tomapClassifier graylist="resources/tomap/tomap-graylist.txt" >resources/tomap/frenchCropUsage.tomap</tomapClassifier>
			<lemmaKeys/>
			<scoreFeature>similarity</scoreFeature>
			<conceptFeature>IRI</conceptFeature>
			<explanationFeaturePrefix>tomap-</explanationFeaturePrefix>
			<targetLayer>fcu-tomap</targetLayer>
		</classify>
		```
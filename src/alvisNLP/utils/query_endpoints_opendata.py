import requests
import json
import csv
import os
import pandas as pd

def transform_python_list_for_sparql(liste):
	str_list  = " ".join(["<"+x+">" for x in liste])
	return str_list

def write_query_results(endpoint, query, outfile, write_colnames=False):
	"""Sends query to endpoint and writes the output in a tsv file."""
	headers = {
			 'Accept':'application/json',
			#  'Accept-Encoding':"gzip",
			#  'User-agent': "mcourtin-bot",
			 }
	query = {'query': query}
	req = requests.get(endpoint,  params=query, headers=headers)
	print("code: ", req.status_code)
	if (req.status_code == requests.codes.ok):
		print("content-type: %s" % req.headers['content-type'])
		print("encoding: %s" % req.encoding)
		print("outfile: %s" % outfile)
		res = json.loads(req.text)['results']['bindings']
		with open(outfile, 'wt') as out_file:
			tsv_writer = csv.writer(out_file, delimiter='\t')
			if (len(res) > 0):
				ky = []
				for r in res:
					for k in r.keys():
						if k not in ky: ky.append(k)
				if write_colnames:
					tsv_writer.writerow(ky)
				for r in res:
					row = ['' if r.get(k) is None else r[k]['value']
							for k in ky]
					tsv_writer.writerow(row)
			out_file.close()
	return req.status_code

def get_query_results_as_json(endpoint, query):
	"""Sends query to endpoint and gets the output as json."""
	headers = {
			 'Accept':'application/json',
			 'Accept-Encoding':"gzip",
			 'User-agent': "mcourtin-bot",
			 }
	query = {'query': query}
	req = requests.get(endpoint,  params=query, headers=headers)
	print("code: ", req.status_code)
	if (req.status_code == requests.codes.ok):
		print("content-type: %s" % req.headers['content-type'])
		print("encoding: %s" % req.encoding)
		res = json.loads(req.text)['results']['bindings']
		return res
	return req.status_code



if __name__ == "__main__":
	fcu_endpoint = "https://rdf.codex.cati.inrae.fr/fcu-res/sparql"
	ppdo_endpoint = "https://rdf.codex.cati.inrae.fr/ppd-res/generic/sparql"
	bsv_endpoint = "http://rdf.codex.cati.inrae.fr/bsv-res/sparql"
	prefixes = """PREFIX fcu: <https://opendata.inrae.fr/fcu-res/>
PREFIX fcuo: <https://opendata.inrae.fr/fcu-def#>
PREFIX bsv: <https://opendata.inrae.fr/bsv-res/>
PREFIX d2kab: <https://opendata.inrae.fr/bsv-def#>
PREFIX ppd: <https://opendata.inrae.fr/ppd-res/generic#>
PREFIX ppdo: <https://opendata.inrae.fr/ppd-def#>
"""


	# print("Query on the bsv endpoint")
	query = prefixes+"""PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
	PREFIX dct: <http://purl.org/dc/terms/>
	PREFIX dctypes: <http://purl.org/dc/dcmitype/>
	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	PREFIX wikidata: <http://www.wikidata.org/entity/>
	PREFIX prov: <http://www.w3.org/ns/prov#>
	PREFIX oa: <http://www.w3.org/ns/oa#>

	SELECT * WHERE {
	?bulletin a d2kab:Bulletin ;
	dct:description ?description .
	?collection prov:hadMember ?bulletin .
	OPTIONAL {
	?bulletin dct:spatial ?region;
	dct:date ?date .
	}
	}
	"""
	write_query_results(bsv_endpoint, query, "../resources/corpus/bulletin-metadata_incomplete.tsv")
	os.system('cut -f 1 ../resources/corpus/bulletin-metadata_incomplete.tsv | grep -oP "[^/]+$" > tmp.tsv')
	os.system("paste tmp.tsv ../resources/corpus/bulletin-metadata_incomplete.tsv > ../resources/corpus/bulletin-metadata.tsv")
	os.system("rm tmp.tsv ../resources/corpus/bulletin-metadata_incomplete.tsv")
	

	# # extract tables with the following format (used to project, and for tomap)
	# # label | uri | prefLabel

	## FCU
	print("Query on fcu endpoint")
	query = prefixes+"""PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	SELECT DISTINCT ?label ?instance ?pref
	WHERE {
	?instance skos:inScheme fcuo:thesaurus_FrenchCropUsage;
	skos:prefLabel ?prefa .
	VALUES ?anylabel {skos:prefLabel skos:altLabel} 
	?instance ?anylabel ?labela .
	FILTER(LANG(?prefa)='fr')
	FILTER(LANG(?labela)='fr')
	BIND (STR(?prefa)  AS ?pref) 
	BIND (STR(?labela)  AS ?label) 
	}
	ORDER BY ?instance"""
	write_query_results(fcu_endpoint, query, "../resources/thesaurus/fcu/fcu.tsv")
	# making sure we don't keep the old lemmatized version, it should be recreated through the pipeline
	os.system("""rm ../resources/thesaurus/fcu/frenchCropUsage.lemma""")

	### Provenance info : FCU
	query = prefixes+"""PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX vann: <http://purl.org/vocab/vann/>
	SELECT ?version WHERE {
	[] a owl:Ontology; vann:preferredNamespaceUri <https://opendata.inrae.fr/fcu-res>;
        owl:versionInfo ?version }
	"""
	v = get_query_results_as_json(fcu_endpoint,query)
	metadata = pd.read_csv("../resources/metadata.tsv", sep="\t", index_col="resource")
	metadata.at["fcu", "version"] = v[0]['version']['value']
	metadata.to_csv("../resources/metadata.tsv", sep="\t")


	## PPDO 
	print("Query on PPDO endpoint")
	query = prefixes+"""PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	SELECT DISTINCT ?label ?stade ?pref WHERE {
	?concept a skos:ConceptScheme .
	?stade skos:inScheme ?concept .
	?stade skos:prefLabel ?prefa .
	VALUES ?anylabel {skos:prefLabel skos:altLabel} 
	?stade ?anylabel ?labela .
	FILTER(LANG(?prefa)='fr')
	FILTER(LANG(?labela)='fr')
	BIND (STR(?prefa)  AS ?pref) 
	BIND (STR(?labela)  AS ?label) 
	}
	ORDER BY ?stade"""
	write_query_results(ppdo_endpoint, query, "../resources/thesaurus/ppdo/ppdo.tsv")
	# making sure we don't keep the old lemmatized version, it should be recreated through the pipeline
	os.system("""rm ../resources/thesaurus/ppdo/ppdo.lemma""")

	### Provenance info : PPDO
        ## On a un petit problème : l'échelle générique n'est pas versionnée :-(
        ## Du coup la requête ne renvoie rien.
	query = prefixes+"""PREFIX owl: <http://www.w3.org/2002/07/owl#>
        select ?version WHERE {
        OPTIONAL { [] a owl:Ontology ; owl:versionInfo ?vers}
        BIND(COALESCE(?vers, "0.0") AS ?version) }"""
        
	v = get_query_results_as_json(ppdo_endpoint,query)
	metadata = pd.read_csv("../resources/metadata.tsv", sep="\t", index_col="resource")
	metadata.at["ppdo", "version"] = v[0]['version']['value']
	metadata.to_csv("../resources/metadata.tsv", sep="\t")


	## Wikidata
	q_lieux = """
	PREFIX wdt: <http://www.wikidata.org/prop/direct/>
	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	PREFIX wd: <http://www.wikidata.org/entity/>
	SELECT ?lieu ?type ?label
	WHERE { 
	VALUES ?type { wd:Q36784 wd:Q22670030 wd:Q6465 wd:Q484170 }	
	?lieu wdt:P31 ?type .
	?lieu skos:prefLabel ?label_lang .
	FILTER (LANG(?label_lang)='fr')
	BIND (STR(?label_lang)  AS ?label) 
	OPTIONAL {
		?lieu wdt:P576 ?abolishedIn_date .
		FILTER (year(?abolishedIn_date) > 2007)
	}
	}
	ORDER BY ?lieu"""
	d2kab_lieux_endpoint = "http://ontology.inrae.fr/wd/sparql"
	write_query_results(d2kab_lieux_endpoint, q_lieux, "../resources/locations/region-department-municipality.tsv")

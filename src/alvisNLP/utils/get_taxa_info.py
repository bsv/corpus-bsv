import pandas as pd
import re
import os
import query_endpoints_opendata
from stanzaPreprocess import stanza_lemmatize_labels

def taxa2entity_type(infile):
	"""
	Reads a tab-separated file with first column
	taxa, second column list of entity_types allowed
	returns the dict
	"""
	mapping_taxa = {}
	with open(infile) as inf:
		for line in inf:
			line = line.strip()
			taxa, ent_types = line.split("\t")
			ent_types = ent_types.split(", ")
			mapping_taxa[taxa] = ent_types
	return mapping_taxa

def make_full_name(sci, authority):
	if authority != "":
		return sci + " " + authority
	else:
		return sci

def add_new_label_if_new(old_label, sub_pattern):
	new = re.sub(sub_pattern, "", old_label)
	if new != old_label:
		return new
	else:
		return ""


if __name__ == "__main__":

	# 0. Extract info from input file
	mapping_taxa = taxa2entity_type("../resources/pests/taxa_entity_types.tsv")
	list_taxa_taxref = [uri for uri in mapping_taxa.keys() if "taxref" in uri]


	# 1. Query Taxref to get complementary info
	# info we're looking for : all the names (including scientific with authorities and vernacular )
	taxref_endpoint = "https://taxref.i3s.unice.fr/sparql"
	input_taxref_taxa = query_endpoints_opendata.transform_python_list_for_sparql(list_taxa_taxref)
	query = """
	PREFIX taxref: <http://taxref.mnhn.fr/lod/>
	PREFIX taxrefprop: <http://taxref.mnhn.fr/lod/property/>
	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	SELECT DISTINCT ?taxref_taxon ?taxref_name ?sci ?authority ?verna WHERE {
		VALUES ?taxref_taxon {"""+input_taxref_taxa+""" }
		?taxref_taxon taxrefprop:hasSynonym|taxrefprop:hasReferenceName ?taxref_name .
		?taxref_name dwc:scientificName ?sci .

		OPTIONAL {
			?taxref_taxon taxrefprop:vernacularName ?verna .
			FILTER (LANG(?verna)='fr')
		}
		
		OPTIONAL {
		?taxref_name taxrefprop:hasAuthority ?authority
		}
	}
	# """
	query_endpoints_opendata.write_query_results(taxref_endpoint, query, "../resources/pests/taxref_info.tsv", write_colnames=True)


	# 2. Transform Taxref info into easy format for alvisNLP projection.
	full_taxref = pd.read_csv("../resources/pests/taxref_info.tsv", sep="\t", keep_default_na=False)
	full_taxref.rename(columns={"taxref_taxon":"taxon", "taxref_name":"uri"},inplace=True)


	# 3. Add supplementary vernacular names (not in Taxref)
	manual_file = pd.read_csv("../resources/pests/manual.tsv", sep="\t", keep_default_na=False)
	manual_file["uri"] = manual_file.apply(lambda x:x.taxon.replace("/taxon/", "/name/"), axis=1)
	manual_grouped = manual_file.groupby("uri").agg({"verna_alt":lambda x: x.tolist()}).reset_index()
	# for now, only merging the names that come with a uri,
	# the rest will be merged later.
	manual_withuri = manual_grouped[manual_grouped["uri"]!= ""]
	# Merging the two tables, based on the uri column
	full_table = full_taxref.merge(manual_withuri, on="uri",how="outer")
	# Time to clean up some of this mess
	# we don't want to deal with columns with mixed types, casting everything as a str
	full_table = full_table.fillna('')
	full_table["taxon"] = full_table.apply(lambda x: x.uri if x.taxon == "" else x.taxon, axis=1)
	# removing redundant info so that when we melt the data we won't get duplicate lines
	full_table["is_ref"] = full_table.apply(lambda x: True if "NCBI" in x.uri else (True if x.uri.split("/")[-1]  == x.taxon.split("/")[-1] else False), axis=1)
	full_table["full_name"] = full_table.apply(lambda x: make_full_name(x.sci, x.authority) if "taxref" in x.uri else "", axis=1)
	# # there can be several vernacular names, joined with ", "
	full_table["verna"] = full_table.apply(lambda x: x.verna.split(", ") if x.is_ref else [""], axis=1)
	full_table["verna_alt"] = full_table.apply(lambda x: x.verna_alt if x.is_ref else "", axis=1)
	# giving each name it's own row
	full_table = full_table.explode('verna').explode('verna_alt')
	# cleaning up some of the labels (mostly about removing end of line determiners)
	full_table["verna"] = full_table["verna"].apply(lambda x: re.sub("(\([Ll]e\))|(\(La\))|(\(L'\))|(\[n\. f\.\])$", "", x).strip())
	# adding a shorter version on some of the labels
	full_table["verna_alt_2"] = full_table["verna"].apply(lambda x: add_new_label_if_new(x, " de la vigne$"))
	full_table["verna_alt_3"] = full_table["verna_alt"].apply(lambda x: add_new_label_if_new(x, " de la vigne$"))
	full_table.drop(columns=["authority"],inplace=True)
	full_table.rename(columns={"sci":"sci_name"},inplace=True)
	# melting everything so each nametype gets its own row
	full_table = full_table.melt(id_vars=["uri","taxon", "is_ref"], value_vars=["sci_name", "full_name", "verna", "verna_alt", "verna_alt_2", "verna_alt_3"], var_name="label_type", value_name="key")
	full_table = full_table[full_table["key"]!=""]
	full_table["label_type"] = full_table.apply(lambda x : "verna_alt" if x.label_type in ["verna_alt_2", "verna_alt_3"] else x.label_type, axis=1)
	full_table["entity_type"] = full_table.apply(lambda x: mapping_taxa.get(x.taxon, "Unknown"), axis=1)
	full_table = full_table.explode('entity_type')
	full_table = full_table.drop_duplicates(subset=["taxon", "uri", "entity_type", "key"])
	# finally, we add the labels that come without uris
	manual_withouturi = manual_file[manual_file["uri"] == ""]
	mapper = {"GLRaV4":["Pathogen"], "GLRaV2":["Pathogen"], "Cécidomyie des feuilles":["Pest"], "Cécidomyie de la feuille":["Pest"]}
	manual_withouturi = manual_withouturi.explode("verna_alt")
	manual_withouturi["entity_type"] = manual_withouturi.apply(lambda x:mapper.get(x.verna_alt, "Unknown"), axis=1)
	manual_withouturi = manual_withouturi.explode('entity_type')
	# adding the necessary columns to concat with the rest
	manual_withouturi[["is_ref", "label_type"]] = ["", "verna_alt"]
	manual_withouturi.rename(columns={"verna_alt":"key"},inplace=True)
	df = pd.concat([full_table, manual_withouturi])

	# creating a sorting column which is kinda unecessary
	# but will make it easier to compare between versions
	# sorting keys : source, taxon id, uri id (to order taxref names), label, entity_type
	df["sorting"] = df.apply(lambda x:[2 if x.uri == "" else (1 if "taxref" in x.uri else 0), 0 if x.uri == "" else int(x.taxon.split("/")[-1]), 0 if x.uri == "" else int(x.uri.split("/")[-1]), x.key, x.entity_type], axis=1)
	df = df.sort_values("sorting")
	df.drop("sorting", axis=1, inplace=True)
	df.to_csv("../resources/pests/grapevine-bioagressors.tsv", sep="\t", index=False)


	# 4. Making a lemmatized version for vernacular names (some of which are plural..)
	vernacular_names = df[df["label_type"].isin(["verna", "verna_alt"])]
	list_labels = [x.lower() for x in vernacular_names.key.tolist()]
	lemmatized = stanza_lemmatize_labels(list_labels, "fr", combinedMwt=3)
	vernacular_names["key"] = lemmatized
	vernacular_names.to_csv("../resources/pests/grapevine-bioagressors-lemmatized.tsv", sep="\t", index=False)


	# 5. Inject taxref metadata into our table for provenance tracking
	query = """select ?version WHERE {
		<http://taxref.mnhn.fr/lod/taxref-ld> owl:versionInfo ?version
	}
	"""
	v = query_endpoints_opendata.get_query_results_as_json(taxref_endpoint,query)
	metadata = pd.read_csv("../resources/metadata.tsv", sep="\t", index_col="resource")
	metadata.at["taxref", "version"] = v[0]['version']['value']
	metadata.to_csv("../resources/metadata.tsv", sep="\t")

	# remove tmp file
	os.remove("../resources/pests/taxref_info.tsv")
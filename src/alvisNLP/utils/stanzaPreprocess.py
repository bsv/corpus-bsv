import stanza

def stanza_lemmatize_labels(list_labels, lang, combinedMwt=1):
	"""
	lang:
		stanza will select a model appropriate for this language.
		available models here : https://stanfordnlp.github.io/stanza/available_models.html
	combinedMwt:
		1 : merge the lemmas of mwt e.g "au" is lemmatized as "à+le"
		2 : keep the lemmas as provided by stanza e.g "au" is lemmatized as ["à", "le"]
		3 : chose the first lemma (compatible with what alvisnlp does)  e.g "au" is lemmatized as "à"
	"""
	nlp = stanza.Pipeline(lang=lang, processors='tokenize,mwt,pos,lemma')
	in_docs = [stanza.Document([], text=d) for d in list_labels]
	out_docs = nlp(in_docs)
	lemmatized_labels = []
	for doc in out_docs:
		lem_label = ""
		if combinedMwt == 1:
			for token in doc.sentences[0].tokens:
				lem_label += "+".join([word.lemma for word in token.words])+" "
			lemmatized_labels.append(lem_label[:-1])
		elif combinedMwt==2:
			lem_label = " ".join([word.lemma for word in doc.sentences[0].words])
			lemmatized_labels.append(lem_label)
		elif combinedMwt==3:
			for token in doc.sentences[0].tokens:
				lem_label += token.words[0].lemma+" "
			lemmatized_labels.append(lem_label[:-1])
	return lemmatized_labels
#!/bin/env python3

import owlready2 as owl
import sys

####################################################################
# How to run : python3 rdf2tabular.py > {name_of_the_thesaurus}.txt
####################################################################
# Parse labels of concepts
####################################################################

onto = owl.get_ontology(sys.argv[1]).load() # load ontology 
                                            # make sure to be connected to internet as owlready does not work without it
for i in onto.individuals():
    prefs = i.prefLabel                     # get prefLabel
    if len(prefs) == 0:                     # this label refers to the highest concept : French Crop Usage
        sys.stderr.write('no prefLabel, ignoring %s\n' % i.iri) # print error
        continue
    pref = prefs.fr.first() # more reliable way to get a french prefLabel
    # pref = prefs[0]                         # get french  prefLabel
    print('%s\t%s\t%s' % (pref, i.iri, pref))   # print pref + iri + pref
    for alt in i.altLabel:                  # get all altlabels
        if alt.lang == "fr": # for altLabels we also want french
            print('%s\t%s\t%s' % (alt, i.iri, pref)) # print each alt label + its iri + its preflabel




<?xml version="1.0" encoding="UTF-8"?>
<alvisnlp-plan id="preprocess">
<!-- creates a dump with 
		- layers : html (with @xpath-short), words, sentences 
		- relations : dependencies
		- document metadatas are added -->



<!-- Which corpus is being preprocessed -->
	<param name="corpus">
		<alias module="load-corpus" param="source"/>
	</param>



	<!--
	///////////////////////////////////////////////////////////////////////////////////////
	//
	//  I. Load corpus
	//
	///////////////////////////////////////////////////////////////////////////////////////
	-->
	<!-- Load and segment html pages -->

	<load-corpus class="XMLReader">
		<html/>
		<xslTransform>resources/segmentation/html2alvisnlp.xslt</xslTransform>
	</load-corpus>

	<!-- Making the @xpath-short feature -->
	<make-xpath>

		<!-- How many html elements of the same type are there before in the whole doc  -->
		<!-- @num == 1, it's the first, @num == 2 it's the second... -->
		<num class="Action">
			<target>documents.sections.layer:html</target>
			<action>set:feat:num(int(int(target.before:html[@tag == target.@tag])+1))</action>
			<setFeatures/>
		</num>
		
		<position class="Action">
			<target>documents.sections.layer:html</target>
			<action>set:feat:xpath(
				str:join:'/'(
					nav:xoutside:html, @tag ^ "[" ^ (int(1 + before:html[@tag == target.@tag and outside:html == target.outside:html])) ^ "]"))</action>
			<setFeatures/>
		</position>
		
		<!-- @xpath-short: //P[@num=43'] for an entity can be accessed by outside:html{-1}.@xpath-short-->
		<xpath-selector class="Action">
			<target>documents.sections.layer:html</target>
			<action>set:feat:xpath-short("//" ^ str:seds(str:basename(target.@xpath), "\\[\\d+\\]", "") ^ "[@num='" ^ target.@num ^ "']")|set:remove-feature:xpath</action>
			<setFeatures/>
		</xpath-selector>

	</make-xpath>
	
	<!-- Adds a lot of documents features, the most important one is @uri which points to the specific PHB resource -->
	<meta-key class="Action">
	  <target>documents</target>
	  <action>set:feat:meta-key(str:seds(@path, "^.*/([^/]+)/(\\d+)/([^/]+).html$", "bulletin_$1_$2_$3"))</action>
	  <setFeatures/>
	</meta-key>
	
	<bulletin-meta class="FileMapper">
		<target>documents</target>
	        <form>@meta-key</form>
		<mappingFile>resources/corpus/bulletin-metadata.tsv</mappingFile>
		<targetFeatures>,uri,description,corpus,covers-region,dated</targetFeatures>
	</bulletin-meta>

	


	<!--
	///////////////////////////////////////////////////////////////////////////////////////
	//
	//  II. Tokenisation (in several steps)
	//
	///////////////////////////////////////////////////////////////////////////////////////
	-->

	<!-- First using Stanza (french model)  -->
	<stanza-segment class="Stanza">
		<language>fr</language>
		<parse>false</parse>
		<ner>false</ner>
	</stanza-segment>

	<!-- Many things are still wrong, (spaces and puncts in tokens mostly) -->
	<post-segmentation>
		<url class="Action">
			<target>documents.sections.layer:words[@form =~ "www\\." or @form =~ "https?:"]</target>
			<action>set:feat:fixed("url")</action>
			<setFeatures/>
		</url>

		<email class="Action">
			<target>documents.sections.layer:words[@form =~ "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$"]</target>
			<action>set:feat:fixed("mail")</action>
			<setFeatures/>
		</email>

		<!-- This is the stuff we want to resegment -->
		<select-weird-tokens class="Action">
			<target>documents.sections.layer:words[not @fixed and (@form=~ "[\\s+\\(\\)\\[\\]-]" or @form ?= "." or @form ?= "/") ]</target>
			<action>add:resegment|remove:words</action>
			<addToLayer/>
			<removeFromLayer/>
		</select-weird-tokens>

		<reseg class="WoSMig">
			<targetLayer>new-words</targetLayer>
			<fixedFormLayer>words</fixedFormLayer>
			<punctuations>?.!;,:-+/</punctuations>
		</reseg>

		<!-- I tested wether it changes something if I keep the the features before parsing,
		and it does seems like stanza takes the old version into account.
		This is is not ideal since we changed some stuff in between that could impact the feature
		Hence their removal -->
		<clean-old-features class="Action">
			<target>documents.sections.layer:words</target>
			<action>set:remove-feature:lemma|set:remove-feature:upos|set:remove-feature:wordType|set:remove-feature:stanza-id</action>
			<setFeatures/>
		</clean-old-features>

		<merge class="Action">
			<target>documents.sections.layer:new-words</target>
			<action>add:words|remove:new-words</action>
			<addToLayer/>
			<removeFromLayer/>
		</merge>
	</post-segmentation>

	<!-- YaTeA needs a TreeTagger input so we create it in new features to not impact those made by stanza -->
	<tt class="TreeTagger">
		<lemmaFeature>lemma2</lemmaFeature>
		<posFeature>pos2</posFeature>
		<noUnknownLemma/>
		<inputCharset>UTF-8</inputCharset>
		<outputCharset>UTF-8</outputCharset>
	</tt>

	<!-- Now we can do the parsing
	@stephan : if this takes too long you can change parse to false,
	I'm using the structures to explore the corpus but they're not used
	for the annotation yet -->
	<parse class="Stanza">
		<language>fr</language>
		<pretokenized>true</pretokenized>
		<parse>true</parse>
		<ner>false</ner>
	</parse>


	<!-- Stanza will do weird stuff with amalgams (e.g decomposing du into de+le) which causes a ton of issue
		like having a token be a dependent in two tuples, having two upos...
		we check that there are no amalgams -->
	<check-for-amalgam-weirdness>

		<!-- Inside each document, a word can only be a dependent argument for one tuple -->
		<in-deps class="Assert">
			<target>documents</target>
			<assertion>int($.sections.relations:dependencies.tuples.args:dependent) == int(sort:nsval($.sections.relations:dependencies.tuples.args:dependent, @form ^ "-" ^ start ^ "-" ^ end))</assertion>
			<message>"Found a MWT (via deps):" ^ " in doc " ^ document.@id</message>
		</in-deps>
	</check-for-amalgam-weirdness>

	<!-- stanza-ids are a bit weird, so we create a clean one -->
	<word-id class="Action">
		<target>documents.sections.layer:sentences</target>
		<action>
		id:enumerate:word-index(inside:words)
		</action>
		<setFeatures/>
	</word-id>

	<add-metadata-for-conllu-export>
		<!-- different for all sentences within a document -->
		<sentence-index class="Action">
			<target>documents.sections</target>
			<action>id:enumerate:sentence-index(layer:sentences)</action>
			<setFeatures/>
		</sentence-index>
	</add-metadata-for-conllu-export>

	<!-- we're not doing the conll export just yet because we want to include the entities inside
	so it has to be after projection -->


</alvisnlp-plan>

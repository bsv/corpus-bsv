<?xml version="1.0" encoding="UTF-8"?>
<alvisnlp-plan id="dates">

	<day>
		<find class="RegExp">
			<pattern>(1|2)[0-9]|3[01]|0?[1-9]</pattern>
			<targetLayer>day</targetLayer>
		</find>

		<day-day>
			<!-- the following 2 rules are necessary because of the poor tokenization... -->
			<find class="PatternMatcher">
				<layer>words</layer>
				<pattern>
				(days:[@form=~"^\\d{1,2}-\\d{1,2}$"])
				</pattern>
				<actions>
					<createAnnotation layer="day-day"/>
				</actions>
			</find>

			<split class="Action">
				<target>documents.sections.layer:day-day</target>
				<action>
				section.new:annotation:day(target.start, target.start+str:index(target, "-"))|
				section.new:annotation:day(target.start+str:index(target, "-")+1,target.end)
				</action>
				<createAnnotations/>
				<addToLayer/>
				<deleteElements/>
			</split>
		</day-day>


		<normalize class="Action">
			<target>documents.sections.layer:day</target>
			<action>if str:len(target.@form)== 1
				then set:feat:day("0"^target.@form)
				else set:feat:day(target.@form)
			</action>
			<setFeatures/>
		</normalize>
	</day>

	<month>
		<find-lexical class="TabularProjector">
			<dictFile>resources/others/months.txt</dictFile>
			<subject layer="words" feature="form"/>
			<valueFeatures>,month</valueFeatures>
			<allUpperCaseInsensitive>true</allUpperCaseInsensitive>
			<caseInsensitive>true</caseInsensitive>
			<ignoreDiacritics/>
			<skipWhitespace/>
			<targetLayer>month</targetLayer>
			<constantAnnotationFeatures>lexical=yes</constantAnnotationFeatures>
		</find-lexical>

		<iso-for-lexical class="Action">
			<target>documents.sections.layer:month[@lexical]</target>
			<action>
			set:feat:ISO("YYYY-" ^ target.@month)
			</action>
			<setFeatures/>
		</iso-for-lexical>

		<find-num class="RegExp">
			<pattern>(0[1-9]|1[12])</pattern>
			<targetLayer>month</targetLayer>
			<constantAnnotationFeatures>numerical=yes</constantAnnotationFeatures>
		</find-num>

		<normalize-num class="Action">
			<target>documents.sections.layer:month[@numerical=="yes"]</target>
			<action>
			set:feat:month(target.@form)</action>
			<setFeatures/>
		</normalize-num>
	</month>

	<year>
		<!-- will match either 19 or 2019 -->
		<find class="RegExp">
			<pattern>(19|20)?[0-9]{2}</pattern>
			<targetLayer>year</targetLayer>
		</find>
		<normalize class="Action">
			<target>documents.sections.layer:year</target>
			<action>if str:len(target.@form)== 2
				then set:feat:year("20"^target.@form)|set:feat:full-year("no")
				else set:feat:year(target.@form)|set:feat:full-year("yes")
			</action>
			<setFeatures/>
		</normalize>

		<iso-for-full-year class="Action">
			<target>documents.sections.layer:year[str:len(@form)==4]</target>
			<action>set:feat:ISO(target.@year)</action>
			<setFeatures/>
		</iso-for-full-year>
	</year>

	<!-- semaine 20 -->
	<semaine class="PatternMatcher">
		<layer>words</layer>
		<pattern>
		[str:lower(@form)=="semaine"]
		(value:[@form=~"^\\d{1,2}$"])</pattern>
		<actions>
			<createAnnotation layer="week" features='ISO=("YYYY-W" ^ group:value)'/>
		</actions>
	</semaine>

	<!-- 21 avril ou 21/04 -->
	<day-month class="PatternMatcher">
		<layer>words</layer>
		<pattern>
		(day:[span:day])
		[@form=="/" or @form=="-"]?
		(month:[span:month])
		</pattern>
		<actions>
			<createAnnotation layer="compound-date" features='
			day=(group:day.span:day.@day),
			month=(group:month.span:month.@month),
			ISO=("YYYY-" ^ group:month.span:month.@month ^ "-" ^ group:day.span:day.@day)'/>
		</actions>
	</day-month>

	<!-- avril 2012 ou 04/2012 -->
	<month-year class="PatternMatcher">
		<layer>words</layer>
		<pattern>
		(month:[span:month])
		[@form=="/" or @form=="-"]?
		(year:[span:year])</pattern>
		<actions>
			<createAnnotation layer="compound-date" features='
			month=(group:month.span:month.@month),
			year=(group:year.span:year.@year),
			full-year=(group:year.span:year.@full-year),
			ISO=(group:year.span:year.@year ^ "-" ^ group:month.span:month.@month)'/>
		</actions>
	</month-year>


	<day-month-year class="PatternMatcher">
		<layer>words</layer>
		<pattern>
		(day:[span:day])
		[@form=="/" or @form=="-"]?
		(month:[span:month])
		[@form=="/" or @form=="-"]?
		(year:[span:year])</pattern>
		<actions>
			<createAnnotation layer="compound-date" features='
			day=(group:day.span:day.@day),
			month=(group:month.span:month.@month),
			year=(group:year.span:year.@year),
			year=(group:year.span:year.@full-year),
			ISO=(group:year.span:year.@year ^ "-" ^ group:month.span:month.@month ^ "-" ^ group:day.span:day.@day)'/>
		</actions>
	</day-month-year>

	<removeoverlaps class="RemoveOverlaps">
		<layer>compound-date</layer>
		<removeEqual>false</removeEqual>
		<removeIncluded>true</removeIncluded>
	</removeoverlaps>


	<timespans>


		<!-- 10-15 mai -->
		<timespan-1 class="PatternMatcher">
			<layer>words</layer>
			<pattern>
			(days:[span:day-day])
			(month:[span:month[@lexical]])
			</pattern>
			<actions>
				<createAnnotation layer="timespan" features='ISO=("YYYY-" ^ group:month.span:month.@month ^ "-" ^ group:days.inside:day{0}.@day ^ "/" ^ 
				"YYYY-" ^ group:month.span:month.@month ^ "-" ^ group:days.inside:day{-1}.@day)'/>
			</actions>
		</timespan-1>

	<!-- du 28 juin au 24 juillet 2014
		entre les 11 et 13 mars
		entre le 3 et le 12 avril -->
		<timespan-2 class="PatternMatcher">
			<layer>words</layer>
			<pattern>
			([str:lower(@form)=="du"]|[str:lower(@form)=="entre"][@lemma=="le"])
			([outside:compound-date]+|[span:day])
			([str:lower(@form)=="au"]|[str:lower(@form)=="et"][str:lower(@form)=="le"]?)
			([outside:compound-date]+|[span:day])
			</pattern>
			<actions>
				<createAnnotation layer="timespan"/>
			</actions>
		</timespan-2>

		<year-year class="PatternMatcher">
			<layer>words</layer>
			<pattern>
			(first:[span:year[str:len(@form)==4]])
			[@form == "-"]
			(second:[span:year[str:len(@form)==4]])
			</pattern>
			<actions>
				<createAnnotation layer="timespan" features='ISO=(group:first ^ "/" ^ group:second)'/>
			</actions>
		</year-year>

		<year-year-glued class="PatternMatcher">
			<layer>words</layer>
			<pattern>
			(merged:[@form=~"^\\d{4}-\\d{4}"])</pattern>
			<actions>
				<createAnnotation layer="timespan" features='ISO=((group:merged =~ "^....") ^ "/" ^ (group:merged =~ "....$"))'/>
			</actions>
		</year-year-glued>

	</timespans>

	<!-- we have to do this before removing the stuff inside the compounds (which might be good) -->
	<rm-weird-compounds>

		<!-- to avoid stuff like JUIN\n25 (however we wanna keep the JUIN inside)-->
		<space-inside class="Action">
			<target>documents.sections.layer:compound-date[@form =~ "[\\n\\t]"]</target>
			<action>remove:compound-date|add:test</action>
			<removeFromLayer/>
			<addToLayer/>
		</space-inside>

		<slash-and-space class="Action">
			<target>documents.sections.layer:compound-date[@form =~ "/" and @form =~ " "]</target>
			<action>remove:compound-date|add:test</action>
			<removeFromLayer/>
			<addToLayer/>
		</slash-and-space>

	</rm-weird-compounds>


	<cleanup class="Action">
		<target>documents.sections.(layer:compound-date.(inside:day|inside:month|inside:year)|layer:timespan[@ISO].(inside:day|inside:month|inside:year|inside:compound-date))</target>
		<action>
		delete
		</action>
		<deleteElements/>
	</cleanup>



	<normalization class="Action">
		<target>documents.sections.layer:timespan[not @ISO]</target>
		<action>
		set:feat:ISO(str:join:'/'(
			sort:ival($.(inside:compound-date|inside:day), int(start)),
				(if @year then @year else "YYYY") ^ "-" ^
				(if @month then @month else "MM") ^ "-" ^
				(if @day then @day else "DD")))|
		$.(inside:compound-date|inside:day).delete
		</action>
		<setFeatures/>
		<deleteElements/>
	</normalization>


	<!-- heuristic 04/08 = YYYY-08-04 rather than 2008-04 -->
	<competing-compounds class="Action">
		<target>documents.sections.layer:compound-date[@madeByModule =~ "\\.month-year" and @full-year == "no" and span:compound-date[@madeByModule =~ "\\.day-month"]]</target>
		<action>remove:compound-date|add:test</action>
		<removeFromLayer/>
		<addToLayer/>
	</competing-compounds>

	<!-- du 12 au 15 mai -->
	<!-- if the first month isn't mentionned we're pretty confident that the months are the same -->
	<heuristic-normalisation class="Action">
		<target>documents.sections.layer:timespan[@ISO =~ "^YYYY-MM-\\d{2}/YYYY-\\d{2}-\\d{2}$"]</target>
		<action>
			set:feat:ISO(str:seds(@ISO, "MM", str:regrp(@ISO, "YYYY-MM-\\d{2}/YYYY-(\\d{2})-\\d{2}", 1)))
		</action>
		<setFeatures/>
	</heuristic-normalisation>

	<date class="Action">
		<target>documents.sections.(layer:timespan|layer:compound-date|layer:month[@lexical]|layer:year[str:len(@form)==4]|layer:week)</target>
		<action>add:date</action>
		<addToLayer/>
	</date>

	<!-- whenever the info is available, and for incomplete dates, we get the year of the bulletin publication -->
	<heuristic-normalisation-based-on-doc-year class="Action">
		<target>documents[@dated =~ "^\\d{4}"].sections.layer:date[@ISO =~ "^YYYY"]</target>
		<action>
			set:feat:ISO(str:seds(@ISO, "YYYY", str:sub(document.@dated, 0,4)))
		</action>
		<setFeatures/>
	</heuristic-normalisation-based-on-doc-year>

	<!-- same but for months -->
	<heuristic-normalisation-based-on-doc-month class="Action">
		<target>documents[@dated =~ "^\\d{4}-\\d{2}"].sections.layer:date[@ISO =~ "-MM"]</target>
		<action>
			set:feat:ISO(str:seds(@ISO, "MM", str:sub(document.@dated, 5,7)))
		</action>
		<setFeatures/>
	</heuristic-normalisation-based-on-doc-month>



	<export class="TabularExport">
		<outDir>output/annotations</outDir>
		<corpusFile>extracted_dates.tsv</corpusFile>
		<lines>documents.sections.layer:date</lines>
		<columns separator=";">
		section.document.@path;
		@form ^ "\t";
		@ISO ^ "\t";
		str:normalizeSpace(str:seds(outside:sentences, "\"", "'"))
		</columns>
	</export>


</alvisnlp-plan>
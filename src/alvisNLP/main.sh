#!/bin/bash

## for conda users
# source /usr/local/genome/Anaconda3/etc/profile.d/conda.sh
# conda activate alvis-py

# Unzip the corpus
cd resources/corpus
tar -xf Corpus_test.html.tar.xz
cd ../..


# 0. Create the folder structure

## to store the thesaurus
mkdir -p resources/thesaurus/fcu
mkdir -p resources/thesaurus/ppdo
## to store preprocessed corpora
mkdir -p output/dumps

## Parameters that might change depending on the run are sourced
source migale.config
## Fixed parameters
dumpFolder="output/dumps"
annotationFolder="output/annotations"


# 1. Download the latest version of FCU and PPDO
cd utils
python3 query_endpoints_opendata.py
## Paths to the resources created
fcu="resources/thesaurus/fcu/fcu.tsv"
ppdo="resources/thesaurus/ppdo/ppdo.tsv"

#### For tomap
tomapOutDir="resources/tomap"
fcuTermsFile="$(basename "$fcu" .rdf)".txt
ppdoTermsFile="$(basename "$ppdo" .rdf)".txt
parseSuffix=".tomap"
lemmaSuffix=".lemma"

cd ..

# 2. Run train-tomap.plan on both resources (will also export the lemmatized thesaurus)

## 2.1 ON FCU

trainTomapFcu="-alias terms-file $fcu \
-alias terms-file-lemmatized fcu/frenchCropUsage.lemma \
-alias yatea-export output/yatea/fcu-candidates.xml \
-alias terms-parsed resources/tomap/frenchCropUsage.tomap"
alvisnlp $trainTomapFcu train-tomap.plan 

# ## On PPDO

trainTomapPpdo="-alias terms-file $ppdo \
-alias terms-file-lemmatized ppdo/ppdo.lemma \
-alias yatea-export output/yatea/ppdo-candidates.xml \
-alias terms-parsed resources/tomap/ppdo.tomap"
alvisnlp $trainTomapPpdo train-tomap.plan


# # 3. Preprocess the corpus and store the output in a dump
# Will tokenize, lemmatize, parse ... once and for all since it takes a while each time
alvisnlp -alias corpus $corpus -dumpModule add-metadata-for-conllu-export $dumpFolder/$dumpName modules/preprocess.plan


# # 4. Extracting the terms from the corpus using YaTeA
opt="-resume $dumpFolder/$dumpName -alias corpus-terms $tomapOutDir/$corpusTermsFile"
alvisnlp $opt modules/config-tomap.plan

# 5. Build the resources for bioagressors
# Create the disease and pests/vector tables based on the manually edited files
cd resources/disease
jupyter execute extract-disease.ipynb
cd ../../utils
python3 get_taxa_info.py
cd ../resources/pests/ncbi
tar -zxvf taxa+id_full.tar.gz
# Create the trie version of the NCBI taxonomy
alvisnlp compile-taxa.plan
cd ../../..

# 6. Finally, running the annotation
opt="-resume $dumpFolder/$dumpName \
-alias run-tomap $runTomap \
-alias fcu $fcu \
-alias ppdo $ppdo"


## provenance info
plan_commit=$(git rev-parse HEAD)
alvis_commit=$(alvisnlp -version | grep "Commit" | cut -d ' ' -f 2)
alvis_tag=$(alvisnlp -version | grep "tag" | cut -d ' ' -f 2)
now=$(date '+%Y-%m-%d_%H:%M:%S')

opt="$opt -feat plan_commit $plan_commit -feat alvis_commit $alvis_commit -feat now $now -feat alvis_tag $alvis_tag"

## resources : version info
taxref_v=$(grep "taxref" resources/metadata.tsv | cut -f 2)
fcu_v=$(grep "fcu" resources/metadata.tsv | cut -f 2)
ppdo_v=$(grep "ppdo" resources/metadata.tsv | cut -f 2)

opt="$opt -feat taxref_v $taxref_v -feat fcu_v $fcu_v -feat ppdo_v $ppdo_v"

# Test wether quick html should be run
if [[ -v htmlFolder ]];
	then
	opt="$opt -alias make-html-viz true -alias html-folder $htmlFolder"
	else
	opt="$opt -alias make-html-viz false -alias html-folder None"
fi

# Create an analysisFile
## It's a plan that lists the modules, imports, exports.. useful to debug or get an overview of what the plan does
if [[ -v analysisFile ]];
	then
		opt="$opt -analysisFile $analysisFile"
fi

## Useful for debugging
opt="$opt -creator madeByModule"

## Reading output path
opt="$opt -alias annotations-folder $annotationFolder"

## Verbose
if [[ $verbose = true ]];
	then opt="$opt -verbose"
fi

## Interactive mode
if [[ $shell = true ]];
	then opt="$opt -shell"
fi

alvisnlp $opt main-annotation.plan
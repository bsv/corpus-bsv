# À partir du (ou des) fichier(s) pdf de BSV(s)
# crée le code turtle-rdf qui correspond, à destination du projet D2KAB.
# Ce code est envoyé sur la sortie standard.
# Les préfixes ne sont pas générés
# (ils doivent être dans ttl/prefixes.ttl)
#
# Initialement, la tâche était réalisée par deux scripts python,
# l'un dédié aux BSV du projet Vespa, l'autre pour les BSV collectés
# automatiquement. Celui-ci recherche le BSV parmi ceux du projet VESPA
# (qui a le plus d'informations) et s'il l'y trouve pas
# il le cherche parmi les BSV collectés.

import sys
import io
import os
import re

import requests
import json
import csv
from datetime import datetime, date, timedelta, timezone
from config import *
from etl_functions import *



# +--------------------------------------------------------------+
# |                          CONFIG                              |
# +--------------------------------------------------------------+

# Si not None, alors un fichier csv est chargé pour y chercher des dates
# de publication extraites manuellement.
CSV_DATE_FILE = None
CSV_DATE_DELIMITER = ','
CSV_DATE_COLUMNS = ['bsv', 'date'] # Other than 'date' and 'bsv' will be ignored
CSV_GET_BSV = lambda s : os.path.splitext(os.path.basename(s))[0]
CSV_GET_XSD_DATE = lambda s : '-'.join([s.split('/')[x] for x in [2,1,0]])


# +--------------------------------------------------------------+
# |                      Initialisations                         |
# +--------------------------------------------------------------+
IS_TEST_CORPUS = False
DEBUG = False # Ne pas mettre true, mais utiliser la ligne de commande
TESTCORPUS_NAME = ''
DEST_PATH = None
ex = EX_PREFIX
region_from_url = re.compile(r'https?://[^\.]+\.([^\.]+)\.agriculture')
dates_collectes = [] # Pour identifier l'Activity de collecte

# +--------------------------------------------------------------+
# |                        CONSTANTES                            |
# +--------------------------------------------------------------+
TEST_CORPUS_NAMES = ["Vespa", "Alea", "D2KAB", "BilansViti21", "VitiAls"]

REGIONS = {
    'auvergne-rhone-alpes' : 'Q18338206',
    'bourgogne-franche-comte' : 'Q18578267',
    'bretagne' : 'Q12130',
    'centre-val-de-loire' : 'Q13947',
    'corse' : 'Q14112',
    'grand-est' : 'Q18677983',
    'hauts-de-france' : 'Q18677767',
    'ile-de-france' : 'Q13917',
    'normandie' : 'Q18677875',
    'nouvelle-aquitaine' : 'Q18678082',
    'occitanie' : 'Q18678265',
    'paca' : 'Q15104',
    'pays-de-la-loire' : 'Q16994',
    'guadeloupe' : 'Q17012',
    'mayotte' : 'Q17063',
}

OLD_REGIONS = {
  '11':{'nom': 'Île-de-France', 'wd': 'Q13917'} ,
  '21':{'nom': 'Champagne-Ardenne', 'wd': 'Q14103'} ,
  '22':{'nom': 'Picardie', 'wd': 'Q13950'} ,
  '23':{'nom': 'Haute-Normandie', 'wd': 'Q16961'} ,
  '24':{'nom': 'Centre', 'wd': 'Q13947'} ,
  '25':{'nom': 'Basse-Normandie', 'wd': 'Q16954'} ,
  '26':{'nom': 'Bourgogne', 'wd': 'Q1173'} ,
  '31':{'nom': 'Nord-Pas-de-Calais', 'wd': 'Q16987'} ,
  '41':{'nom': 'Lorraine', 'wd': 'Q1137'} ,
  '42':{'nom': 'Alsace', 'wd': 'Q1142'} ,
  '43':{'nom': 'Franche-Comté', 'wd': 'Q16394'} ,
  '52':{'nom': 'Pays de la Loire', 'wd': 'Q16994'} ,
  '53':{'nom': 'Bretagne', 'wd': 'Q12130'} ,
  '54':{'nom': 'Poitou-Charentes', 'wd': 'Q17009'} ,
  '72':{'nom': 'Aquitaine', 'wd': 'Q1179'} ,
  '73':{'nom': 'Midi-Pyrénées', 'wd': 'Q16393'} ,
  '74':{'nom': 'Limousin', 'wd': 'Q1190'} ,
  '82':{'nom': 'Rhône-Alpes', 'wd': 'Q463'} ,
  '83':{'nom': 'Auvergne', 'wd': 'Q1152'} ,
  '91':{'nom': 'Languedoc-Roussillon', 'wd': 'Q17005'} ,
  '93':{'nom': 'Provence-Alpes-Côte d\'Azur', 'wd': 'Q15104'} ,
  '94':{'nom': 'Corse', 'wd': 'Q14112'} ,
  '01':{'nom': 'Guadeloupe', 'wd': 'Q17012'} ,
  '02':{'nom': 'Martinique', 'wd': 'Q17054'} ,
  '03':{'nom': 'Guyane', 'wd': 'Q3769'} ,
  '04':{'nom': 'La Réunion', 'wd': 'Q17070'} ,
  '06':{'nom': 'Mayotte', 'wd': 'Q17063'} ,
}

# +--------------------------------------------------------------+
# |                        Fonctions                             |
# +--------------------------------------------------------------+

############################ VESPA ###############################
def try_vespa(line, dates_manu):
  bsv_name = os.path.splitext(os.path.basename(line))[0]
  sel_bsv = sparql_query(VESPA_ENDPOINT+'/query',
"""SELECT ?bsv ?reg ?dt ?dq ?desc
WHERE {
  ?bsv a <http://ontology.irstea.fr/bsv/ontology/Bulletin> ;
    <http://purl.org/dc/terms/spatial> ?reg ;
    <http://purl.org/dc/terms/date> ?dt ;
    <http://ontology.irstea.fr/bsv/ontology/dateExtractionQuality> ?dq ;
    <http://purl.org/dc/terms/description> ?desc
  BIND (str(?bsv) AS ?b) FILTER (STRENDS(?b, "%s"))
}""" % bsv_name)
  
  if (sel_bsv is None) or (len(sel_bsv) == 0):
    if DEBUG:
      print("[DEBUG] %s non trouvé dans Vespa" % bsv_name)
    return None

  rb = sel_bsv[0]
  date = rb['dt']['value'].split('T')[0]
  dateq = rb['dq']['value']
  desc = re.sub(r' +', ' ', rb['desc']['value'].replace('\n', ' ')).strip()
  if re.match('.*[?.!]$', desc): # Remove final ponctuation.
    desc = desc[:-1]
  region = OLD_REGIONS[rb['reg']['value'].split('/')[-1]]['wd']
  manu = False
  
  if (dates_manu is not None) and (dates_manu.get(bsv_name) is not None):
    dt = None
    try:
      dt = CSV_GET_XSD_DATE(dates_manu[bsv_name])
    except Exception as e:
      pass
    if dt is not None:
      date = dt
      dateq = '100'
      manu = True
  
  if DEBUG:
    if manu:
      print("[DEBUG]   date : %s, relevée manuellement." % date)
    else:
      print("[DEBUG]   date : %s" % date)
    print("[DEBUG]   qual : %s" % dateq)
    print("[DEBUG]   reg  : %s" % region)
    print("[DEBUG]   desc : %s" % desc)
  
  d2kab_basename = get_d2kab_basename(line)
  d2kab_bsvname = get_d2kab_bsvname(line, region, date)
  url_pdf = "%spdf/%s.pdf" % (DEST_URL, d2kab_bsvname)
  url_html = "%shtml/%s.html" % (DEST_URL, d2kab_bsvname)
  
  if DEST_PATH is not None:
    dest = "%spdf/%s.pdf" % (DEST_PATH, d2kab_bsvname)
    if DEBUG:
      print("[DEBUG]   copy to %s" % dest)
    d = file_copy(line, dest)
    if d != dest:
      url_pdf = "%s%s" % (DEST_URL, d[len(DEST_PATH):])
      num = re.findall(r'_[0-9]+\.pdf', d)[-1][1:4]
      url_html = "%shtml/%s_%s.html" % (DEST_URL, d2kab_bsvname, num)
  
  if TEST_URL:
      exists_pdf = url_exists(url_pdf)
      exists_html = url_exists(url_html)
      if DEBUG:
          print("[DEBUG]   %s exists : %s" % (url_pdf, exists_pdf))
          print("[DEBUG]   %s exists : %s" % (url_html, exists_html))
  else:
      exists_pdf = True
      exists_html = True
      if DEBUG:
          print("[DEBUG]   url_pdf : %s" % (url_pdf))
          print("[DEBUG]   url_html : %s" % (url_html))
  
  # ===============================
  # ===== Now write turte-rdf =====
  # ===============================
  bsv_uri = "%sresources/%s" % (ex, d2kab_bsvname)
  ttl = "## --- GRAPH ex:graph/corpus --- ##\n"
  ttl += "ex:resources/corpusVespa prov:hadMember <%s> .\n" % bsv_uri
  if IS_TEST_CORPUS:
      ttl += "ex:resources/%s " % TESTCORPUS_NAME
      ttl += "prov:hadMember <%s> .\n" % bsv_uri
  
  ttl += "## --- GRAPH ex:graph/activities --- ##\n"
  ttl += 'ex:resources/collecte_manuelle'
  ttl += ' prov:generated <%s_pdf> .\n' % bsv_uri
  
  ttl += "## --- GRAPH ex:graph/bsv --- ##\n"
  ttl += "<%s> a d2kab:Bulletin, prov:Entity ;\n" % bsv_uri
  ttl += '  dct:description "%s."@fr ;\n' % desc
  ttl += '  prov:wasGeneratedBy ex:resources/collecte_manuelle ;\n'
  ttl += '  dct:spatial wd:%s ;\n' % region
  ttl += '  dct:date "%s"^^xsd:date ;\n' % date
  ttl += '  d2kab:dateExtractionQuality "%s %%"^^cdt:dimensionless ;\n' % dateq
  ttl += '  dul:isRealizedBy <%s_pdf> .\n' % bsv_uri
  ttl += '<%s_pdf> a prov:Entity, dct:Text ;\n' % bsv_uri
  if TEST_URL and not exists_pdf:
      ttl += "  # WARNING: URL doesn't exist (yet) :\n"
  ttl += '  schema:url <%s> ;\n' % url_pdf
  ttl += '  prov:wasGeneratedBy ex:resources/collecte_manuelle ;\n'
  ttl += '  rdfs:comment "Entité qui représente une copie, '
  ttl += 'stockée sur le serveur TSCF, du %s."@fr ;\n' % desc
  ttl += '  dce:language "fr-FR" ;\n'
  ttl += '  dce:format "application/pdf" ;\n'
  ttl += '  oa:textDirection oa:ltr .\n'
  return ttl

############################ D2KAB ###############################
def try_d2kab(line, dates_manu):
  bsv_name = os.path.splitext(os.path.basename(line))[0]
  rb = sparql_query(COLLECT_ENDPOINT+'/query',
"""prefix dwl: <http://ontology.irstea.fr/bsv_dwnl/>
SELECT ?bsv ?dt ?txt WHERE {
?bsv a dwl:BSVDownload ; dwl:datetime ?dt ; dwl:status "OK";
dwl:local_file ?f .
OPTIONAL { ?bsv dwl:has_text ?txt }
FILTER contains(?f, "%s") }""" % bsv_name)

  if (rb is None) or (len(rb) == 0):
    if DEBUG:
      print("[DEBUG] %s non trouvé dans la collecte Auto" % bsv_name)
    return None

  ## Dans le cas d'un BSV téléchargé en http ET en https,
  ## le second écrase le premier et on n'aura qu'un seul 
  ## fichier, ce qui est un comportement qu'on estime
  ## convenable.
  ## À noter que les préfixes région/année ont disparu
  ## lors du passage à xR2RML, ce qui peut mener à des
  ## erreurs si deux BSV ont exactement le même nom de
  ## fichier. L'expérience semble montrer que ceci n'est
  ## le cas que pour les notes nationales. À VÉRIFIER.
  rb = rb[0]
  date_collecte = rb['dt']['value'].split('T')[0]
  if rb.get('txt') is not None:
    hyperlink_text = rb['txt']['value']
  else:
    hyperlink_text = rb['bsv']['value'].split('/')[-1]
  if re.match('.*[?.!]$', hyperlink_text):
    hyperlink_text = hyperlink_text[:-1]          
  download_url  = rb['bsv']['value']
  region = region_from_url.search(download_url)
  if region is None:
      wd_region = None
  else:
      region = region[1]
      wd_region = REGIONS.get(region)
  if wd_region is None:
      sys.stderr.write("=ERROR==> Région non trouvée dans l'URL %s\n" % download_url)
  
  date = None
  if dates_manu is not None:
    if dates_manu.get(bsv_name) is not None:
      try:
          date = CSV_GET_XSD_DATE(dates_manu[bsv_name])
      except Exception as e:
          pass
  if DEBUG:
      if date is not None:
          print("[DEBUG]   date : %s, relevée manuellement." % date)
      else:
          print("[DEBUG]   Pas de date de publication (pour le moment)")
      print("[DEBUG]   reg  : %s" % region)
      print("[DEBUG]   desc : %s" % hyperlink_text)
  
  ### SHOULD call pdf2blocks here, and extract date.
  
  d2kab_basename = get_d2kab_basename(line)
  if date is not None:
      d2kab_bsvname = get_d2kab_bsvname(line, wd_region, date)
  else :
      d2kab_bsvname = get_d2kab_bsvname(line, wd_region, date_collecte)
  url_pdf = "%spdf/%s.pdf" % (DEST_URL, d2kab_bsvname)
  url_html = "%shtml/%s.html" % (DEST_URL, d2kab_bsvname)
  
  if DEST_PATH is not None:
      dest = "%spdf/%s.pdf" % (DEST_PATH, d2kab_bsvname)
      if DEBUG:
          print("[DEBUG]   copy to %s" % dest)
      d = file_copy(line, dest, False, True) ### A Tester
      if d != dest:
          url_pdf = "%s%s" % (DEST_URL, d[len(DEST_PATH):])
          num = re.findall(r'_[0-9]+\.pdf', d)[-1][1:4]
          url_html = "%shtml/%s_%s.html" % (DEST_URL, d2kab_bsvname, num)
  
  if TEST_URL:
      exists_pdf = url_exists(url_pdf)
      exists_html = url_exists(url_html)
      if DEBUG:
          print("[DEBUG]   %s exists : %s" % (url_pdf, exists_pdf))
          print("[DEBUG]   %s exists : %s" % (url_html, exists_html))
  else:
      if DEBUG:
          print("[DEBUG]   url_pdf : %s" % (url_pdf))
          print("[DEBUG]   url_html : %s" % (url_html))
  
  # ================================
  # ===== Now write turtle-rdf =====
  # ================================
  bsv_uri = "%sresources/%s" % (ex, d2kab_bsvname)
  dt = ''.join(date_collecte.split('-')) # YYYYMMDD
  ttl = ''
  ttl += "## --- GRAPH ex:graph/activities --- ##\n"
  if date_collecte not in dates_collectes:
      dates_collectes.append(date_collecte)
      ttl += "ex:resources/collecte%s a prov:Activity ;\n" % dt
      ttl += '  prov:startedAtTime "%s"^^xsd:date ;\n' % date_collecte
      ttl += '  prov:endedAtTime "%s"^^xsd:date ;\n' % date_collecte
      dt_fr = '/'.join([date_collecte.split('-')[x] for x in [2,1,0]])
      ttl += '  rdfs:comment "Activité démarrée le %s ' % dt_fr
      ttl += 'avec l\'outil de collecte automatique des BSV."@fr ;\n'
      ttl += '  prov:wasAssociatedWith ex:resources/draafWebCrawler_1_0'
      ttl += ' .\n'
  
  bsv_uri = "%sresources/%s" % (ex, d2kab_bsvname)
  
  ttl += 'ex:resources/collecte%s' % dt
  ttl += '  prov:generated <%s_pdf> .\n' % bsv_uri
  
  ttl += "## --- GRAPH ex:graph/corpus --- ##\n"
  ttl += "ex:resources/corpusAuto prov:hadMember <%s> .\n" % bsv_uri
  if IS_TEST_CORPUS:
      ttl += "ex:resources/%s " % TESTCORPUS_NAME
      ttl += "prov:hadMember <%s> .\n" % bsv_uri
  ttl += "## --- GRAPH ex:graph/bsv --- ##\n"
  ttl += "<%s> a d2kab:Bulletin, prov:Entity ;\n" % bsv_uri
  ttl += '  dct:description "%s"@fr ;\n' % hyperlink_text
  if wd_region is not None:
      ttl += '  dct:spatial wd:%s ;\n' % wd_region
  if date is not None:
      ttl += '  dct:date "%s"^^xsd:date ;\n' % date
      if len(date) > 7: # date could be '' but should be 'YYYY-MM-DD'
          ttl += '  d2kab:dateExtractionQuality "100 %"^^cdt:dimensionless ;\n'
  ttl += '  dul:isRealizedBy <%s_pdf> .\n' % bsv_uri
  ttl += '<%s_pdf> a prov:Entity, dct:Text,' % bsv_uri
  ttl += ' schema:DigitalDocument ;\n'
  if TEST_URL and not exists_pdf:
      ttl += "  # WARNING: URL doesn't exist (yet) :\n"
  ttl += '  schema:url <%s> ;\n' % url_pdf
  ttl += '  schema:isBasedOn <%s> ;\n' % download_url
  ttl += '  rdfs:comment "Entité qui représente une copie, '
  ttl += 'stockée sur le serveur TSCF, du «%s»."@fr ;\n' % hyperlink_text
  ttl += '  prov:wasGeneratedBy ex:resources/collecte%s;\n' % dt
  ttl += '  dce:language "fr-FR" ;\n'
  ttl += '  dce:format "application/pdf" ;\n'
  ttl += '  oa:textDirection oa:ltr .\n'  
  return ttl

# +--------------------------------------------------------------+
# |                           main                               |
# +--------------------------------------------------------------+
if (len(sys.argv) >= 1):
  if "--help" in sys.argv[1:]:
    print("Usage : python %s [options] [<file1> [<file2> …]]" % sys.argv[0])
    print("  If no filename is given, it'll get them from standard input")
    print("  (so you can pipe the result of a find command for example).")
    print('Options :')
    print("  --corpus_test=<Value> :")
    print("            If present, the files are included in a test corpus.")
    print("            The test corpus IRI will be ex:resources/corpusTest<Value>")
    print("            and should be declared in collections.ttl.")
    print("            Possible values : %"% TEST_CORPUS_NAMES)
    print("  --copy_bsv=/path/to/dest : Do a copy of the pdf files,")
    print("                  organized the same way as their URL into")
    print("                  dest directory.")
    print("  --dates=/path/to/csv/file : A csv file containing PHB")
    print("                  publication dates.")
    print("  --debug : Print step by step process.")
    print("            Result should not be piped to doSparqlUpdate.py.")
    print("")
    sys.exit(0)

  DEBUG = ("--debug" in sys.argv[1:])
  if DEBUG :
    sys.argv.remove("--debug")
    
  for arg in sys.argv:
    if arg.startswith("--corpus_test"):
      if not arg.startswith("--corpus_test="):
        print("ERROR : --corpus_test option's syntax is --corpus_test=<Value>")
        print("ERROR : <Value> Should be in %s" % TEST_CORPUS_NAMES)
        sys.exit(-1)
      IS_TEST_CORPUS = True
      tcname = ('='.join(arg.split('=')[1:]))
      if tcname not in TEST_CORPUS_NAMES:
        print("ERROR : --corpus_test option's syntax is --corpus_test=<Value>")
        print("ERROR : <Value> Should be in %s" % TEST_CORPUS_NAMES)
        sys.exit(-1)
      TESTCORPUS_NAME = 'corpusTest'+tcname
      sys.argv.remove(arg)
      break

  for arg in sys.argv:
    if arg.startswith("--copy_bsv"):
      if not arg.startswith("--copy_bsv="):
        print("ERROR : --copy_bsv option's syntax is --copy_bsv=/path/to/dest")
        sys.exit(-1)
      DEST_PATH = '='.join(arg.split('=')[1:])
      sys.argv.remove(arg)
      break
      
  for arg in sys.argv:
    if arg.startswith("--dates"):
      if not arg.startswith("--dates="):
        print("ERROR : --dates option's syntax is --dates=/path/to/csv/file")
        sys.exit(-1)
      CSV_DATE_FILE = '='.join(arg.split('=')[1:])
      sys.argv.remove(arg)
      break
      

if DEST_URL[-1] != '/':
  DEST_URL += '/'
if DEST_PATH == 'None': DEST_PATH = None
if DEST_PATH is not None:
  DEST_PATH = os.path.realpath(os.path.expanduser(DEST_PATH)) + '/'


if DEBUG:
  print("[DEBUG] Parameters :")
  print("[DEBUG]   DEBUG is ON")
  print("[DEBUG]   DEST_PATH = %s" % DEST_PATH)
  print("[DEBUG]   DEST_URL = %s" % DEST_URL)
  if IS_TEST_CORPUS:
    print("[DEBUG]   TESTCORPUS_NAME = %s" % TESTCORPUS_NAME)
  else:
    print("[DEBUG]   Is not a test corpus.")

dt_manu = None
if CSV_DATE_FILE is not None:
    dt_manu = {}
    with open(CSV_DATE_FILE, newline='') as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=CSV_DATE_COLUMNS,
                delimiter=CSV_DATE_DELIMITER)
        for row in reader:
            dt_manu[CSV_GET_BSV(row['bsv'])] = row['date']

prefixes = load_prefixes(PREFIXES)
if prefixes is None:
    ex = EX_PREFIX
else:
    ex = prefixes['ex']

#organisation, conversion = get_current_acivity_names()
#print(create_current_activity())

if (len(sys.argv) == 1):
    inp = sys.stdin
else:
    ch = "\n".join(sys.argv[1:])
    inp = io.StringIO(ch)

BSV_written = 0
for li in inp:
  if li[-1] == '\n':
    li = li[:-1]
  if li[-1] == '*': # command file adds ugly trailing '*'…
    li = li[:-1]

  nom_bsv = os.path.splitext(os.path.basename(li))[0]
  if DEBUG:
    print("[DEBUG] ===== BSV_NAME : %s" % nom_bsv)
  
  turtle = try_vespa(li, dt_manu)
  if turtle is None:
    turtle = try_d2kab(li, dt_manu)
  if turtle is None:
    sys.stderr.write("=ERROR=====> %s :\n" % nom_bsv)
    sys.stderr.write("  BSV non trouvé, donc non traité.\n")
    # TODO ?
    
  else:
    print(turtle)
    
    BSV_written += 1
    if (BSV_written % UPDATE_EVERY_N_BSV) == 0:
      print("## --- UPDATE --- ##")
  

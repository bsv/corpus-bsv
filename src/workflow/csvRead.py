#!/usr/bin/env python3

# Programme qui lit un fichier csv généré par alvisNLP et le met
# en forme pour qu'il soit utilisé par xr2rml.
# Traitements effectués :
# - le séparateur (<tab>) devient virgule,
# - ajout de colonnes regin, annee, ref_short, body_name, body_type
#   et idBSV, utilisées pour les URI,
# - suppression des colonnes "location" et "features"
#
import sys
import re
import csv
from datetime import date
from etl_functions import *
from config import *


# --- Noms des colonnes ---
BSV = 'bsv'
CORPUS = 'corpus'
HTMLTAGS = 'html_tags'
XPATH = 'xpath_num'
ENTITY = 'entity'
ENTITYID = 'entity_id'
PREFLBL = 'skos-prefLabel'
LEMMA = 'lemma'
NBWORDS = 'number of words'
START = 'start_offset'
END = 'end_offset'
BEFORE = 'context_before'
AFTER = 'context_after'
BODY = 'reference'


# --- Outils ---

# Une fonction un peu (trop) vite écrite pour retirer les caractères gênants
# dans les chaînes de caractères oa:prefix, oa;suffix et oa:exact.
# Pour le moment :
# - On sépare les : des caractères qui les précèdent par une espace,
# - on remplace les retours à la ligne par une espace
# - on remplace les double-quotes par deux simple-quotes.
def unescape(ch):
    return ch.replace(':', " :").replace('"', "''").replace('\n', ' ')

# --- Arguments ---
DEBUG = ("--debug" in sys.argv[1:])
if DEBUG :
  sys.argv.remove("--debug")

if (len(sys.argv) > 2):
  print("Usage : python %s [options] <filename>" % sys.argv[0])
  print("  If no filename is given, it will read from sandard input.")
  print("  Options :")
  print("    --debug : Print verbose output for debugging.")
  print("              The result is not csv complient")
  print("              and cannot be imported to mogodb.")
  sys.exit(-1)


# --- Init ---
prefixes = load_prefixes(PREFIXES)
EX_PREFIX = prefixes['ex']
prefixes_str = '\n'.join(['PREFIX %s: <%s>'%(k,v) for k,v in prefixes.items() ])
SPARQL_QUERY_SERVER = SPARQL_SERVER+BSV_DATASET+'/query'

BBCHshort = re.compile(r'^.*_BBCH')
search_uri = re.compile(r"uri=([^\,]*)(\,|$)")

lastbsv = "un nom de BSV bidon"
lastloc = "-1-1-"
dt_short = date.today().strftime('%y%m%d')

search_fcu = re.compile(r'/(fcu|frenchcropusage)/')
search_ppdo = re.compile(r'/ppdo/')

# --- Open csv file
if (len(sys.argv) == 2):
  csvfile = open(sys.argv[1], 'r', newline='', encoding='utf-8')
else:
  csvfile = sys.stdin

csvreader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')

line = '"bsv","corpus","html_tags","xpath_num","entity",'
line += '"prefLabel",'
#line += '"canonical-form",'
line += '"context_before","context_after",'
line += '"lemma", "number of words","entity_id","start_offset",'
line += '"end_offset","reference","ref_short","bdyname","bdytype",'
line += '"region","annee","idBSV","num","dt"'
print(line)

# --- Read csv file row per row
num = 0
for r in csvreader:
  bsv_parts = r['bsv'].split('/')
  bsv = bsv_parts[-1]
  idBSV = slugify('.'.join(bsv.split('.')[:-1]))

  if idBSV != lastbsv:
    num = 1
    lastbsv = idBSV
    lastloc = "%s-%s" % (r[START], r[END])
  else:
    loc = "%s-%s" % (r[START], r[END])
    if lastloc != loc:
      num += 1
    lastloc = loc

  body_type = ""
  if search_fcu.search(r[BODY]):
    body_type = "FCU"
  if search_ppdo.search(r[BODY]):
    body_type = "PPDO"

  line = '"' + r[BSV]
  line += '","' + r[CORPUS]
  line += '","' + r[HTMLTAGS]
  line += '","' + r[XPATH]
  line += '","' + r[ENTITY]
  line += '","' + r[PREFLBL]
  #line += '","' + r['canonical-form']
  line += '","' + r[BEFORE]
  line += '","' + r[AFTER]
  line += '","' + r[LEMMA]
  line += '",' + r[NBWORDS]
  line += ',"' + r[ENTITYID]
  line += '",' + r[START]
  line += ',' + r[END]
  line += ',"' + r[BODY]
  line += '","' + r[BODY].split('/')[-1]
  line += '","' + slugify(unescape(r[ENTITY]))[:40]
  line += '","' + body_type
  line += '","' + bsv_parts[-3]
  line += '","' + bsv_parts[-2]
  line += '","' + idBSV
  line += '","%03d","%s"' % (num, dt_short)
  print(line)



### Some tool functions for different ttl generators
import unicodedata
import requests
import json
import re
import os
import sys
import shutil
from datetime import date


## -----------------------------------------------
## ---------- To make "valid" filenames ----------
## -----------------------------------------------

# https://stackoverflow.com/questions/295135/turn-a-string-into-a-valid-filename
def slugifyterm(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    WARNING : Deletes dots
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value)
    return re.sub(r'[-\s]+', '-', value).strip('-_')[:124]

def slugify(value, allow_unicode=False):
    res = ''
    value = str(value)
    for s in value.split('/'):
        if len(s) > 0:
            w = ''
            for d in s.split('.'):
                if len(d) > 0:
                    if len(w) == 0:
                        w = slugifyterm(d)
                    else:
                        w = "%s.%s" % (w,slugifyterm(d))
            if len(w) > 0:
                if len(res) == 0:
                    res = w
                else:
                    res = "%s/%s" % (res, w)
    return res


## -------------------------------
## ---------- copy file ----------
## -------------------------------
## Copies a file to its destination. Creates the destination
## directory structure if its needed.
## ...but does not slugify (you have to do it yourself before)
def file_copy(src, dest, rename_if_exists = True, overwrite = False):
    dest_dir = "/".join(dest.split('/')[:-1])
    dest_base = '.'.join(dest.split('.')[:-1])
    dest_extension = dest.split('.')[-1]
    try:
        os.makedirs(dest_dir, exist_ok=True)
        if rename_if_exists:
          i = 0
          while os.path.exists(dest):
            i += 1
            dest = "%s_%03d.%s" % (dest_base, i, dest_extension)
        else:
          if (not overwrite) and (os.path.exists(dest)):
            return None
        shutil.copy2(src, dest)
        return dest
    except Exception as e:
        sys.stderr.write("=ERROR=====> copying %s\n" % src)
        sys.stderr.write("%s" % e)
    return None

## -----------------------------------------
## ---------- d2kab BSV filenames ----------
## -----------------------------------------

def get_d2kab_basename(bsv_name):
  return slugify(".".join(bsv_name.split("/")[-1].split('.')[0:-1]))

## Returns the designation of a BSV in D2KAB, without the extension
## and without the full path. This is to be completed as an URL,
## a file path, ans its needed form (pdf, html, ...)
##
## ex : get_d2kab_bsvname("BSV*17#34_cle2514.pdf", "Q1137", "2018-12-11")
## returns : 'Q1137/2018/BSV1734_cle2514'
##
## date shoud be a string containig "YYYY-whatever" or None or ""
## region should be "Qxxxxx" (wikidata name) or None or ""
def get_d2kab_bsvname(bsv_name, region, date):
    if region is None or len(region) == 0 or region == 'NotLocated' :
        region = 'NotLocated'
    else:
        region = region.upper()
    if date is None or len(date) == 0:
        date = "NoDate"
    else:
        date = date.split('-')[0]
    return "%s/%s/%s" % (region, date, get_d2kab_basename(bsv_name))



## --------------------------------------------
## ---------- get prefixes from file ----------
## --------------------------------------------
## Returns a dictionnary {'prefix':'uri'},
## e.g { 'dct': 'http://purl.org/dc/terms/', ... }
## Returns None if file not found or file is None.

def load_prefixes(file, write_errors = True):
    find_prefix = re.compile(r'@?prefix\s+(\S+):\s*<([\S]+)>\s*\.?', flags=re.I)
    prefixes = {}
    if file is None:
        return None
    f = open(file,"r")
    lines = f.readlines()
    for l in lines:
        pr = find_prefix.findall(l.strip())
        for e in pr:
            if prefixes.get(e[0]) is None:
                prefixes[e[0]] = e[1]
            elif write_errors and (prefixes[e[0]] != e[1]):
                sys.stderr.write("ATTENTION: préfixe %s: défini 2× :\n" % e[0])
                sys.stderr.write("    <%s> et <%s>.\n" % (prefixes[e[0]], e[1]))
                sys.stderr.write("    <%s> sera utilisé.\n\n" % prefixes[e[0]])
    return prefixes



## ------------------------------------------------
## ---------- get current activity names ----------
## ------------------------------------------------
## Returns 'ex:resources/stockageYYYYMMDD', 'ex:resources/conversionYYYYMMDD'
## for current activity
#### OBSOLETE : Voir create_current_activity()
#def get_current_acivity_names():
#    dt = date.today().strftime('%Y%m%d')
#    st = 'ex:resources/organisation%s' % dt
#    co = 'ex:resources/conversion%s' % dt
#    return st,co

## ---------------------------------------------
## ---------- create current activity ----------
## ---------------------------------------------
## The etl process currently running does the ex:resources/stockage
## and maybe the ex:resources/conversion activities.
## This function returns the ttl code to create these prov:Activity
## instances. It doesn't matter if it's created many times ; it has
## finally only one instance.
#### OBSOLETE : Le stockage ne désigne plus que jena-fuseki,
####            un script spécifique le met à jour.
####            L'activité de conversion s'auto-documente.
#def create_current_activity():
#    dt = date.today().strftime('%Y-%m-%d')
#    stockage, conversion = get_current_acivity_names()
#    ret = "## --- GRAPH ex:graph/activities --- ##\n"
#    #ret += '%s a prov:Activity ;\n' % stockage
#    #ret += '  prov:startedAtTime "%s"^^xsd:date ;\n' % dt
#    #ret += '  prov:wasAttributedTo ex:resources/stephan ;\n'
#    #ret += '  prov:wasAssociatedWith ex:/resources/jena_fuseki ;\n'
#    #ret += '  rdfs:comment "Activité de stockage et de structuration des pdf'
#    #ret += ' démarrée le %s.' % date.today().strftime('%d/%m/%Y')
#    #ret += ' Le stockage est assuré par Jena Fuseki"@fr ;\n.\n'
#    ret += '%s a prov:Activity ;\n' % conversion
#    ret += '  prov:startedAtTime "%s"^^xsd:date ;\n' % dt
#    ret += '  prov:wasAttributedTo ex:resources/stephan ;\n'
#    ret += '  prov:wasAssociatedWith ex:/resources/pdf2blocks_1_0 ;\n'
#    ret += '  rdfs:comment "Activité de conversion des pdf en html'
#    ret += ' avec l\'outil pdf2blocks"@fr ;\n.\n'
#    return ret



## --------------------------------------------
## ---------- Check if an URL exists ----------
## --------------------------------------------
## ...without downloading its content.
def url_exists(url):
    with requests.get(url, stream=True) as r:
        code = r.status_code
        r.close()
    return code == 200


# ------------------------------------
# ----------- sparql_update ----------
# ------------------------------------
def sparql_update(server, query):
    query = { 'update': query }
    req = requests.post(server, data=query)
    if (req.status_code != requests.codes.ok):
        #return '%d - %s' % (req.status_code, req.text)
        return req.status_code, req.text
    return None, None


## ----------------------------------
## ---------- sparql_query ----------
## ----------------------------------
def sparql_query(server, sparql_query, silent=False):
    headers = {'Accept':'application/json'}
    query = {'query': sparql_query}

    req = requests.get(server,  params=query, headers=headers)
    if (req.status_code != requests.codes.ok):
        if not silent:
            sys.stderr.write("\n=SPARQL ERROR===> [%d] :\n%s\n" % (req.status_code, query['query']))
        return None
    s = json.loads(req.text)
    return s['results']['bindings']

@prefix res: <http://opendata.inrae.fr/bsv-res/> .
@prefix def: <http://opendata.inrae.fr/bsv-def/> .
@prefix tapo: <https://opendata.inrae.fr/tap-def#> .
@prefix fcu: <http://opendata.inrae.fr/fcu-res/> .
@prefix dctypes: <http://purl.org/dc/dcmitype/> .
@prefix dce: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix cdt:   <http://w3id.org/lindt/custom_datatypes#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix schema: <http://schema.org/> .
@prefix dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#> .
@prefix oa: <http://www.w3.org/ns/oa#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix ms: <http://w3id.org/meta-share/meta-share/> .
@prefix frac: <http://www.w3.org/ns/lemon/frac#> .
@prefix oa: <http://www.w3.org/ns/oa#> .
@prefix vann: <http://purl.org/vocab/vann/> .
@prefix wd: <http://www.wikidata.org/entity/> .
@prefix sd: <https://www.w3.org/ns/sparql-service-description#> .


<https://opendata.inrae.fr/bsv-def> rdf:type owl:Ontology, sd:Service ;
  owl:versionIRI <https://opendata.inrae.fr/bsv-def/1.0> ;
  dct:contributor res:stephan, res:catherine, res:anna, res:marine ,
          "Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499)" ,
          "Anna CHEPAIKINA, TSCF, MAIAGE, INRAE (https://orcid.org/0000-0001-7494-7748)" ,
          "Marine COURTIN, MAIAGE, INRAE (https://orcid.org/0000-0003-4189-4322)" ,
          "Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443)" ;
  dct:creator res:stephan ;
  dct:description "Ontologie associée au dataset des Bulletins de Santé du Végétal (BSV), définissant des concepts et propriétés permettant de compléter les information relatives à ces bulletins."@fr ,
          "Ontology related Plant Health Bulletin (PHB) dataset. This ontology defines concepts and properties to complete informations relative to PHBs."@en ;
  dct:issued "2021-05-27T00:00:00Z"^^xsd:dateTime ;
  dct:modified "2024-05-27T11:02:34Z"^^xsd:dateTime ;
  dct:publisher "INRAE (https://www.inrae.fr/)" ;
  dct:rightsHolder "Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499)"@fr ;
  dct:title "PHB Ontology"@en ,
          "Ontologie des BSV"@fr ;
  vann:preferredNamespacePrefix "bsv-def" ;
  vann:preferredNamespaceUri <https://opendata.inrae.fr/bsv-def> ;
  owl:versionInfo 1.0 ;
  
  sd:endpoint <https://rdf.codex.cati.inrae.fr/bsv-def/sparql> ;
  sd:supportedLanguage sd:SPARQL11Query ;
  sd:resultFormat <http://www.w3.org/ns/formats/RDF_XML>,
          <http://www.w3.org/ns/formats/Turtle>,
          <https://www.w3.org/ns/formats/JSON-LD>;
  sd:feature sd:DereferencesURIs ;
.


def:dataset_BSV rdf:type owl:Ontology ;
  dct:title "Annotated Plant Health Bulletin dataset"@en ,
          "Dataset des Bulletins de Santé du Végétal annotés"@fr ;
  owl:versionIRI <https://opendata.inrae.fr/bsv-res/1.0> ;
  owl:imports <https://opendata.inrae.fr/bsv-def> ;
  dct:accessRights "public"^^rdfs:Literal ;
  dct:accrualPeriodicity <http://purl.org/cld/freq/completelyIrregular> ;
  dct:bibliographicCitation <https://dx.doi.org/10.1016/j.compag.2017.10.022> ,
          "C. ROUSSEY, S. BERNARD, F. PINET, X. REBOUD, V. CELLIER, I. SIVADON, D. SIMONNEAU, AL. BOURIGAULT. A methodology for the publication of agricultural alert bulletins as LOD. Computers and Electronics in Agriculture . Volume 142, partie B , novembre 2017, pp. 632-650 . https://dx.doi.org/10.1016/j.compag.2017.10.022, https://hal.inrae.fr/hal-02606598" ;
  dct:contributor res:stephan, res:catherine, res:anna, res:marine,
          "Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499)" ,
          "Anna CHEPAIKINA, TSCF, MAIAGE, INRAE (https://orcid.org/0000-0001-7494-7748)" ,
          "Marine COURTIN, MAIAGE, INRAE (https://orcid.org/0000-0003-4189-4322)" ,
          "Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443)" ;
  dct:coverage "France"^^rdfs:Literal ;
  dct:created "2016-06-22T00:00:00Z"^^xsd:dateTime ;
  dct:description """Dataset des Bulletins de Santé du Végétal.
Les Bulletins de Santé du Végétal sont téléchargés depuis les sites web des DRAAF puis convertis au format html pour être annotés à l'aide du logiciel AlvisNLP de l'unité MAIAGE de l'INRAE."""@fr ;
  dct:format <http://www.w3.org/ns/formats/RDF_XML> ,
          <http://www.w3.org/ns/formats/Turtle> ;
  dct:issued "2016-06-22T00:00:00Z"^^xsd:dateTime ;
  dct:license <https://creativecommons.org/licenses/by/4.0/> ;
  dct:modified "2024-05-27T11:18:25Z"^^xsd:dateTime ;
  dct:publisher "INRAE (https://www.inrae.fr)"^^rdfs:Literal ;
  dct:rightsHolder "Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499)"^^rdfs:Literal ;
  vann:preferredNamespacePrefix "bsv_res" ;
  vann:preferredNamespaceUri <https://opendata.inrae.fr/bsv-res> ;
  owl:priorVersion <http://ontology.inrae.fr/bsv> ;
  owl:versionInfo "1.0" .



def:Bulletin a owl:Class ;
  rdfs:label	"Bulletin de Santé du Végétal, bulletin, bulletin agricole"@fr ;
  rdfs:comment	"Création intellectuelle, ensemble d'informations relatives à un bulletin indépendemment de comment le bulletin est réalisée concrétement. Voir la classe Oeuvre ou Manifestation dans data.bnf.fr. Dans notre cas nous parlons d'objet d'information."@fr ;
  rdfs:subClassOf	dul:InformationObject .


def:dateExtractionQuality a owl:DatatypeProperty ;
  rdfs:label "indice de qualité de la sélection de date"@fr ;
  rdfs:label "date extraction quality"@en ;
  rdfs:comment "Un indicateur de la qualité de la date choisie parmi celles issues d'une série de processus automatiques. Une valeur de 100 indique que tous les processus ont trouvé la même date. Il n'y a pas de zéro, qui signifie une absence de date. Cet indicateur concerne les valeurs associées aux propriétés dc:date et d2kab:text_date, sauf dans le cas où une d2kab:manual_date est renseignée."@fr ;
  rdfs:comment "A quality value on dates found by our automatic extraction processes. A value of 100 means that all processes have returned the same date. There is no zero as it means that no process returned a date. This value concerns the dates designed by the properties dc:date and d2kab:text_date except if a d2kab:manual_date exists."@en ;
  rdfs:range xsd:integer .


def:htmlTag a owl:DatatypeProperty ;
  rdfs:label "Balise html contenant le texte"@fr ;
  rdfs:label "Html tag containing matching text"@en ;
  rdfs:comment "Permet d'identifier la balise html générée par pdf2blocs qui contient l'occurence de texte. Permet de savoir notamment s'il s'agit d'un titre,d'une légende d'image, …"@fr ;
  rdfs:comment "Allows to identify html tag genrated by pdf2blocs containing matching text. Gives ability to know if this text is contained into a title, a picture's caption, …"@en ;
  rdfs:range xsd:string ;
  rdfs:domain oa:Selector ;
  .

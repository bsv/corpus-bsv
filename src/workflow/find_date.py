import sys
import io
import os
import re
import arrow
import json

from datetime import datetime, date, timedelta, timezone

from DateParser import *

##################### Dates #####################
FROM = date(2008,1,1)
TO = date.today() - timedelta(days = 1)
LOCALE = 'fr_FR.UTF-8'

MONTH_RE = DateParser.MONTH_RE
MONTH_SUB = DateParser.MONTH_SUB
FILENAME_DT_FORMATS = [
  {'regex': r'(?P<day>\d\d?)\W?'+MONTH_RE+'\W?(?P<year>\d{4})', 'fmt':('%d','%B','%Y')}, # 15 janvier 2017
  {'regex': r'(?P<day>\d\d?)\W?'+MONTH_RE+'\W?(?P<year>\d{2})\D', 'fmt':('%d','%B','%y')}, # 15 janvier 17
  {'regex': r'(?P<day>1)\W?er\W?'+MONTH_RE+'\W?(?P<year>\d{4})', 'fmt':('%d','%B','%Y')}, # 15 janvier 2017
  {'regex': r'(?P<day>1)\W?er\W?'+MONTH_RE+'\W?(?P<year>\d{2})\D', 'fmt':('%d','%B','%y')}, # 15 janvier 17
  {'regex': r'(?P<day>\d\d?)\W(?P<month>\d\d?)\W(?P<year>\d{4})', 'fmt':('%d','%m','%Y')}, # 15 01 2017
  {'regex': r'\D(?P<year>\d{4})\W(?P<month>\d\d?)\W(?P<day>\d\d?)\D', 'fmt':('%d','%m','%Y')}, # 2017 01 15
  {'regex': r'(?P<day>\d\d?)/(?P<month>\d\d?)/(?P<year>\d{2})\D', 'fmt':('%d','%m','%y')}, # 15/01/17
  {'regex': r'20(?P<year>\d{2})(?P<month>\d{2})(?P<day>\d{2})\D', 'fmt':('%d','%m','%y')}, # 20170115
  {'regex': r'(?P<day>\d{2})(?P<month>\d{2})20(?P<year>\d{2})', 'fmt':('%d','%m','%y')}, # 15012017
  {'regex': r'\D(?P<day>\d{2})\W?(?P<month>\d{2})\W?(?P<year>\d{2})\D', 'fmt':('%d','%m','%y')}, # 150117
]
FILENAME_DT_FORMATS_IF_NONE = [
  {'regex': r'\D(?P<year>\d{2})\W?(?P<month>\d{2})\W?(?P<day>\d{2})\D', 'fmt':('%d','%m','%y')}, # 150117
]
TEXT_DT_FORMATS = [
  {'regex': r'(?P<day>\d\d?)\W?'+MONTH_RE+'\W?(?P<year>\d{4})', 'fmt':('%d','%B','%Y')}, # 15 janvier 2017
  {'regex': r'(?P<day>\d\d?)\W?'+MONTH_RE+'\W?(?P<year>\d{2})\D', 'fmt':('%d','%B','%y')}, # 15 janvier 17
  {'regex': r'(?P<day>1)\W?er\W?'+MONTH_RE+'\W?(?P<year>\d{4})', 'fmt':('%d','%B','%Y')}, # 15 janvier 2017
  {'regex': r'(?P<day>1)\W?er\W?'+MONTH_RE+'\W?(?P<year>\d{2})\D', 'fmt':('%d','%B','%y')}, # 15 janvier 17
  {'regex': r'(?P<day>\d\d?)/(?P<month>\d\d?)/(?P<year>\d{2})\D', 'fmt':('%d','%m','%y')}, # 15/01/17
  {'regex': r'(?P<day>\d\d?)\W(?P<month>\d\d?)\W(?P<year>\d{4})', 'fmt':('%d','%m','%Y')}, # 15 01 2017
]

# Be careful : Date labels should be ordered so that the first
# is the best method, then decrease. This order is used for choosing
# the publication date.
# Note that the txt_most is after tstore, event if it's better ;
# if a date is found in text, it'll return the txt_first, so…
DATE_LABELS = ['txt_first', 'file', 'tstore', 'txt_most', 'download']

# % of exact dates for each method. Have been computed on test corpuses.
# Note that considering a difference of 1 week, the scores are better.
SCORES = {'txt_first':91 , 'file':92, 'tstore':85,
	'txt_most':91, 'download':0} # The score of download date have not
	# been evaluated, but it should be quite wrong…

OVERALL_SCORE = 91 # Overall score seems not better than filename,
                   # but it returns much more results…


# +--------------------------------------------------------------+
# |                         get_date                             |
# +--------------------------------------------------------------+
### Initialize date parsers
filename_dt_parser = DateParser(FILENAME_DT_FORMATS, LOCALE, MONTH_SUB, FROM,TO)
if TO >= date(2013,1,1):
    filename_dt_parser_if_none = DateParser(FILENAME_DT_FORMATS_IF_NONE,
                                        LOCALE, MONTH_SUB,
                                        max(FROM, date(2013,1,1)), TO)
else:
    filename_dt_parser_if_none = None
text_dt_parser = DateParser(TEXT_DT_FORMATS, LOCALE, MONTH_SUB, FROM,TO)

remove_html_tags = re.compile(r'<[^>]*>')

# Chooses a date for a BSV.
# May return None if no date is found.
def find_date(filename, html_content):
    # --- Search date in filename
    dt = { 'date': None, # The finally chosen date
            'file': None,  # If found in filename
            #'tsore': None, 'download': None,  # Found during collecting
            'txt_most': None, 'txt_first': None } # In whole text
    fic = filename.split("/")
    found = False
    i = 0
    while i < len(fic) and not found:
        i += 1
        filename = re.sub('_','-',fic[-i])
        dt['file'] = filename_dt_parser.getDates(filename,
                                mode = DateParser.GET_FIRST)
        if dt['file'] is not None:
            found = True
    if not found and filename_dt_parser_if_none is not None:
        dt['file'] = filename_dt_parser_if_none.getDates(fic[-1],
                                mode = DateParser.GET_FIRST)

    # --- Search date into text (if still not found)
    bsv_text = remove_html_tags.sub(' ', html_content)
    #print(bsv_text)
    
    all_dts = text_dt_parser.getDates(bsv_text, mode = DateParser.GET_ALL, res = {})
    dt['txt_most'] = text_dt_parser.getMost(all_dts)
    dt['txt_first'] = text_dt_parser.getDates(bsv_text, mode = DateParser.GET_FIRST)
    
    # --- Choose returned date
    nb_none = sum([dt[i] is None for i in dt])
    if nb_none == len(dt):
        return None
    if nb_none <= 2: # One or two values : choose best
      for d in DATE_LABELS:
        if dt[d] is not None:
          dt['date'] = dt[d]
          dt['score'] = SCORES[d]
          return dt

    dist = {}
    for k in dt:
      dist[k] = None
    mini = None
    for i in dt:
      if dt[i]: # dt['date'] is None here.
        dist[i] = 0
        for j in dt:
          if i != j and dt[j]:
            dist[i] += int(abs((dt[i] - dt[j]).days)/7)
        if not mini: mini = dist[i]
        if dist[i] < mini: mini = dist[i]
        
    for i in dt:
      if dt[i] and dist[i] == mini:
        dt['date'] = dt[i]
        dt['score'] = OVERALL_SCORE
        return dt

    return None

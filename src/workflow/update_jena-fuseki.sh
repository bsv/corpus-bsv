#!/bin/bash

### ----- CONFIG -----
if [ -z ${FUSEKI_HOME+x} ]; then 
  which s-update >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    if [[ ${LANG} == fr* ]]; then
      printf '%s\n' "ERREUR : Apache jena-fuseki doit être installé" >&2
      printf '%s\n' "    et son répertoire bin/ déclaré dans la varible PATH." >&2;
    else
      printf '%s\n' "ERROR : Apache jena-fuseki must be installed" >&2
      printf '%s\n' "    and its bin/ directory declared in the PATH." >&2;
    fi
    exit 1;
  fi
  FUSEKI_HOME=$( which s-update | sed -e 's/\/bin\/s-update//' );
fi

${FUSEKI_HOME}/fuseki-server --version >/dev/null 2>&1
if [ $? -ne 0 ]; then
  if [[ ${LANG} == fr* ]]; then
    printf '%s\n' "ERREUR : ${FUSEKI_HOME}/fuseki-server non trouvé." >&2
    printf '%s\n' "    Merci d'initialiser la variable FUSEKI_HOME avec le répertoire" >&2
    printf '%s\n' "    contenant fuseki-server avant d'exécuter ce script." >&2;
  else
    printf '%s\n' "ERROR : ${FUSEKI_HOME}/fuseki-server not found." >&2
    printf '%s\n' "    Please set FUSEKI_HOME with the directory containing" >&2
    printf '%s\n' "    the program fuseki-server before running this script." >&2;
  fi
  exit 2;
fi

## Pour chercher le lien de téléchargement sur le site d'Apache :
#DOWNLOAD_LINK=$( curl https://jena.apache.org/download/index.cgi | sed -e '/apache-jena-fuseki-[0-9]\+\.[0-9]\+\.[0-9]\+\.tar\.gz</!d;s/.*href="\(.*\)".*$/\1/')
## Sinon :
DOWNLOAD_LINK=https://dlcdn.apache.org/jena/binaries/apache-jena-fuseki-4.8.0.tar.gz


# En-dessous, le script cherche les éléments de configuration
# dans config.py et prefixes.ttl.
SCRIPT_PATH=$( readlink -f $0 | sed -e 's/\/[^/]\+$//' )
source ${SCRIPT_PATH}/config.py

## On cherche le préfixe ex: dans prefixes.ttl
EX_EX_PREFIX=$EX_PREFIX
EX_PREFIX=$(cat ${SCRIPT_PATH}/data/prefixes.ttl | grep " ex: " |
  sed -e 's/^.*<\([^<]*\)>.*$/\1/' )
if [ -z "$EX_PREFIX" ]; then EX_PREFIX=$EX_EX_PREFIX; fi



### ----- go -----
FORCE_WRITE=0 # Si ≠ 0 alors ajoute la nouvelle version au triplestore
              # même en cas d'échec de la requête de récupération
              # de la version de jena-fuseki.
for arg in $*; do
  if [ $arg = "-f" ] || [ $arg = "--force" ]; then FORCE_WRITE=1; fi;
  if [ $arg = "-h" ] || [ $arg = "--help"  ]; then 
    echo "Usage : "$( basename $0 )" [-f|--force] [-h|--help]"
    echo ""
    echo "Met à jour, si nécessaire, dans le dataset des BSV,"
    echo "la pro:Activity d'organisation afin que la version de jena-fuseki"
    echo "indiquée corresponde à la version effectivement installée."
    echo ""
    echo "Options :"
    echo "  -h : Affiche ce message et quitte."
    echo "  -f : Force la mise à jour même si la version de jena-fuseki"
    echo "       n'a pas pu être lue ou si elle est identique à la version"
    echo "       installée."
    exit 0;
  fi;
done


echo "Lien de téléchargement de la dernière version de jena-fuseki :"
echo ${DOWNLOAD_LINK}
echo ""

# ----- Version installée de jena-fuseki -----
VERSION=$( ${FUSEKI_HOME}/fuseki-server --version | sed -e '/^Fuseki: \+VERSION:/!d;s/^.* \([^ ]\+\) *$/\1/' )

VERIF=$( echo $VERSION | sed -e '/[0-9]\+\.[0-9]\+\.[0-9]\+/!d' | wc -l )
if [ $VERIF -ne 1 ]; then
  echo "ERREUR lors de l'extraction de la version installée de jena-fuseki."
  echo "A été extrait : «$VERSION»"
  echo "Sortie sans mise à jour."
  exit -1;
fi

echo "Version installée de jena_fuseki : ${VERSION}"

# ----- Version indiquée par le prov:Agent -----
QUERY="""
PREFIX ex: <${EX_PREFIX}resources/>
PREFIX g: <${EX_PREFIX}graph/>
PREFIX schema: <http://schema.org/>
PREFIX prov: <http://www.w3.org/ns/prov#>
SELECT ?v WHERE { 
GRAPH g:activities { ex:organisation prov:wasAssociatedWith ?a } 
graph g:agents { ?a schema:version ?v }}
"""

AGENT_VER=$(curl --silent -H "Accept: text/csv" --data "query=$QUERY" ${SPARQL_SERVER}${BSV_DATASET}/sparql | tail -n 1 )
AGENT_VER=$(echo $AGENT_VER | sed -e 's/[^0-9\.]//g')

if [ $FORCE_WRITE -eq 1 ]; then
  AGENT_VER="${AGENT-VER}-force-update";
else
  VERIF=$( echo $AGENT_VER | sed -e '/[0-9]\+\.[0-9]\+\.[0-9]\+/!d' | wc -l )
  if [ $VERIF -ne 1 ]; then
    echo "ERREUR lors de l'extraction de la version indiquée par le prov:Agent."
    echo "A été extrait : «$AGENT_VER»"
    echo "Sortie sans mise à jour."
    exit -1;
  fi
  echo "Version indiquée par le prov:Agent : $AGENT_VER";
fi


# ----- On teste -----

if [ "$VERSION" = "$AGENT_VER" ]; then
  echo "Les deux versions sont identiques. Une mise à jour n'est pas nécessaire."
  exit 0;
fi

echo "Mise à jour de l'activité d'organisation"

# ----- Mise à jour -----
## --- Suppression des triplets de l'ancienne version
s-update --service=${SPARQL_SERVER}${BSV_DATASET}/update """
PREFIX g: <${EX_PREFIX}graph/>
PREFIX ex: <${EX_PREFIX}resources/>
PREFIX prov: <http://www.w3.org/ns/prov#>
WITH g:agents DELETE { ?a ?b ?c } WHERE { 
  graph g:activities { ex:organisation prov:wasAssociatedWith ?a }
  graph g:agents { ?a ?b ?c }
}"""

s-update --service=${SPARQL_SERVER}${BSV_DATASET}/update """
PREFIX g: <${EX_PREFIX}graph/>
PREFIX ex: <${EX_PREFIX}resources/>
WITH g:activities DELETE { ex:organisation ?b ?c }  WHERE { ex:organisation ?b ?c }
"""

## --- Ajout de la nouvelle version
DT=$(date +'%Y-%m-%d')
DT_FR=$(date +'%d/%m/%Y')
s-update --service=${SPARQL_SERVER}${BSV_DATASET}/update """
PREFIX g: <${EX_PREFIX}graph/>
PREFIX ex: <${EX_PREFIX}resources/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX schema: <http://schema.org/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
INSERT DATA {
GRAPH g:activities {
  ex:organisation a <http://www.w3.org/ns/prov#Activity> ;
  prov:startedAtTime \"${DT}\"^^xsd:date ;
  rdfs:comment \"Organisation des BSV. Les metadonnées des BSV sont stockées par Jena Fuseki. Montée en version le ${DT_FR}.\"@fr ;
  prov:wasAssociatedWith ex:JenaFuseki-${VERSION} .
} GRAPH g:agents {
  ex:JenaFuseki-${VERSION} a prov:SoftwareAgent, schema:CreativeWork ;
  schema:url <${DOWNLOAD_LINK}> ;
  schema:version \"${VERSION}\" .
}}
"""

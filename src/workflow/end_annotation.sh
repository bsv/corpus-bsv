#!/bin/bash

# Recherche d'éléments de configuration dans config.py et prefixes.ttl.
SCRIPT_PATH=$( readlink -f $0 | sed -e 's/\/[^/]\+$//' )
source ${SCRIPT_PATH}/config.py


DATE_SHORT=$( date +%Y%m%d )
DATE_END=$( date -Is )

# On ajoute les liens prov:wasGeneratedBy pour les annotations
UPDATE_QUERY="""PREFIX ex: <${EX_PREFIX}resources/>
PREFIX prov: <http://www.w3.org/ns/prov#>
WITH <http://ontology.inrae.fr/bsv/graph/annotations>
INSERT { ?a prov:wasGeneratedBy ex:projectionProcess${DATE_SHORT} }
WHERE { ?a a <http://ontology.inrae.fr/bsv/ontology/AutomaticAnnotation>
FILTER NOT EXISTS { ?a prov:wasGeneratedBy ?b }}
"""

curl -X POST -H "Accept:application/x-trig" \
  --data-urlencode "update=${UPDATE_QUERY}" ${SPARQL_SERVER}${BSV_DATASET}/update


# Puis on met la date de fin de l'activité de projection.
UPDATE_QUERY="""PREFIX ex: <${EX_PREFIX}resources/>
PREFIX g: <${EX_PREFIX}graph/>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
INSERT DATA { GRAPH g:activities {
ex:projectionProcess${DATE_SHORT} 
  prov:endedAtTime \"${DATE_END}\"^^xsd:dateTime. }}
"""

curl -X POST -H "Accept:application/x-trig" \
  --data-urlencode "update=${UPDATE_QUERY}" ${SPARQL_SERVER}${BSV_DATASET}/update



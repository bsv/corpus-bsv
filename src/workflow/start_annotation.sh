#!/bin/bash

PLAN_DIR=$( readlink -f $0 | sed -e 's/workflow.\+$/\//' )alvisNLP
PLAN_COMMIT=$( git log -n 1 --pretty=format:%H ${PLAN_DIR} )
PLAN_DATE=$( git log -n 1 --pretty=format:%aI ${PLAN_DIR} )
PLAN_DATE_SHORT=$( git log -n 1 --pretty=format:%as ${PLAN_DIR} |
  sed -e 's/-//g' )
PLAN_AUTHOR_FULL=$( git log -n 1 --pretty=format:%an ${PLAN_DIR} )
PLAN_AUTHOR_SHORT=$( echo ${PLAN_AUTHOR_FULL} |
  sed -e 's/\([[:alnum:]]\)[[:alnum:]]*[^[:alnum:]]\+/\1/g' )

ALVISNLP_VERSION=$( alvisnlp -version | head -n 1 )
ALVISNLP_COMMIT=$( alvisnlp -version | grep Commit: | cut -d' ' -f 2 )

# Recherche d'éléments de configuration dans config.py et prefixes.ttl.
SCRIPT_PATH=$( readlink -f $0 | sed -e 's/\/[^/]\+$//' )
source ${SCRIPT_PATH}/config.py

## On cherche le préfixe ex: dans prefixes.ttl
EX_EX_PREFIX=$EX_PREFIX
EX_PREFIX=$(cat ${SCRIPT_PATH}/data/prefixes.ttl | grep " ex: " |
  sed -e 's/^.*<\([^<]*\)>.*$/\1/' )
if [ -z "$EX_PREFIX" ]; then EX_PREFIX=$EX_EX_PREFIX; fi

# --- Récupération de l'URI de l'auteur ou de l'autrice du plan
QUERY="""PREFIX ex: <${EX_PREFIX}resources/>
PREFIX g: <${EX_PREFIX}graph/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT ?v WHERE { GRAPH g:agents { 
?v a foaf:Person ; foaf:givenName \"${PLAN_AUTHOR_FULL}\"}}
"""

AUTHOR=$( curl --silent -H "Accept: text/csv" --data "query=$QUERY" \
     ${SPARQL_SERVER}${BSV_DATASET}/sparql | tail -n 1 | \
     sed -e 's/[^[:alnum:]]*$//' )

if [[ ${AUTHOR} == v* ]]; then
  if [[ ${LANG} == fr* ]]; then
    printf '%s\n' "ERREUR : Aucun auteur dont le foaf:givenName est \"${PLAN_AUTHOR_FULL}\"." >&2
    printf '%s\n' "    → Abandon." >&2;
  else
    printf '%s\n' "ERROR : No author having \"${PLAN_AUTHOR_FULL}\" as foaf:givenName found." >&2
    printf '%s\n' "    → Operation cancelled." >&2;
  fi
  exit 4;
fi

# --- URI versionnée de frenchcropusage
FCU_VERS_URI=$( curl -s -I 'http://ontology.inrae.fr/frenchcropusage' | 
  grep ^Location | tr -d '\r' | cut -d' ' -f2 )

# --- URI versionnée de ppdo
PPDO_VERS_URI=$( curl -s -I 'http://ontology.inrae.fr/ppdo' | 
  grep ^Location | tr -d '\r' | cut -d' ' -f2 )


DATE_SHORT=$( date +%Y%m%d )
DATE_START=$( date -Is )

# --- Écriture et envoi des triplets

# echo $PLAN_DIR
# echo $PLAN_COMMIT
# echo $PLAN_DATE
# echo $PLAN_DATE_SHORT
# echo $PLAN_AUTHOR_FULL
# echo $PLAN_AUTHOR_SHORT
# echo $AUTHOR
# echo $ALVISNLP_VERSION
# echo $ALVISNLP_COMMIT
# echo $DATE_START
# echo $FCU_VERS_URI
# echo $PPDO_VERS_URI

UPDATE_QUERY="""PREFIX ex: <${EX_PREFIX}resources/>
PREFIX g: <${EX_PREFIX}graph/>
PREFIX schema: <http://schema.org/>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
INSERT DATA { GRAPH g:activities {
  ex:plan${PLAN_AUTHOR_SHORT}${PLAN_DATE_SHORT} a prov:Plan, schema:CreativeWork;
    prov:generatedAtTime \"${PLAN_DATE}\"^^xsd:dateTime;
    prov:wasAttributedTo <${AUTHOR}>;
    schema:url <${GIT_CORPUS_URL}-/commit/${PLAN_COMMIT}>.
  ex:associationPlan${PLAN_AUTHOR_SHORT}${PLAN_DATE_SHORT} a prov:Association;
    prov:hadPlan ex:plan${PLAN_AUTHOR_SHORT}${PLAN_DATE_SHORT};
    prov:agent ex:alvisNLP-${ALVISNLP_VERSION}.
  ex:projectionProcess${DATE_SHORT} a prov:Activity;
    prov:wasAssociatedWith ex:alvisNLP-${ALVISNLP_VERSION};
    prov:qualifiedAssociation
      ex:associationPlan${PLAN_AUTHOR_SHORT}${PLAN_DATE_SHORT};
    rdfs:comment 
\"Activité de projection des thesaurus frenchcropusage et ppdo sur les BSV à l'aide d'AlvisNLP.\"@fr;
    prov:startedAtTime \"${DATE_START}\"^^xsd:dateTime;
    prov:used <${FCU_VERS_URI}>, <${PPDO_VERS_URI}>.
} GRAPH g:agents {
  ex:alvisNLP-${ALVISNLP_VERSION} a prov:SoftwareAgent;
    schema:version \"$ALVISNLP_VERSION\"^^xsd:string;
    schema:url <${GIT_ALVIS_URL}/commit/${ALVISNLP_COMMIT}>.
}
}"""

#echo $UPDATE_QUERY

# Bon, bein yapuka envoyer !
curl -X POST -v -H "Accept:application/x-trig" \
  --data-urlencode "update=${UPDATE_QUERY}" ${SPARQL_SERVER}${BSV_DATASET}/update


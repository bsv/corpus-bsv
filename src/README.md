# src/

Contient le workflow d'intégration des données BSV
dans le triplestore.

## Structure des sous-dossiers

- workflow/ : Les différents outils nécessaire à l'intégration
  des BSV dans le triplestore.
- workflow/data : Les fichiers ttl pour les éléments statiques.
  Tous les préfixes sont à mettre dans prefixes.ttl, pas de préfixe
  dans les autres fichiers : ces fichiers ttl seront découpés
  en clauses "INSERT DATA"
- alvisNLP/ : Contient les plans d'annotation utilisés avec AlvisNLP.
- collecte/ : Le "Draaf web crawler" utilisé pour la collecte des BSV.  
  Utilisation :

```bash
python -u collect.py | tee COLLECT.log
```

## Les étapes du worklow d'intégration des BSV dans le triplestore.

### Préalable

- [AlvisNLP](https://bibliome.github.io/alvisnlp/) doit être installé
  et la commande `alvisnlp` dans le PATH.
- Un triplestore doit être accessible (on utilise
  [jena-fuseki](https://jena.apache.org/documentation/fuseki2/)
  installé localement).  
  On utilise aussi les outils s-put et s-get fournis avec jena-fuseki,
  qui doivent être accessibles dans le PATH.
- [xR2RML](https://www.i3s.unice.fr/~fmichel/xr2rml_specification.html)
  doit être installé.
- Un serveur [mongodb](https://www.mongodb.com/fr-fr) doit être
  installé.

### Variables d'environnement

Dans un shell, qui sera le même tout au long du processus,
on définit un certain nombre de variables d'environnement
nécessaires pour exécuter les différentes étapes.

```bash
# WORKFLOW_HOME :
# (fr) Répertoire dans lequel le git a été cloné.
# (en) Location where the git repository has been cloned.
export WORKFLOW_HOME=/home/user/work/corpus-bsv

# BSV_SOURCE_PATH :
# (fr) Le répertoire où sont stockés les corpus de test.
#      Il doit au moins avoir la structure suivante :
# (en) Directory where test corpuses PHBs are stored.
#      It should at least have the following structure :
#
# BSV_SOURCE_PATH/
# ├── corpus-alea
# │   └── bsv
# │       └── pdf
# ├── corpus-d2kab
# │   └── bsv
# │       └── pdf
# │           ├── GrandesCultures
# │           ├── Maraichage
# │           └── Viticulture
# └── corpus-vespa
#     └── bsv
#         └── pdf
#
export BSV_SOURCE_PATH=/where/PHB/test/corpus/are

# BSV_DEST_PATH :
# (fr) Répertoire où seront copiés stockés les BSV (en pdf et html).
#      La structure générée sera conforme aux URLs de téléchargement
#      des BSV.
# (en) Directory used to store PHBs (in pdf and html).
#      The generated directory structure will be used for download URLs.
export BSV_DEST_PATH=/where/PHBs/will/be/served/for/download

# (fr) Si on ne veut pas copier les BSV, on définit :
# (en) In case we don't want to copy PHBs :
# export BSV_DEST_PATH=None

export PDF2BLOCS_PATH=/the/location/where/pdf2blocks/git/have/been/cloned

export NB_PARALLEL_TASKS=<number of CPU (×2 if multithreaded)>

# Ex{e,a}mple :
# (fr) nombre de cœurs - 1 (pour garder un cœur pour d'autres tâches) :
# (en) number of cores -1 (if you want to keep one CPU for other work) :
export NCPU=$(grep -c ^processor /proc/cpuinfo)
export MINCPU=1
export NB_PARALLEL_TASKS=$( dc -e "[$NCPU 1-]sM $MINCPU $NCPU>Mp" )
unset NCPU MINCPU
```

### Déroulement du workflow

Dans un premier temps, seuls les corpus de test sont
intégrés dans le triplestore.

```bash
cd ${WORKFLOW_HOME}/src/workflow

# (fr) Éditer config.py et ajuster les valeurs,
#      notamment celles du triplestore.
# (en) Edit config.py to ajust values, such as triplestore's URL.
source config.py

# (fr) Ajout des éléments statiques dans le triplestore.
# (en) Add static data into the triplestore.
cat data/{vespa,agents,collections,activities}.ttl | python -u doSparqlUpdate.py

# (fr) Ajout de l'activité d'organisation.
# (en) Add organization activity.
./update_jena-fuseki.sh -f


# (fr) Copie des fichiers et ajout du corpus de test VESPA dans le triplestore.
# (en) File copy and add VESPA test corpus into triplestore.
ls ${BSV_SOURCE_PATH}/corpus-vespa/bsv/pdf/* | \
  python -u bsv2ttl.py --copy_bsv=${BSV_DEST_PATH} --corpus_test=Vespa \
    --dates=${WORKFLOW_HOME}/corpus_test/corpus_test_vespa/datesManuVespa.csv | \
  python -u doSparqlUpdate.py

# (fr) Copie des fichiers et ajout du corpus de test Alea dans le triplestore.
# (en) File copy and add Alea test corpus into triplestore.
find ${BSV_SOURCE_PATH}/corpus-alea/bsv/pdf/ -type f | \
  python -u bsv2ttl.py --copy_bsv=${BSV_DEST_PATH} --corpus_test=Alea \
    --dates=${WORKFLOW_HOME}/corpus_test/corpus_test_alea/datesManuAlea.csv | \
  python doSparqlUpdate.py

# (fr) Copie des fichiers et ajout du corpus de test D2KAB dans le triplestore.
# (en) File copy and add D2KAB test corpus into triplestore.
find ${BSV_SOURCE_PATH}/corpus-d2kab/bsv/pdf/ -type f | \
  python -u bsv2ttl.py --copy_bsv=$BSV_DEST_PATH --corpus_test=D2KAB \
    --dates=${WORKFLOW_HOME}/corpus_test/corpus_test_d2kab/datesManuD2kab.csv | \
  python doSparqlUpdate.py

# (fr) Copie des fichiers et ajout du corpus de test BilansViti21 
#      dans le triplestore.
# (en) File copy and add BilansViti21 test corpus into triplestore.
find ${BSV_SOURCE_PATH}/corpus-bviti21/bsv/pdf/ -type f | \
  python -u bsv2ttl.py --copy_bsv=${BSV_DEST_PATH} --corpus_test=BilansViti21 \
    --dates=${WORKFLOW_HOME}/corpus_test/corpus_test_bviti21/dates.csv | \
  python doSparqlUpdate.py

# (fr) Copie des fichiers et ajout du corpus de test VitiAls 
#      dans le triplestore.
# (en) File copy and add BilansViti21 test corpus into triplestore.
find ${BSV_SOURCE_PATH}/corpus-vitials/bsv/pdf/ -type f | \
  python -u bsv2ttl.py --copy_bsv=${BSV_DEST_PATH} --corpus_test=VitiAls \
    --dates=${WORKFLOW_HOME}/corpus_test/corpus_test_vitials/dates.csv | \
  python doSparqlUpdate.py

# (fr) Conversion en html.
#  RQ : AVANT conversion, vérifier que la librairie camelot marche bien
#       (à cause d'une MÀJ de poppler par exemple, et comme les erreurs
#        sont silencieuses - la plupart étant "malformed pdf" - il
#        se trouve que l'on ne s'en rend pas compte).
# (en) html conversion.
#  WARNING : BEFORE doing any conversion into html, the camelot python
#        library has to be checked, because any poppler update has
#        an impact on it. We turnd error outputs silent because there
#        is a huge amount of "malformed pdf" errors during the process.

# (fr) Vérification du fonctionnement de la librairie camelot :
# (en) Camelot python library check :

python -c """import camelot
doc='${BSV_SOURCE_PATH}/corpus-alea/bsv/pdf/BSV_viti_10_cle4e6966-2.pdf'
tl = camelot.read_pdf(doc, backend='poppler', pages='all')
print(tl[1].df)
"""

# (fr) Doit renvoyer (si tout est opérationnel) :
# (en) Should return (if OK) :

## 0  Situation \nau \n12/06/17  Parcelles les plus \ntardives     (…)
## 1                 Chardonnay      J 27 \n« début nouaison »  J 28 \ »
## 2                     Chenin      J 27 \n« début nouaison »  J 28 \ »
## 3                  Sauvignon      J 27 \n« début nouaison »  J 28 \ »
## 4                   Cabernet      J 27 \n« début nouaison »  J 28 \ »
## 5                 Pinot Noir      J 27 \n« début nouaison »  J 28 \ »
## 6                      Gamay      J 27 \n« début nouaison »  J 28 \ »
## 7                        Cot      J 27 \n« début nouaison »   J 27  »

# (fr) Si ça ne marche pas, réinstaller la librairie camelot.
# (en) If it doesn't work, camelot libraries have to be reinstalled.
# https://pypi.org/project/camelot-py/


# (fr) Quand tout est OK :
# (en) When everything's OK :

python -u convertToHtml.py --dest_path=$BSV_DEST_PATH --force_sparql \
 --proc=${NB_PARALLEL_TASKS} --pdf2blocks_path=${PDF2BLOCS_PATH} \
 2>errors.txt | python doSparqlUpdate.py

### À savoir ### 
# Si $BSV_DEST_PATH est None, le comptage du nombre de mots
# n'est pas effectué, et il n'y a pas non plus d'extraction de date.

### Warning ### 
# If $BSV_DEST_PATH is None, the number of words is not available.
                

### (fr)(en) ANNOTATIONS

# (fr) On fait une copie de travail des plans d'annotation
# (en) We make a working copy of annotation plans
rm -Rf ../alvisNLP.work 2> /dev/null
cp -Rf ../alvisNLP ../alvisNLP.work

./start_annotation.sh

pushd .
cd ../alvisNLP.work
chmod u+x main.sh

# Il arrive que ces répertoires ne soient pas créés :
mkdir -p resources/thesaurus/fcu
mkdir -p resources/thesaurus/ppdo
mkdir -p output/dumps
cd resources/corpus
tar -Jxf Corpus_test.html.tar.xz
ln -s test_d2kab/Viticulture d2kab_viti
cd ../..
cd utils
python extract-subcorpus.py
cd ..
### Il faut aussi modifier resources/pests/d2kab/build_bioagressors_tables.py 


## Modification de l'origine des BSV
cat main-annotation.plan | sed -e '/<load-corpus>/,$ d' > main.plan
# ATTENTION : Si $BSV_DEST_PATH est None, il faut adapter la balise
#   <source> ci-dessous :
# WARNING : If $BSV_DEST_PATH is None, <source> tag has to be rewritten
#   below :
echo """    <load-corpus>
      <bsv class=\"XMLReader\">
        <html/>
        <xslTransform>resources/segmentation/html2alvisnlp.xslt</xslTransform>
        <source>${BSV_DEST_PATH}/html/</source>
        <constantSectionFeatures>corpus=corpus d2kab</constantSectionFeatures>
      </bsv>""" >> main.plan
cat main-annotation.plan | sed -ne '/<\/load-corpus>/,$ p' >> main.plan

## Récupération de la dernière version des thesaurus
git archive --remote=git@gitlab-ssh.irstea.fr:copain/phenologicalstages.git \
  HEAD ppdo.rdf | tar -xf - 
mv ppdo.rdf resources/thesaurus/ppdo/ppdo.rdf

git archive --remote=git@gitlab-ssh.irstea.fr:copain/frenchcropusage.git \
  HEAD frenchCropUsage_latest.rdf | tar -xf -
mv frenchCropUsage_latest.rdf resources/thesaurus/fcu/frenchCropUsage.rdf

## Mise à jour des fichiers du projectors.plan
sed -i -e 's/fcu\/\(.*\)\.rdf/fcu\/frenchCropUsage.rdf/;s/ppdo\/\(.*\)\.rdf/ppdo\/ppdo.rdf/' \
  modules/projectors.plan

# (fr) On suppose qu'alvisnlp est installé et dans le PATH :
# (en) Assuming alvisnlp is installed and in the PATH :
alvisnlp main.plan

# (fr) À faire tant qu'on est dans le répertoire alvisNLP.work :
# (en) To be done while we are in the directory alvisNLP.work :
export CSVDIR=$(pwd)/output/annotations

popd
# (fr) De retour dans le répertoire workflow/
# (en) Back to workflow/ directory

# (fr) Import des csv d'annotations dans mongodb
# (en) Import generated csv into mongodb
cat $CSVDIR/fcu-baseline-annotations.csv | python csvRead.py |
  mongoimport --db=d2kab -c Entities --type=csv --ignoreBlanks --headerline --drop
cat $CSVDIR/ppdo-baseline-annotations.csv | python csvRead.py |
  mongoimport --db=d2kab -c Entities --type=csv --ignoreBlanks --headerline

# (fr) Exécution de xR2RML
# (en) Running xR2RML
java -jar /home/stef/opt/share/xr2rml/bin/morph*.jar \
  --configDir ../xR2RML/ --configFile ../xR2RML/annotation.conf \
  --mappingFile ../xR2RML/annotation.ttl  \
  --output ../xR2RML/result.ttl >/dev/null;

# (fr) Import du résultat dans le triplestore
# (en) Import result into triplestore
s-put 'http://localhost:3030/bsv' \
  'http://ontology.inrae.fr/bsv/graph/annotations' ../xR2RML/result.ttl 

# (fr) C'est fini. Il faut ajouter le prov:endedAtTime à l'Activity.
# (en) Finished. Have to add prov:endedAtTime to the Activity.
./end_annotation.sh

```


